/* -*-Mode: C;-*-
 * $Id: archivetest.c 1.21 Wed, 03 May 2000 10:36:27 -0700 jmacd $
 * file.c:
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "dbfs.h"
#include <xdelta.h>
#include <edsiostdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <dirent.h>

/*#define DEBUG_SYNC*/

typedef struct _ArchiveTest ArchiveTest;

struct _ArchiveTest {
  const char *name;
  gboolean  (*initialize1) (void);
  gboolean  (*initialize2) (void);
  gboolean  (*close1) (void);
  gboolean  (*close2) (void);
  void      (*abort) (void);
  gboolean  (*onefile) (RcsFile *rcs, RcsStats* set, void* data);
  gboolean  (*dateorder) (RcsFile* rcs, RcsVersion *v, void* data);
  gboolean  (*retrieve) (RcsFile* rcs, RcsVersion *v, void* data);
  gboolean    copydir;
  XdfsPolicy  policy;
  int         flags;
};

#define AT_FLAG_RCS_LINEAR  (1 << 0)
#define AT_FLAG_RCS_TREE    (1 << 1)

static char *tmp_file;

DBFS        *dbfs;
RepoTxn     *xdfs_txn;
Path        *xdfs_loc;

Allocator   *alloc;
GTimer      *timer;

BinCounter  *insert_single_times;
BinCounter  *insert_version_size;
BinCounter  *insert_time_size_ratio;

BinCounter  *retrieve_single_times;
BinCounter  *retrieve_version_size;
BinCounter  *retrieve_time_size_ratio;

IntStat     *xdfs_file_sizes;

double       insert_pass_time;
double       retrieve_pass_time;

int          process_versions;
int          process_files;
int          process_write_commits;

long long    encoded_size;
long long    unencoded_size;
long long    disk_blocks;

ArchiveTest *current_test;

gboolean     atest_profile          = FALSE;
gboolean     atest_retrieve_verify = FALSE;
gint32       atest_min_versions    = 0;
gint32       atest_max_versions    = G_MAXINT;
const char*  atest_test            = NULL;
const char*  atest_base_dir        = NULL;
const char*  atest_log_dir         = NULL;

#define atest_txn_per_insert TRUE

gint32 atest_short_threshold              = 2048;
gint32 atest_src_buffer_max_files         = 0;
gint32 atest_src_buffer_min_size_per_file = 2048;
gint32 atest_cluster_max_versions         = 40;

static ConfigOption options [] = {
  { "min_versions", "min",    CS_Use,    CO_Optional, CD_Int32,   & atest_min_versions },
  { "max_versions", "max",    CS_Use,    CO_Optional, CD_Int32,   & atest_max_versions },
  { "verify",       "vfy",    CS_Use,    CO_None,     CD_Bool,    & atest_retrieve_verify },
  { "method",       "mth",    CS_Use,    CO_Required, CD_String,  & atest_test },

  { "archive_base", "ab",     CS_Ignore, CO_Required, CD_String,  & atest_base_dir },
  { "archive_log",  "al",     CS_Ignore, CO_Optional, CD_String,  & atest_log_dir },

  { "short_threshold", "st",               CS_Use, CO_Optional, CD_Int32,  & atest_short_threshold },
  { "src_buf_max_files", "sbmf",           CS_Use, CO_Optional, CD_Int32,  & atest_src_buffer_max_files },
  { "src_buf_min_size_per_file", "sbmspf", CS_Use, CO_Optional, CD_Int32,  & atest_src_buffer_min_size_per_file },
  { "cluster_max_versions", "cmv",         CS_Use, CO_Optional, CD_Int32,  & atest_cluster_max_versions },

  { "profile", "pg",                       CS_Use, CO_None,     CD_Bool,   & atest_profile },
};

#define IN_ARCHIVETEST
#include "rcsextract.c"

void reset_alloc ()
{
  allocator_free (alloc);
  alloc = allocator_new ();
}

gboolean
disk_usage (const char* file, long long *dblocks)
{
  struct stat buf;
  DIR* thisdir = NULL;
  struct dirent* ent = NULL;

  if (stat (file, & buf) < 0)
    return TRUE;

  (* dblocks) += buf.st_blocks;

  if (S_ISDIR (buf.st_mode))
    {
      if (! (thisdir = opendir (file)))
	{
	  g_warning ("opendir failed: %s", file);
	  return FALSE;
	}

      while ((ent = readdir (thisdir)))
	{
	  char* name = ent->d_name;
	  char* fullname;

	  if (strcmp (name, ".") == 0)
	    continue;

	  if (strcmp (name, "..") == 0)
	    continue;

	  /* Special cases here: I'm NOT counting Berkeley DB logs and shared
	   * memory regions. */
	  if (strncmp (name, "log.", 4) == 0)
	    continue;

	  if (strncmp (name, "__db", 4) == 0)
	    continue;

	  fullname = g_strdup_printf ("%s/%s", file, name);

	  if (! disk_usage (fullname, dblocks))
	    goto abort;

	  g_free (fullname);
	}
    }

  if (thisdir && closedir (thisdir) < 0)
    {
      g_warning ("closedir failed: %s", file);
      return FALSE;
    }

  return TRUE;

 abort:

  if (thisdir)
    closedir (thisdir);

  return FALSE;
}

void
report_stats ()
{
  FILE* tf;
  long long real_size = disk_blocks * 512;

  if (! (tf = config_output ("Result.data")))
    abort ();

  fprintf (tf, "----------------\n");
  fprintf (tf, "Name:               %s\n", current_test->name);
  fprintf (tf, "Insert time:        %0.2f seconds\n", insert_pass_time);
  fprintf (tf, "Unencoded:          %qd bytes\n", unencoded_size);
  fprintf (tf, "Storage:            %qd bytes\n", real_size);
  fprintf (tf, "Ideal:              %qd bytes\n", encoded_size);
  fprintf (tf, "Storage diff:       %qd bytes\n", real_size - encoded_size);
  fprintf (tf, "Storage overhead:   %0.2f%%\n", 100.0 * ((double) (real_size - encoded_size) / (double) encoded_size));
  fprintf (tf, "Actual compression: %0.2f%%\n", 100.0 * (1.0 - ((double) real_size    / (double) unencoded_size)));
  fprintf (tf, "Ideal compression:  %0.2f%%\n", 100.0 * (1.0 - ((double) encoded_size / (double) unencoded_size)));

  fprintf (tf, "Retrieve time:      %0.2f seconds\n", retrieve_pass_time);

  if (fclose (tf) < 0)
    g_error ("fclose failed\n");
}

gboolean
atest_finalize (RcsStats* set, void* data)
{
  if (current_test->close1)
    {
      g_timer_start (timer);
      if (! current_test->close1 ())
	return FALSE;
      g_timer_stop (timer);

      insert_pass_time += g_timer_elapsed (timer, NULL);
    }

  rcswalk_report (set);

  if (! disk_usage (atest_base_dir, & disk_blocks))
    return FALSE;

  return TRUE;
}

gboolean
atest_onefile (RcsFile *rcs, RcsStats* set, void* data)
{
  gboolean ret = TRUE;

  process_files += 1;

#ifdef DEBUG_SYNC
  fprintf (stderr, "BEGIN ONEFILE\n");
#endif
  if (current_test->onefile)
    {
      ret = current_test->onefile (rcs, set, data);
    }
#ifdef DEBUG_SYNC
  fprintf (stderr, "END ONEFILE\n");
#endif

  return ret;
}

double
disk_block_size (off_t size)
{
  return (double) (((size % 4096) + 1) * 4096);
}

gboolean
atest_dateorder (RcsFile* rcs, RcsVersion *v, void* data)
{
  gboolean res = TRUE;
  double stime;
  static int progress = 0;

  process_versions += 1;

  if ((++progress % 1000) == 0)
    fprintf (stdout, "i %d\n", progress);

  unencoded_size += v->size;

#ifdef DEBUG_SYNC
  fprintf (stderr, "BEGIN DATEORDER\n");
#endif
  g_timer_start (timer);

  res = current_test->dateorder (rcs, v, data);

  g_timer_stop (timer);
#ifdef DEBUG_SYNC
  fprintf (stderr, "END DATEORDER\n");
#endif

  stime = g_timer_elapsed (timer, NULL);

  insert_pass_time += stime;

  stat_bincount_add_item (insert_single_times, v->dateseq, stime);
  stat_bincount_add_item (insert_version_size, v->dateseq, v->size);
  stat_bincount_add_item (insert_time_size_ratio, v->dateseq, stime / disk_block_size (v->size));

  return res;
}

gboolean
xdfs_onefile (RcsFile *rcs, RcsStats* set, void* data)
{
  XdfsState xstat;

  if (! xdfs_txn)
    {
      process_write_commits -= 1;

      if (! (xdfs_txn = dbfs_txn_begin (dbfs, DBFS_TXN_FLAT)))
 	return FALSE;
    }

  if (! xdfs_stat (xdfs_txn, xdfs_loc, & xstat))
    return FALSE;

#if 0
  if (! xdfs_state_print (_stdout_handle, xdfs_loc, & xstat))
    return FALSE;
#endif

  encoded_size += xstat.literal_size + xstat.control_size + xstat.patch_size;

  if (xdfs_txn)
    {
      g_timer_start (timer);

      process_write_commits += 1;

      if (! dbfs_txn_commit (xdfs_txn))
	return FALSE;

      g_timer_stop (timer);

      insert_pass_time += g_timer_elapsed (timer, NULL);
    }

  xdfs_txn = NULL;
  xdfs_loc = NULL;
  reset_alloc ();

  return TRUE;
}

gboolean
xdfs_dateorder (RcsFile* rcs, RcsVersion *v, void* data)
{
  FileHandle* fh;

  if (! xdfs_txn)
    {
      if (! (xdfs_txn = dbfs_txn_begin (dbfs, DBFS_TXN_FLAT)))
 	return FALSE;
    }

  if (! xdfs_loc)
    {
      Inode ino;
      Inode root;
      XdfsParams params;

      xdfs_loc = path_append_bytes (alloc, path_root (alloc),
				    PP_String, rcs->filename, strlen (rcs->filename));

      if (! dbfs_inode_find_root (xdfs_txn, path_root (alloc), FALSE, FT_Directory, & root))
	return FALSE;

      if (! dbfs_inode_new (xdfs_txn, & ino))
	return FALSE;

#if 0
      if (! dbfs_make_directory (xdfs_txn, & ino, TRUE))
	return FALSE;
#endif

      if (! dbfs_link_create (xdfs_txn, & root, & ino, path_basename (xdfs_loc), FALSE))
	return FALSE;

      memset (& params, 0, sizeof (params));

      params.policy                       = current_test->policy;
      params.src_buffer_max_files         = atest_src_buffer_max_files;
      params.src_buffer_min_size_per_file = atest_src_buffer_min_size_per_file;
      params.cluster_max_versions         = atest_cluster_max_versions;

      if (! xdfs_location_create (xdfs_txn, xdfs_loc, & params))
	return FALSE;
    }

  if (! (fh = handle_read_file (v->filename)))
    return FALSE;

  if (! xdfs_insert_version (xdfs_txn, xdfs_loc, fh, NULL))
    return FALSE;

  handle_free (fh);

  if (atest_txn_per_insert)
    {
      process_write_commits += 1;

      if (! dbfs_txn_commit (xdfs_txn))
	return FALSE;

      xdfs_txn = NULL;
    }

  return TRUE;
}

gboolean
xdfs_init1 ()
{
  Path *archive_path;
  Path *log_path = NULL;

  if (! dbfs_library_init ())
    return FALSE;

  if (! (archive_path = path_from_host_string (NULL, _fs_pthn, atest_base_dir)))
    return FALSE;

  if (atest_log_dir && ! (log_path = path_from_host_string (NULL, _fs_pthn, atest_log_dir)))
    return FALSE;

  if (! (dbfs = dbfs_create (archive_path, log_path)))
    return FALSE;

  if (! xdfs_library_init ())
    return FALSE;

  return TRUE;
}

gboolean
xdfs_init2 ()
{
  Path *archive_path;

  if (! (archive_path = path_from_host_string (NULL, _fs_pthn, atest_base_dir)))
    return FALSE;

  if (! (dbfs = dbfs_initialize (archive_path)))
    return FALSE;

  return TRUE;
}

gboolean
dbfs_file_size_histogram (DBFS *dbfs)
{
  DBC *dbc = NULL;
  int ret;
  DBT key, data;

  dbfs_clear_dbts (& key, & data);

  if (! (xdfs_txn = dbfs_txn_begin (dbfs, DBFS_TXN_FLAT)))
    return FALSE;

  if ((ret = dbfs->minor_inodes_dbp->cursor (dbfs->minor_inodes_dbp, xdfs_txn->db_txn, & dbc, 0)))
    goto abort;

  while ((ret = dbc->c_get (dbc, & key, & data, DB_NEXT)) == 0)
    {
      MinorInode* m = data.data;
      g_assert (data.size == sizeof(MinorInode));

      switch (m->ino_type)
	{
	case FT_Invalid:
	case FT_NotFound:
	case FT_Null:
	case FT_NotPresent:
	case FT_Directory:
	case FT_Sequence:
	case FT_Index:
	case FT_Relation:
	case FT_Symlink:
	case FT_Indirect:
	case FT_View:

	  break;
	case FT_Segment:
	  stat_int_add_item (xdfs_file_sizes, m->ino_segment_len);
	  break;
	}
    }

  if (! dbfs_txn_commit (xdfs_txn))
    goto abort;

  xdfs_txn = NULL;

  dbc->c_close (dbc);

  return TRUE;

 abort:

  if (dbc) dbc->c_close (dbc);

  return FALSE;
}

gboolean
xdfs_close1 ()
{
  if (! dbfs_file_size_histogram (dbfs))
    return FALSE;

  if (! dbfs_close (dbfs))
    return FALSE;

  return TRUE;
}

gboolean
xdfs_close2 ()
{
  if (! dbfs_close (dbfs))
    return FALSE;

  if (! dbfs_library_close ())
    return FALSE;

  return TRUE;
}

void
xdfs_abort ()
{
  if (xdfs_txn)
    dbfs_txn_abort (xdfs_txn);

  if (dbfs)
    dbfs_close (dbfs);

  dbfs_library_close ();
}

/* RCS
 */

gboolean
rcs_register (RcsFile *rcs)
{
  int pid;
  static GPtrArray *a = NULL;

  if (! a)
    a = g_ptr_array_new ();

  g_ptr_array_set_size (a, 0);

  g_ptr_array_add (a, "rcs");
  g_ptr_array_add (a, "-i");
  g_ptr_array_add (a, "-U");
  g_ptr_array_add (a, "-t-no");
  g_ptr_array_add (a, "-q");
  g_ptr_array_add (a, (char*) rcs->copyname);
  g_ptr_array_add (a, NULL);

  if ((pid = vfork ()) < 0)
    g_error ("vfork failed\n");

  if (pid == 0)
    {
      /* child */
      execv ("/usr/bin/rcs", (char**) a->pdata);
      g_warning ("exec failed1: %s", g_strerror (errno));
      _exit (127);
    }
  else
    {
      /* parent */
      int status;

      if (waitpid (pid, &status, 0) < 0)
	g_error ("waitpid failed\n");

      if (! WIFEXITED (status))
	g_error ("rcs did not exit\n");

      if (WEXITSTATUS (status) == 127)
	g_error ("rcs did not execute\n");
    }

  return TRUE;
}

gboolean
rcs_copy_to_location (RcsFile *rcs, RcsVersion *v, char **locp)
{
  int   len = strlen (rcs->copyname);
  char* loc = g_strdup (rcs->copyname);
  FileHandle *r, *w;

  loc[len-2] = 0;

  (*locp) = loc;

  if (! (r = handle_read_file (v->filename)))
    return FALSE;

  if (! (w = handle_write_file (loc)))
    return FALSE;

  if (! handle_drain (r, w))
    return FALSE;

  if (! handle_close (r))
    return FALSE;

  if (! handle_close (w))
    return FALSE;

  handle_free (r);
  handle_free (w);

  return TRUE;
}

gboolean
rcs_onefile (RcsFile *rcs, RcsStats* set, void* data)
{
  guint this_size;

  if (! rcs_count (rcs->copyname, & this_size))
    return FALSE;

  encoded_size += this_size;

  return TRUE;
}

gboolean
rcs_dateorder (RcsFile* rcs, RcsVersion *v, void* data)
{
  int pid;
  static GPtrArray *a = NULL;
  static GString   *varg = NULL;
  char *loc;

  if (! rcs_copy_to_location (rcs, v, &loc))
    return FALSE;

  if (! a)
    {
      a = g_ptr_array_new ();
      varg = g_string_new (NULL);
    }

  if (! v->parent && ! rcs_register (rcs))
    return FALSE;

  if (current_test->flags & AT_FLAG_RCS_TREE)
    g_string_sprintf (varg, "-q%s", v->vname);
  else if (current_test->flags & AT_FLAG_RCS_LINEAR)
    g_string_sprintf (varg, "-q");
  else
    abort ();

  g_ptr_array_set_size (a, 0);

  g_ptr_array_add (a, "ci");
  g_ptr_array_add (a, "-f");
  g_ptr_array_add (a, "-mno");
  g_ptr_array_add (a, "-t-no");
  g_ptr_array_add (a, varg->str);
  g_ptr_array_add (a, loc);
  g_ptr_array_add (a, (char*) rcs->copyname);
  g_ptr_array_add (a, NULL);

  if ((pid = vfork ()) < 0)
    g_error ("vfork failed\n");

  if (pid == 0)
    {
      /* child */
      execv ("/local/jmacd/xdfs/libdbfs/tools/bin/ci", (char**) a->pdata);
      g_warning ("exec failed2: %s", g_strerror (errno));
      _exit (127);
    }
  else
    {
      /* parent */
      int status;

      if (waitpid (pid, &status, 0) < 0)
	g_error ("waitpid failed\n");

      if (WIFSIGNALED (status))
	g_error ("ci was signaled: %d\n", WTERMSIG (status));

      if (! WIFEXITED (status))
	g_error ("ci did not exit\n");

      if (WEXITSTATUS (status) == 127)
	g_error ("ci did not execute\n");
    }

  g_free (loc);

  return TRUE;
}

/* FS
 */

gboolean
fs_dateorder (RcsFile* rcs, RcsVersion *v, void* data)
{
  FileHandle *r, *w;
  char* loc;

  encoded_size += v->size;

  if (! rcs->atflag)
    {
      rcs->atflag = TRUE;

      if (mkdir (rcs->copyname, 0777) < 0)
	{
	  g_error ("mkdir %s failed\n", rcs->copyname);
	  return FALSE;
	}
    }

  loc = g_strdup_printf ("%s/%s", rcs->copyname, v->vname);

  if (! (r = handle_read_file (v->filename)))
    return FALSE;

  if (! (w = handle_write_file (loc)))
    return FALSE;

  if (! handle_drain (r, w))
    return FALSE;

  if (! handle_close (r))
    return FALSE;

  if (! handle_sync (w))
    return FALSE;

  if (! handle_close (w))
    return FALSE;

  handle_free (r);
  handle_free (w);
  g_free (loc);

  return TRUE;
}

/* Retrieval code
 */

gboolean
rtest_onefile (RcsFile *rcs, RcsStats* set, void* data)
{
  if (xdfs_txn)
    {
      if (! dbfs_txn_commit (xdfs_txn))
	return FALSE;
    }

  xdfs_txn = NULL;
  xdfs_loc = NULL;
  reset_alloc ();

  return TRUE;
}

gboolean
xdfs_retrieve (RcsFile* rcs, RcsVersion *v, void* data)
{
  FileHandle *in_fh, *out_fh;
  Inode seq;
  Inode ino;
  int bin;

  if (! xdfs_txn)
    {
      if (! (xdfs_txn = dbfs_txn_begin (dbfs, DBFS_TXN_FLAT | DBFS_TXN_NOSYNC)))
 	return FALSE;
    }

  if (! xdfs_loc)
    {
      xdfs_loc = path_append_bytes (alloc, path_root (alloc), PP_String, rcs->filename, strlen (rcs->filename));
    }

  if (! xdfs_container_sequence (xdfs_txn, xdfs_loc, & seq))
    return FALSE;

  if (! dbfs_inode_find_seqno (xdfs_txn, & seq, v->dateseq+1, DbfsNoFollowLinks, FT_CanRead, & ino))
    return FALSE;

  if (! (in_fh = dbfs_inode_open_read (xdfs_txn, & ino)))
    return FALSE;

  if (! (out_fh = handle_write_file (tmp_file)))
    return FALSE;

  if (! handle_drain (in_fh, out_fh))
    return FALSE;

  if (! handle_close (out_fh))
    return FALSE;

  handle_close (in_fh);
  handle_free (out_fh);
  handle_free (in_fh);

  bin = rcs->version_count - v->dateseq - 1;

  if (atest_txn_per_insert)
    {
      if (! dbfs_txn_commit (xdfs_txn))
	return FALSE;

      xdfs_txn = NULL;
    }

  return TRUE;
}

gboolean
rcs_retrieve (RcsFile* rcs, RcsVersion *v, void* data)
{
  int pid;
  static GPtrArray *a = NULL;
  static GString   *varg = NULL;

  if (! a)
    {
      a = g_ptr_array_new ();
      varg = g_string_new (NULL);
    }

  /* child */
  g_ptr_array_set_size (a, 0);

  if (current_test->flags & AT_FLAG_RCS_TREE)
    g_string_sprintf (varg, "-p%s", v->vname);
  else
    g_string_sprintf (varg, "-p1.%d", v->dateseq+1);

  g_ptr_array_add (a, "co");

  g_ptr_array_add (a, "-ko");
  g_ptr_array_add (a, "-q");
  g_ptr_array_add (a, varg->str);
  g_ptr_array_add (a, (char*) rcs->copyname);
  g_ptr_array_add (a, NULL);

  if ((pid = vfork ()) < 0)
    g_error ("vfork failed\n");

  if (pid == 0)
    {
      int fd;

      if ((fd = open (tmp_file, O_WRONLY | O_TRUNC | O_CREAT, 0666)) < 0)
	g_error ("open tmp failed\n");

      dup2(fd, STDOUT_FILENO);

      close (fd);

      /* exec */
      execv ("/usr/bin/co", (char**) a->pdata);
      g_warning ("exec failed3: %s", g_strerror (errno));
      _exit (127);
    }
  else
    {
      /* parent */
      int status;

      if (waitpid (pid, &status, 0) < 0)
	g_error ("waitpid failed\n");

      if (! WIFEXITED (status))
	g_error ("co did not exit\n");

      if (WEXITSTATUS (status) == 127)
	g_error ("co did not execute\n");
    }

  return TRUE;
}

gboolean
fs_retrieve (RcsFile* rcs, RcsVersion *v, void* data)
{
  FileHandle *r, *w;
  char* loc;

  loc = g_strdup_printf ("%s/%s", rcs->copyname, v->vname);

  if (! (r = handle_read_file (loc)))
    return FALSE;

  if (! (w = handle_write_file (tmp_file)))
    return FALSE;

  if (! handle_drain (r, w))
    return FALSE;

  if (! handle_close (r))
    return FALSE;

  if (! handle_close (w))
    return FALSE;

  handle_free (r);
  handle_free (w);
  g_free (loc);

  return TRUE;
}

gboolean
verify_version (RcsFile* rcs, RcsVersion *v)
{
  FileHandle *orig, *retr;
  guint8 obuf[1024], rbuf[1024];
  int offset = 0;
  int oc, rc;

  if (! (orig = handle_read_file (v->filename)))
    return FALSE;

  if (! (retr = handle_read_file (tmp_file)))
    return FALSE;

  for (;;)
    {
      oc = handle_read (orig, obuf, 1024);
      rc = handle_read (retr, rbuf, 1024);

      if (oc < 0 || rc < 0)
	g_error ("read failed: verify\n");

      if (oc != rc)
	{
	  fprintf (stderr, "verify failed: different lengths: %d != %d\n", oc, rc);
	  return FALSE;
	}

      if (oc == 0)
	break;

      if (memcmp (obuf, rbuf, oc) != 0)
	{
	  fprintf (stderr, "verify failed: different content near offset: %d\n", offset);
	  return FALSE;
	}

      offset += oc;
    }

  handle_close (orig);
  handle_close (retr);

  handle_free (orig);
  handle_free (retr);

  return TRUE;
}

gboolean
rtest_dateorder (RcsFile* rcs, RcsVersion *v, void* data)
{
  gboolean res;
  double stime;
  int bin;
  static int progress = 0;

  if ((++progress % 1000) == 0)
    fprintf (stdout, "r %d\n", progress);

  g_timer_start (timer);
  res = current_test->retrieve (rcs, v, data);
  g_timer_stop (timer);

  if (! res)
    return FALSE;

  stime = g_timer_elapsed (timer, NULL);

  retrieve_pass_time += stime;

  /* Decide how to place this single time.  For the RCS tree case, use delta
   * chain length, otherwise use sequence number.
   */
  if (current_test->flags & AT_FLAG_RCS_TREE)
    bin = v->chain_length;
  else
    bin = rcs->version_count - v->dateseq - 1;

  stat_bincount_add_item (retrieve_single_times, bin, stime);
  stat_bincount_add_item (retrieve_version_size, bin, v->size);
  stat_bincount_add_item (retrieve_time_size_ratio, bin, stime / disk_block_size (v->size));

  if (atest_retrieve_verify && ! verify_version (rcs, v))
    return FALSE;

  return TRUE;
}

gboolean
rtest_finalize (RcsStats* set, void* data)
{
  if (current_test->close2 && ! current_test->close2 ())
    return FALSE;

  report_stats ();

  stat_bincount_report (insert_single_times);
  stat_bincount_report (insert_version_size);
  stat_bincount_report (insert_time_size_ratio);
  stat_bincount_report (retrieve_single_times);
  stat_bincount_report (retrieve_version_size);
  stat_bincount_report (retrieve_time_size_ratio);
  stat_int_report      (xdfs_file_sizes);

  return TRUE;
}

RcsWalker atest_walker = {
  NULL,
  & atest_finalize,
  & atest_onefile,
  & atest_dateorder,
  NULL,
  NULL,
  0,
  G_MAXINT,
  TRUE
};

RcsWalker rtest_walker = {
  NULL,
  & rtest_finalize,
  & rtest_onefile,
  & rtest_dateorder,
  NULL,
  NULL,
  0,
  G_MAXINT,
  FALSE
};

ArchiveTest all_tests[] = {
  { "XDFS-none",    xdfs_init1, xdfs_init2, xdfs_close1, xdfs_close2, xdfs_abort, xdfs_onefile, xdfs_dateorder, xdfs_retrieve, FALSE, XP_NoCompress, 0 },
  { "XDFS-rj",      xdfs_init1, xdfs_init2, xdfs_close1, xdfs_close2, xdfs_abort, xdfs_onefile, xdfs_dateorder, xdfs_retrieve, FALSE, XP_ReverseJump, 0 },
  { "XDFS-fj",      xdfs_init1, xdfs_init2, xdfs_close1, xdfs_close2, xdfs_abort, xdfs_onefile, xdfs_dateorder, xdfs_retrieve, FALSE, XP_ForwardJump, 0 },
  { "XDFS-f",       xdfs_init1, xdfs_init2, xdfs_close1, xdfs_close2, xdfs_abort, xdfs_onefile, xdfs_dateorder, xdfs_retrieve, FALSE, XP_Forward, 0 },
  { "FS-none",    NULL, NULL, NULL, NULL, NULL, NULL, fs_dateorder,  fs_retrieve,  TRUE, 0, 0 },
  { "RCS-tree",   NULL, NULL, NULL, NULL, NULL, rcs_onefile, rcs_dateorder, rcs_retrieve, TRUE, 0, AT_FLAG_RCS_TREE },
  { "RCS-linear", NULL, NULL, NULL, NULL, NULL, rcs_onefile, rcs_dateorder, rcs_retrieve, TRUE, 0, AT_FLAG_RCS_LINEAR }
};

int
main (int argc, char** argv)
{
  int   i;

  rcswalk_init ();

  alloc                 = allocator_new ();
  timer                 = g_timer_new ();

  insert_single_times      = stat_bincount_new ("InsertTime");
  insert_version_size      = stat_bincount_new ("InsertSize");
  insert_time_size_ratio   = stat_bincount_new ("InsertTimeSizeRatio");
  retrieve_single_times    = stat_bincount_new ("RetrieveTime");
  retrieve_version_size    = stat_bincount_new ("RetrieveSize");
  retrieve_time_size_ratio = stat_bincount_new ("RetrieveTimeSizeRatio");
  xdfs_file_sizes          = stat_int_new      ("XdfsFileSizes");

  tmp_file = g_strdup_printf ("%s/at.%d", g_get_tmp_dir (), (int) getpid ());

  config_register (options, ARRAY_SIZE (options));

  config_set_string ("rcswalk_experiment", "at");

  if (argc < 2)
    {
      config_help ();
      goto bail;
    }

  for (i = 1; i < argc; i += 1)
    {
      if (! config_parse (argv[i]))
	goto bail;
    }

  if (! config_done ())
    goto bail;

  for (i = 0; i < ARRAY_SIZE (all_tests); i += 1)
    {
      if (strcmp (atest_test, all_tests[i].name) == 0)
	{
	  current_test = all_tests + i;
	  break;
	}
    }

  if (! current_test)
    g_error ("No such test: %s\n", atest_test);

  fprintf (stderr, "archivetest: verification %s; method: %s\n", atest_retrieve_verify ? "on" : "off", current_test->name);

  if (! config_clear_dir (atest_base_dir))
    goto bail;

  if (! config_clear_dir (atest_log_dir))
    goto bail;

  rtest_walker.min_versions = atest_min_versions;
  rtest_walker.max_versions = atest_max_versions;

  atest_walker.min_versions = atest_min_versions;
  atest_walker.max_versions = atest_max_versions;

  if (atest_retrieve_verify)
    rtest_walker.write_files = TRUE;

  dbfs_set_fs_short_threshold (atest_short_threshold);

  if (current_test->initialize1 && ! current_test->initialize1 ())
    goto bail;

  if (! rcswalk (& atest_walker, current_test->copydir ? atest_base_dir : NULL))
    goto bail;

  if (current_test->initialize2 && ! current_test->initialize2 ())
    goto bail;

  if (! rcswalk (& rtest_walker, current_test->copydir ? atest_base_dir : NULL))
    goto bail;

  unlink (tmp_file);

  fprintf (stderr, "archivetest: processed %d versions; %d files; %d write/commits\n", process_versions, process_files, process_write_commits);

  return 0;

 bail:

  if (current_test && current_test->abort)
    current_test->abort ();

  unlink (tmp_file);

  return 2;
}
