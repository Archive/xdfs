/* -*-Mode: C;-*-
 * $Id: dbfs.c 1.16 Wed, 03 May 2000 18:09:06 -0700 jmacd $
 *
 * Copyright (C) 1999, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "dbfs.h"
#include "edsiostdio.h"

#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

/* Implementation
 */

BaseName* _dbfs_default_name;

#define DBFS_MAX_OPEN_DBS  32

/*#define DBFS_ONE_BIG_DB*/

/* @@@ Endian issues abound here...  I will deal with them later, or will I? */

#define DB_ENV_OPEN_DEFAULT_FLAGS (DB_INIT_LOCK | DB_INIT_LOG | DB_INIT_MPOOL | DB_INIT_TXN | DB_THREAD)

extern void path_library_init (void);

static GHashTable* _dbfs_views = NULL;

gboolean
dbfs_library_init (void)
{
  gboolean ret = TRUE;

  ret &= edsio_library_init ();
  ret &= edsio_edsio_init ();
  ret &= dbfs_edsio_init ();

  _dbfs_default_name = basename_new (NULL, NULL, 0);

  path_library_init ();

  return ret;
}

gboolean
dbfs_library_close (void)
{
  return TRUE;
}

static DBFS*
dbfs_initialize_internal (Path *fs_base, Path *fs_log_base, int flag)
{
  DBFS        *dbfs;
  RepoTxn     *this_txn = NULL;
  int          err;
  int          open_flags = flag | DB_THREAD | (flag == DB_CREATE ? DB_EXCL : 0);
  GString     *fs_base_str = g_string_new (NULL);
  GString     *fs_log_base_str = g_string_new (NULL);
  GString     *db_config_str = g_string_new (NULL);
  struct stat  sbuf;

  /* Begin constructing the dbfs.
   */

  dbfs = g_new0 (DBFS, 1);

  dbfs->fs_base = fs_base;
  dbfs->fs_page_size = sysconf (_SC_PAGESIZE);

  path_to_host_string (_fs_pthn, fs_base, fs_base_str);

  path_to_host_string (_fs_pthn, path_append_string (NULL, fs_base, PP_String, "DB_CONFIG"), db_config_str);

  /* Test/create the database area.
   */

  if ((err = db_env_create (& dbfs->env, 0)))
    {
      dbfs_generate_int_event (EC_DbfsDbEnvCreate, err);
      goto cleanup;
    }

  if (fs_log_base)
    {
      path_to_host_string (_fs_pthn, fs_log_base, fs_log_base_str);
    }

  err = stat (fs_base_str->str, & sbuf);

  if (flag & DB_CREATE)
    {
      if (err == 0)
	{
	  dbfs_generate_path_event (EC_DbfsFileExists, fs_base);
	  goto cleanup;
	}

      if ((err = mkdir (fs_base_str->str, 0777)))
	{
	  dbfs_generate_pathint_event (EC_DbfsMkdirFailed, fs_base, err);
	  goto cleanup;
	}

      if (fs_log_base)
	{
	  err = stat (fs_log_base_str->str, & sbuf);

	  if (err == 0)
	    {
	      dbfs_generate_path_event (EC_DbfsFileExists, fs_log_base);
	      goto cleanup;
	    }

	  if ((err = mkdir (fs_log_base_str->str, 0777)))
	    {
	      dbfs_generate_pathint_event (EC_DbfsMkdirFailed, fs_log_base, err);
	      goto cleanup;
	    }
	}
    }
  else
    {
      if (err != 0)
	{
	  dbfs_generate_pathint_event (EC_DbfsStatFailed, fs_base, err);
	  goto cleanup;
	}

      if (! S_ISDIR (sbuf.st_mode))
	{
	  dbfs_generate_path_event (EC_DbfsNotADirectory, fs_base);
	  goto cleanup;
	}
    }

  if (flag & DB_CREATE)
    {
      FileHandle *fh;

      if (! (fh = handle_write_file (db_config_str->str)))
	goto cleanup;

      if (! handle_printf (fh, "set_lg_bsize  %d\n",     1 << 16) ||
	  ! handle_printf (fh, "set_cachesize 0 %d 1\n", 1 << 20) ||
	  ! handle_printf (fh, "set_tx_max    %d\n", DBFS_TX_MAX) ||
	  ! handle_printf (fh, "set_lk_max    %d\n", DBFS_LK_MAX))
	goto cleanup;

      if (fs_log_base &&
	  ! handle_printf (fh, "set_lg_dir    %s\n", fs_log_base_str->str))
	goto cleanup;

      if (! handle_close (fh))
	goto cleanup;

      handle_free (fh);
    }

  dbfs->env->set_errfile (dbfs->env, stderr);
  dbfs->env->set_errpfx  (dbfs->env, "dbfs");

  if ((err = dbfs->env->open (dbfs->env,
			      fs_base_str->str,
			      flag | DB_ENV_OPEN_DEFAULT_FLAGS,
			      0666)))
    {
      dbfs_generate_pathint_event (EC_DbfsDbEnvOpen, fs_base, err);
      goto cleanup;
    }

  dbfs->env->set_errfile (dbfs->env, stderr); /* Debug ... */

  /* Open the databases.
   */

  if ((err = db_create (& dbfs->major_inodes_dbp, dbfs->env, 0)))
    goto badcreate;

  if ((err = dbfs->major_inodes_dbp->set_re_len (dbfs->major_inodes_dbp, sizeof (MajorInode))))
    goto badcreate;

  if ((err = dbfs->major_inodes_dbp->open (dbfs->major_inodes_dbp,
					   "major_inodes",
					   NULL,
					   DB_QUEUE,
					   open_flags,
					   0666)))
    goto badopen;

  if ((err = db_create (& dbfs->minor_inodes_dbp, dbfs->env, 0)))
    goto badcreate;

  if ((err = dbfs->minor_inodes_dbp->open (dbfs->minor_inodes_dbp,
					   "minor_inodes",
					   NULL,
					   DB_BTREE,
					   open_flags,
					   0666)))
    goto badopen;

  if ((err = db_create (& dbfs->shorts_dbp, dbfs->env, 0)))
    goto badcreate;

  if ((err = dbfs->shorts_dbp->open (dbfs->shorts_dbp,
				     "shorts",
				     NULL,
				     DB_RECNO,
				     open_flags,
				     0666)))
    goto badopen;

  if ((err = db_create (& dbfs->sequences_dbp, dbfs->env, 0)))
    goto badcreate;

  if ((err = dbfs->sequences_dbp->set_re_len (dbfs->sequences_dbp, sizeof (guint64))))
    goto badcreate;

  if ((err = dbfs->sequences_dbp->open (dbfs->sequences_dbp,
					"sequences",
					NULL,
					DB_QUEUE,
					open_flags,
					0666)))
    goto badopen;

  /* Create initial state: standard sequences, root inode, root inode
   * content.
   */

  if (! (this_txn = dbfs_txn_begin (dbfs, DBFS_TXN_FLAT)))
    goto cleanup;

  dbfs->root_ino.key.inum = INODE_ROOT;
  dbfs->root_ino.key.name = _dbfs_default_name;
  dbfs->root_ino.key.stck = DBFS_DEFAULT_STACK_ID;

  if (flag & DB_CREATE)
    {
      guint64 inum;

      /* Order matters here, so that the constants match their allocation
       */

      if (! sequence_create_known (dbfs, SEQUENCE_KEY_FSLOC, SEQUENCE_INIT_FSLOC))
	goto cleanup;

#ifdef DBFS_ONE_BIG_DB
      if (! sequence_create_known (dbfs, SEQUENCE_KEY_DBNAME, SEQUENCE_INIT_DBNAME))
	goto cleanup;
#endif

      if (! dbfs_inum_allocate (this_txn, & inum) || inum != INODE_NULL)
	goto cleanup;

      if (! dbfs_inum_allocate (this_txn, & inum) || inum != INODE_ROOT)
	goto cleanup;

      dbfs->root_ino.minor.ino_type = FT_Directory;
      dbfs->root_ino.minor.ino_flags = 0; /* Root directory is a hash. */

      if (! dbfs_init_db (this_txn, & dbfs->root_ino))
	goto cleanup;

      if (! dbfs_putnode (this_txn, & dbfs->root_ino, TRUE))
	goto cleanup;
    }
  else
    {
      if (! dbfs_getnode (this_txn, & dbfs->root_ino, FALSE))
	goto cleanup;
    }

  if (! dbfs_txn_commit (this_txn))
    goto cleanup;

  this_txn = NULL;

  return dbfs;

  if (0)
    {
    badopen:
      dbfs_generate_int_event (EC_DbfsDbOpen, err);
    }

  if (0)
    {
    badcreate:
      dbfs_generate_int_event (EC_DbfsDbCreate, err);
    }

 cleanup:

  if (fs_base_str)
    g_string_free (fs_base_str, TRUE);

  if (db_config_str)
    g_string_free (db_config_str, TRUE);

  if (fs_log_base_str)
    g_string_free (fs_log_base_str, TRUE);

  if (this_txn)
    dbfs_txn_abort (this_txn);

  return NULL;
}

DBFS*
dbfs_create (Path *fs_base, Path *fs_log_base)
{
  return dbfs_initialize_internal (fs_base, fs_log_base, DB_CREATE);
}

DBFS*
dbfs_initialize (Path *fs_base)
{
  return dbfs_initialize_internal (fs_base, NULL, 0);
}

gboolean
dbfs_close (DBFS *dbfs)
{
  GSList *t;
  int err;
  gboolean result = TRUE;
  GList *l;

  for (t = dbfs->root_txns; t; t = t->next)
    {
      RepoTxn *txn = t->data;

      dbfs_txn_abort (txn);
    }

  g_slist_free (dbfs->root_txns);

  for (l = dbfs->open_dbs; l; l = l->next)
    {
      OpenDB *odb = l->data;

      g_assert (odb->refcount == 0);

      if ((err = odb->dbp->close (odb->dbp, 0)))
	{
	  dbfs_generate_int_event (EC_DbfsDbClose, err);
	  result = FALSE;
	}

      g_free (odb);
    }

  g_list_free (dbfs->open_dbs);

  if ((err = dbfs->major_inodes_dbp->close (dbfs->major_inodes_dbp, 0)))
    {
      dbfs_generate_int_event (EC_DbfsDbClose, err);
      result = FALSE;
    }

  if ((err = dbfs->minor_inodes_dbp->close (dbfs->minor_inodes_dbp, 0)))
    {
      dbfs_generate_int_event (EC_DbfsDbClose, err);
      result = FALSE;
    }

  if ((err = dbfs->shorts_dbp->close (dbfs->shorts_dbp, 0)))
    {
      dbfs_generate_int_event (EC_DbfsDbClose, err);
      result = FALSE;
    }

  if ((err = dbfs->sequences_dbp->close (dbfs->sequences_dbp, 0)))
    {
      dbfs_generate_int_event (EC_DbfsDbClose, err);
      result = FALSE;
    }

  if ((err = txn_checkpoint (dbfs->env, 0, 0, 0)))
    {
      dbfs_generate_int_event (EC_DbfsDbTxnCheckpoint, err);
      result = FALSE;
    }

  if ((err = dbfs->env->close (dbfs->env, 0)))
    {
      dbfs_generate_int_event (EC_DbfsDbEnvClose, err);
      result = FALSE;
    }

  return result;
}

gboolean
dbfs_txn_pre_commit (RepoTxn *txn)
{
  Cursor *c = NULL;

  if (! txn->parent)
    {
      IDSetElt *p;

      for (p = txn->unref_inums->set; p; p = p->next)
	{
	  Inode ino;

	  if (p->id <= INODE_ROOT)
	    continue;

	  ino.key.inum = p->id;
	  ino.key.stck = DBFS_DEFAULT_STACK_ID;
	  ino.key.name = _dbfs_default_name;

	  if (! (c = dbfs_cursor_open (txn, & ino, CT_MinorInodes)))
	    goto abort;

	  while (dbfs_cursor_next (c))
	    {
#if 0
	      guint64 inum;

	      if (! dbfs_cursor_inum (c, & inum))
		goto abort;
#endif
	      if (! dbfs_cursor_delete (c))
		goto abort;
	    }

	  if (! dbfs_cursor_close (c))
	    return FALSE;
	}

      for (p = txn->unref_fs_aborts->set; p; p = p->next)
	{
	  g_assert (p->data > 0);

	  if (FSYNC (p->data) < 0 || close (p->data) < 0)
	    {
	      dbfs_generate_void_event (EC_DbfsFsyncFailed);
	      goto abort;
	    }
	}
    }

  return TRUE;

 abort:

  if (c)
    dbfs_cursor_close (c);

  return FALSE;
}

void
dbfs_shift_idsets_up (RepoTxn *txn)
{
  GString *fsloc_str = g_string_new (NULL);

  if (! txn->parent)
    {
      IDSetElt *elt;

      /* @@@ This stuff isn't transaction protected.  There is
       * a window in which these files won't be garbage collected.
       * How to fix?
       */

      for (elt = txn->unref_db_commits->set; elt; elt = elt->next)
	{
	  /* @@@ */
	  /*if (! dbfs_delete_db (txn->dbfs, elt->id))
	    continue;*/
	  g_print ("@@@ unref db %qd\n", elt->id);
	}

      for (elt = txn->unref_fs_commits->set; elt; elt = elt->next)
	{
	  Path *p;

	  if (! (p = sequence_fsloc_absolute_path (txn->alloc, txn->dbfs, elt->id)))
	    continue;

	  g_string_truncate (fsloc_str, 0);
	  path_to_host_string (_fs_pthn, p, fsloc_str);

	  if (truncate (fsloc_str->str, 0)) /* @@@ unlink... */
	    {
	      dbfs_generate_int_event (EC_DbfsUnlinkFailed, errno);
	      continue;
	    }

	  /*g_print ("unlink %s\n", fsloc_str->str);*/
	}

      allocator_free (txn->alloc);
      idset_free (txn->unref_inums);
      idset_free (txn->unref_fs_aborts);
      idset_free (txn->unref_fs_commits);
      idset_free (txn->unref_db_aborts);
      idset_free (txn->unref_db_commits);
    }

  g_string_free (fsloc_str, TRUE);
}

void
dbfs_shift_idsets_down (RepoTxn *txn)
{
  txn->unref_inums = txn->parent->unref_inums;
  txn->unref_fs_aborts = txn->parent->unref_fs_aborts;
  txn->unref_fs_commits = txn->parent->unref_fs_commits;
  txn->unref_db_aborts = txn->parent->unref_db_aborts;
  txn->unref_db_commits = txn->parent->unref_db_commits;
}

void
dbfs_create_idsets (RepoTxn *txn)
{
  txn->alloc = allocator_new ();
  txn->unref_inums = idset_new ("inums");
  txn->unref_fs_aborts = idset_new ("fs_aborts");
  txn->unref_fs_commits = idset_new ("fs_commits");
  txn->unref_db_aborts = idset_new ("db_aborts");
  txn->unref_db_commits = idset_new ("db_commits");
}

static int
dbfs_txn_db_flags (int flags)
{
  int res = 0;

  if (flags & DBFS_TXN_SYNC)
    res |= DB_TXN_SYNC;

  if (flags & DBFS_TXN_NOSYNC)
    res |= DB_TXN_NOSYNC;

  if (flags & DBFS_TXN_NONBLOCKING)
    res |= DB_TXN_NOWAIT;

  return res;
}

RepoTxn*
dbfs_txn_begin (DBFS *dbfs, int flags)
{
  RepoTxn* txn = g_new0 (RepoTxn, 1);
  int err;

  dbfs->root_txns = g_slist_prepend (dbfs->root_txns, txn);

  txn->parent = NULL;
  txn->dbfs = dbfs;
  txn->flags = flags;

  dbfs_create_idsets (txn);

  if ((err = txn_begin (dbfs->env, NULL, & txn->db_txn, dbfs_txn_db_flags (flags))))
    {
      dbfs_generate_int_event (EC_DbfsDbTxnBegin, err);
      return NULL;
    }

  return txn;
}

void
dbfs_txn_abort (RepoTxn *txn)
{
  int err;

  /* @@@ Handle DBFS_TXN_FLAT here */
  /* @@@  allocator_free (txn->alloc); */

  if (txn->parent == NULL)
    txn->dbfs->root_txns = g_slist_remove (txn->dbfs->root_txns, txn);
  else
    txn->parent->children = g_slist_remove (txn->parent->children, txn);

  if (txn->db_txn && (err = txn_abort (txn->db_txn)))
    dbfs_generate_int_event (EC_DbfsDbTxnAbort, err);

  txn->db_txn = NULL;

  g_free (txn);
}

gboolean
dbfs_txn_commit  (RepoTxn *txn)
{
  int err;

  g_assert (txn->db_txn);

  if (txn->parent == NULL)
    txn->dbfs->root_txns = g_slist_remove (txn->dbfs->root_txns, txn);
  else
    txn->parent->children = g_slist_remove (txn->parent->children, txn);

  g_assert (! txn->children);

  if (! dbfs_txn_pre_commit (txn))
    return FALSE;

  if ((txn->flags & DBFS_TXN_FLAT) && (txn->flags & DBFS_TXN_NESTED))
    {
      /* nothing */
    }
  else
    {
      if ((err = txn_commit (txn->db_txn, 0)))
	{
	  dbfs_generate_int_event (EC_DbfsDbTxnCommit, err);
	  return FALSE;
	}
    }

  dbfs_shift_idsets_up (txn);

  g_free (txn);

  return TRUE;
}

RepoTxn*
dbfs_txn_nest    (RepoTxn *parent)
{
  RepoTxn* txn = g_new0 (RepoTxn, 1);
  int err;

  txn->parent = parent;
  txn->dbfs = parent->dbfs;
  txn->flags = parent->flags | DBFS_TXN_NESTED;
  txn->alloc = parent->alloc;

  dbfs_shift_idsets_down (txn);

  if (parent->flags & DBFS_TXN_FLAT)
    {
      txn->db_txn = parent->db_txn;
    }
  else
    {
      if ((err = txn_begin (parent->dbfs->env, parent->db_txn, & txn->db_txn, 0)))
	{
	  dbfs_generate_int_event (EC_DbfsDbTxnBegin, err);
	  return NULL;
	}
    }

  parent->children = g_slist_prepend (parent->children, txn);

  return txn;
}

void
dbfs_use_txn (RepoTxn *txn)
{
  g_assert (! txn->children);
}

const char*
eventdelivery_path_to_string (Path* x)
{
  GString *res = g_string_new (NULL);
  const char* res_str;

  path_to_host_string (_print_pthn, x, res);

  res_str = res->str;

  g_string_free (res, FALSE);

  return res_str;
}

void
dbfs_clear_dbts (DBT* one,
		 DBT* two)
{
  if (one) memset (one, 0, sizeof (*one));
  if (two) memset (two, 0, sizeof (*two));
}

void
dbfs_init_u64_key (DBT* key, guint64 *key_be, guint64 key_native)
{
  (* key_be) = key_native;

  key->data = key_be;
  key->size = sizeof (guint64);
}

gboolean
dbfs_inum_allocate (RepoTxn  *txn,
		    guint64  *inum)
{
  DBT key, data;
  MajorInode mi;
  db_recno_t rn;
  int err;

  mi.ino_refcount = 0;

  dbfs_clear_dbts (& key, & data);

  key.data = & rn;
  key.size = sizeof (db_recno_t);
  key.ulen = sizeof (db_recno_t);
  key.flags = DB_DBT_USERMEM;

  data.data = & mi;
  data.size = sizeof (mi);

  dbfs_use_txn (txn);

  if ((err = txn->dbfs->major_inodes_dbp->put (txn->dbfs->major_inodes_dbp,
					       txn->db_txn,
					       & key,
					       & data,
					       DB_APPEND)))
    {
      dbfs_generate_int_event (EC_DbfsDbPut, err);
      return FALSE;
    }

  (* inum) = rn;

  idset_add (txn->unref_inums, *inum, 0);

  return TRUE;
}

ViewDef*
dbfs_view_definition_find (guint64 view_id)
{
  gint view_id_int = view_id;

  if (! _dbfs_views)
    return NULL;

  return g_hash_table_lookup (_dbfs_views, & view_id_int);
}

ViewDef*
dbfs_view_definition (guint64    view_id,
		      guint      view_private_size,
		      ViewBegin *view_begin,
		      ViewPgin  *view_pgin,
		      ViewEnd   *view_end)
{
  ViewDef* vd = g_new0 (ViewDef, 1);

  if (! _dbfs_views)
    _dbfs_views = g_hash_table_new (g_int_hash, g_int_equal);

  vd->view_id_int = vd->view_id = view_id;
  vd->view_private_size = view_private_size;
  vd->view_begin = view_begin;
  vd->view_pgin = view_pgin;
  vd->view_end = view_end;

  g_hash_table_insert (_dbfs_views, & vd->view_id_int, vd);

  return vd;
}

/* Link stuff
 */

gboolean
dbfs_getlink (RepoTxn  *txn,
	      Inode    *cont_inop,
	      BaseName *name,
	      guint64  *inum)
{
  DB* dbp = NULL;
  DBT key, data;
  int err;

  if (! (dbp = dbfs_use_db (txn->alloc, txn->dbfs, cont_inop)))
    goto abort;

  dbfs_clear_dbts (& key, & data);

  key.data = name->data;
  key.size = name->len;

  data.data = inum;
  data.ulen = sizeof (*inum);
  data.flags = DB_DBT_USERMEM;

  dbfs_use_txn (txn);

  if ((err = dbp->get (dbp, txn->db_txn, & key, & data, 0)))
    {
      if (err == DB_NOTFOUND || err == DB_KEYEMPTY)
	(* inum) = INODE_NOTFOUND;
      else
	{
	  dbfs_generate_int_event (EC_DbfsDbGet, err);
	  goto abort;
	}
    }

  dbfs_unuse_db (txn->dbfs, cont_inop, dbp);

  return TRUE;

 abort:

  if (dbp)
    dbfs_unuse_db (txn->dbfs, cont_inop, dbp);

  return FALSE;
}

gboolean
dbfs_putlink (RepoTxn  *txn,
	      Inode    *cont_inop,
	      BaseName *bn,
	      guint64   inum,
	      Overwrite ow)
{
  DB* dbp = NULL;
  DBT key, data;
  int err;

  if (! (dbp = dbfs_use_db (txn->alloc, txn->dbfs, cont_inop)))
    goto abort;

  dbfs_clear_dbts (& key, & data);

  key.data = bn->data;
  key.size = bn->len;

  data.data = & inum;
  data.size = sizeof (inum);

  dbfs_use_txn (txn);

  if ((err = dbp->put (dbp, txn->db_txn, & key, & data, (ow == DbfsNoOverwrite) ? DB_NOOVERWRITE : 0)))
    {
      if (err == DB_KEYEXIST)
	{
	  dbfs_generate_void_event (EC_DbfsAlreadyExists);
	  goto abort;
	}
      else
	{
	  dbfs_generate_int_event (EC_DbfsDbPut, err);
	  goto abort;
	}
    }

  dbfs_unuse_db (txn->dbfs, cont_inop, dbp);

  return TRUE;

 abort:

  if (dbp)
    dbfs_unuse_db (txn->dbfs, cont_inop, dbp);

  return FALSE;
}

gboolean
dbfs_putlink_next (RepoTxn  *txn,
		   Inode    *cont_inop,
		   guint64   inum)
{
  DB* dbp = NULL;
  DBT key, data;
  db_recno_t rn;
  int err;

  dbfs_clear_dbts (& key, & data);

  if (! (dbp = dbfs_use_db (txn->alloc, txn->dbfs, cont_inop)))
    goto abort;

  key.data = & rn;
  key.size = sizeof (rn);
  key.ulen = sizeof (rn);
  key.flags = DB_DBT_USERMEM;

  data.data = & inum;
  data.size = sizeof (inum);

  dbfs_use_txn (txn);

  if ((err = dbp->put (dbp, txn->db_txn, & key, & data, DB_APPEND)))
    {
      dbfs_generate_int_event (EC_DbfsDbPut, err);
      goto abort;
    }

  dbfs_unuse_db (txn->dbfs, cont_inop, dbp);

  return TRUE;

 abort:

  if (dbp)
    dbfs_unuse_db (txn->dbfs, cont_inop, dbp);

  return FALSE;
}

gboolean
dbfs_sequence_max (RepoTxn  *txn,
		   Inode    *inop,
		   Inode    *max_out)
{
  DB* dbp = NULL;
  DBT key, data;
  DBC* dbc = NULL;
  db_recno_t rn;
  guint64 inum;

  int err;

  if (! dbfs_checktype (txn, inop, FT_Sequence))
    goto abort;

  dbfs_clear_dbts (& key, & data);

  if (! (dbp = dbfs_use_db (txn->alloc, txn->dbfs, inop)))
    goto abort;

  if ((err = dbp->cursor (dbp, txn->db_txn, & dbc, 0)))
    {
      dbfs_generate_int_event (EC_DbfsDbCursor, err);
      goto abort;
    }

  key.data = & rn;
  key.size = sizeof (rn);
  key.ulen = sizeof (rn);
  key.flags = DB_DBT_USERMEM;

  data.data = & inum;
  data.size = sizeof (inum);
  data.ulen = sizeof (inum);
  data.flags = DB_DBT_USERMEM;

  if ((err = dbc->c_get (dbc, & key, & data, DB_LAST)))
    {
      if (err == DB_NOTFOUND)
	{
	  max_out->minor.ino_type = FT_NotFound;
	  goto done;
	}

      dbfs_generate_int_event (EC_DbfsDbCursorGet, err);
      goto abort;
    }

  memset (max_out, 0, sizeof (Inode));

  max_out->key.inum = inum;
  max_out->key.name = _dbfs_default_name;
  max_out->key.stck = DBFS_DEFAULT_STACK_ID;

  if (! dbfs_getnode (txn, max_out, FALSE))
    goto abort;

 done:

  if ((err = dbc->c_close (dbc)))
    {
      dbc = NULL;
      dbfs_generate_int_event (EC_DbfsDbCursorClose, err);
      goto abort;
    }

  dbfs_unuse_db (txn->dbfs, inop, dbp);

  return TRUE;

 abort:

  if (dbc)
    dbc->c_close (dbc);

  if (dbp)
    dbfs_unuse_db (txn->dbfs, inop, dbp);

  return FALSE;
}


/* DB cache
 */

static int
dbfs_cont_dbtype (Inode *cont_inop)
{
  int type = cont_inop->minor.ino_type;

  g_assert (type & FT_IsContainer);

  if (type & FT_IsNumeric)
#ifdef DBFS_ONE_BIG_DB
    return DB_RECNO;
#else
    return DB_QUEUE;
#endif

  if (cont_inop->minor.ino_flags & IF_Ordered)
    return DB_BTREE;

  return DB_HASH;
}

#ifdef DBFS_ONE_BIG_DB
static void
dbfs_subdb_string (guint64 ckey, GString *s)
{
  g_string_sprintf (s, "%qd", ckey);
}
#endif

static OpenDB*
dbfs_open_db (Allocator* alloc,
	      DBFS  *dbfs,
	      Inode *cont_inop,
	      int    dbflags)
{
  OpenDB *odb = g_new0 (OpenDB, 1);
  int err;
  GString *fsp = g_string_new (NULL);
  int dbtype = dbfs_cont_dbtype (cont_inop);

#ifndef DBFS_ONE_BIG_DB
  Path *p;

  if (! (p = sequence_fsloc_path (alloc, cont_inop->minor.ino_content_key)))
    goto abort;

  path_to_host_string (_fs_pthn, p, fsp);
#else
  if (dbflags & DB_CREATE && cont_inop->minor.ino_content_key != 1)
    dbflags &= ~(DB_EXCL);

  dbfs_subdb_string (cont_inop->minor.ino_content_key, fsp);
#endif

  if ((err = db_create (& odb->dbp, dbfs->env, 0)))
    {
      dbfs_generate_int_event (EC_DbfsDbCreate, err);
      goto abort;
    }

  if (dbtype == DB_QUEUE)
    {
      if ((err = odb->dbp->set_re_len (odb->dbp, sizeof (guint64))))
	{
	  dbfs_generate_int_event (EC_DbfsDbSet, err);
	  goto abort;
	}
    }

  if (dbtype == DB_HASH)
    {
      /* @@@ Warning: hard coded values for the XDFS tests: small tables.
       * 16 byte keys, 8 byte values, formula given in ref man.
       */
      if ((err = odb->dbp->set_h_ffactor (odb->dbp, 255)))
	{
	  dbfs_generate_int_event (EC_DbfsDbSet, err);
	  goto abort;
	}
    }

  if ((err = odb->dbp->open (odb->dbp,
#ifndef DBFS_ONE_BIG_DB
			     fsp->str,
			     NULL,
#else
			     "alldb",
			     fsp->str,
#endif
			     dbtype,
			     dbflags,
			     0666)))
    {
      dbfs_generate_int_event (EC_DbfsDbOpen, err);
      goto abort;
    }

  dbfs->open_db_count += 1;

  while (dbfs->open_db_count > DBFS_MAX_OPEN_DBS)
    {
      GList  *last = g_list_last (dbfs->open_dbs);
      OpenDB *lodb = last->data;

      if (lodb->refcount > 0)
	break;

      dbfs->open_dbs = g_list_remove_link (dbfs->open_dbs, last);

      g_list_free_1 (last);

      if ((err = lodb->dbp->close (lodb->dbp, 0)))
	{
	  dbfs_generate_int_event (EC_DbfsDbClose, err);
	  goto abort;
	}

      g_free (lodb);

      dbfs->open_db_count -= 1;
    }

  dbfs->open_dbs = g_list_prepend (dbfs->open_dbs, odb);
  odb->ino       = *cont_inop;
  odb->refcount  = 0;

  g_string_free (fsp, TRUE);

  return odb;

 abort:

  g_free (odb);
  g_string_free (fsp, TRUE);

  return NULL;
}

gboolean
dbfs_init_db (RepoTxn *txn,
	      Inode *cont_inop)
{
  OpenDB* odb;

  g_assert (cont_inop->minor.ino_content_key == 0);

#ifndef DBFS_ONE_BIG_DB
  if (! sequence_allocate_fsloc (txn->dbfs, & cont_inop->minor.ino_content_key))
    return FALSE;
#else
  if (! sequence_allocate (txn->dbfs, SEQUENCE_KEY_DBNAME, & cont_inop->minor.ino_content_key))
    return FALSE;
#endif

  idset_add (txn->unref_db_aborts, cont_inop->minor.ino_content_key, 0);

  odb = dbfs_open_db (txn->alloc, txn->dbfs, cont_inop, DB_CREATE | DB_EXCL | DB_THREAD);

  return odb != NULL;
}

DB*
dbfs_use_db  (Allocator *alloc,
	      DBFS      *dbfs,
	      Inode     *cont_inop)
{
  GList *l;
  OpenDB *odb;

  for (l = dbfs->open_dbs; l; l = l->next)
    {
      odb = l->data;

      if (cont_inop->key.inum == odb->ino.key.inum)
	{
	  odb->refcount += 1;

	  dbfs->open_dbs = g_list_remove_link (dbfs->open_dbs, l);
	  l->next = dbfs->open_dbs;
	  if (l->next)
	    l->next->prev = l;
	  dbfs->open_dbs = l;

	  return odb->dbp;
	}
    }

  odb = dbfs_open_db (alloc, dbfs, cont_inop, DB_THREAD);

  odb->refcount += 1;

  return odb->dbp;
}

void
dbfs_unuse_db (DBFS    *dbfs,
	       Inode   *cont_inop,
	       DB      *dbp)
{
  GList *l;
  OpenDB *odb;

  for (l = dbfs->open_dbs; l; l = l->next)
    {
      odb = l->data;

      if (cont_inop->key.inum == odb->ino.key.inum)
	{
	  g_assert (odb->refcount > 0);
	  odb->refcount -= 1;
	  return;
	}
    }

  abort ();
}

/* Node stuff
 */

void
dbfs_minor_key (DBT* key, GByteArray* a, Inode *inop)
{
  g_assert (inop->key.name);

  g_byte_array_append (a, (guint8*) & inop->key.inum, 8);
  g_byte_array_append (a, (guint8*) & inop->key.stck, 2);
  g_byte_array_append (a, inop->key.name->data, inop->key.name->len);

  if (key)
    {
      key->data = a->data;
      key->size = a->len;
    }
}

gboolean
dbfs_getnode (RepoTxn  *txn,
	      Inode    *inop,
	      gboolean  rmw)
{
  DBT key, data;
  int err;
  DB* dbp = txn->dbfs->minor_inodes_dbp;
  gboolean res = TRUE;
  GByteArray *a;

  if (inop->minor.ino_flags & IF_New)
    return TRUE;

  a = g_byte_array_new ();

  dbfs_clear_dbts (& key, & data);

  dbfs_minor_key (& key, a, inop);

  data.data = & inop->minor;
  data.ulen = sizeof (inop->minor);
  data.flags = DB_DBT_USERMEM;

  dbfs_use_txn (txn);

  if ((err = dbp->get (dbp, txn->db_txn, & key, & data, rmw ? DB_RMW : 0)))
    {
      if (err == DB_NOTFOUND)
	{
	  inop->minor.ino_type = FT_NotPresent;
	}
      else
	{
	  dbfs_generate_int_event (EC_DbfsDbGet, err);
	  res = FALSE;
	}
    }

  g_byte_array_free (a, TRUE);

  return res;
}

gboolean
dbfs_putnode (RepoTxn  *txn,
	      Inode    *inop,
	      gboolean  nooverwrite)
{
  DBT key, data;
  int err;
  DB* dbp = txn->dbfs->minor_inodes_dbp;
  gboolean res = TRUE;
  GByteArray *a = g_byte_array_new ();

  dbfs_clear_dbts (& key, & data);

  dbfs_minor_key (& key, a, inop);

  data.data = & inop->minor;
  data.size = sizeof (inop->minor);

  dbfs_use_txn (txn);

  if ((err = dbp->put (dbp, txn->db_txn, & key, & data, nooverwrite ? DB_NOOVERWRITE : 0)))
    {
      dbfs_generate_int_event (EC_DbfsDbPut, err);
      res = FALSE;
    }

  g_byte_array_free (a, TRUE);

  return res;
}

gboolean
dbfs_delnode (RepoTxn  *txn,
	      Inode    *inop)
{
  DBT key;
  int err;
  DB* dbp = txn->dbfs->minor_inodes_dbp;
  gboolean res = TRUE;
  GByteArray *a = g_byte_array_new ();

  dbfs_clear_dbts (& key, NULL);

  dbfs_minor_key (& key, a, inop);

  dbfs_use_txn (txn);

  if ((err = dbp->del (dbp, txn->db_txn, & key, 0)))
    {
      dbfs_generate_int_event (EC_DbfsDbDel, err);
      res = FALSE;
    }

  g_byte_array_free (a, TRUE);

  return res;
}

/* Short stuff
 */

gboolean
dbfs_putshort (RepoTxn      *txn,
	       Inode        *inop,
	       const guint8 *short_buf)
{
  DBT key, data;
  int err;
  DB* dbp = txn->dbfs->shorts_dbp;
  gboolean res = TRUE;
  db_recno_t rn;

  dbfs_clear_dbts (& key, & data);

  key.data = & rn;
  key.size = sizeof (db_recno_t);
  key.ulen = sizeof (db_recno_t);
  key.flags = DB_DBT_USERMEM;

  data.data = (void*) short_buf;
  data.size = inop->minor.ino_segment_len;

  dbfs_use_txn (txn);

  if ((err = dbp->put (dbp, txn->db_txn, & key, & data, DB_APPEND)))
    {
      dbfs_generate_int_event (EC_DbfsDbPut, err);
      res = FALSE;
    }

  inop->minor.ino_content_key = rn;

  return res;
}

gboolean
dbfs_getshort (RepoTxn *txn,
	       Inode   *inop,
	       guint8  *short_buf)
{
  DBT key, data;
  int err;
  DB* dbp = txn->dbfs->shorts_dbp;
  gboolean res = TRUE;
  db_recno_t rn = inop->minor.ino_content_key;

  dbfs_clear_dbts (& key, & data);

  key.data = & rn;
  key.size = sizeof (db_recno_t);
  key.ulen = sizeof (db_recno_t);
  key.flags = DB_DBT_USERMEM;

  data.data = short_buf;
  data.ulen = inop->minor.ino_segment_len;
  data.flags = DB_DBT_USERMEM;

  dbfs_use_txn (txn);

  if ((err = dbp->get (dbp, txn->db_txn, & key, & data, 0)))
    {
      dbfs_generate_int_event (EC_DbfsDbGet, err);
      res = FALSE;
    }

  return res;
}

gboolean
dbfs_delshort (RepoTxn *txn,
	       Inode   *inop)
{
  DBT key;
  int err;
  DB* dbp = txn->dbfs->shorts_dbp;
  gboolean res = TRUE;
  db_recno_t rn = inop->minor.ino_content_key;

  dbfs_clear_dbts (& key, NULL);

  key.data = & rn;
  key.size = sizeof (db_recno_t);
  key.ulen = sizeof (db_recno_t);
  key.flags = DB_DBT_USERMEM;

  dbfs_use_txn (txn);

  if ((err = dbp->del (dbp, txn->db_txn, & key, 0)))
    {
      dbfs_generate_int_event (EC_DbfsDbDel, err);
      res = FALSE;
    }

  return res;
}

/* Refcount maintenence
 */

gboolean
dbfs_inode_decr (RepoTxn *txn,
		 guint64  inum)
{
  DBT key, data;
  MajorInode mi;
  db_recno_t rn = inum;
  DB* dbp = txn->dbfs->major_inodes_dbp;
  int err;

  dbfs_clear_dbts (& key, & data);

  key.data = & rn;
  key.size = sizeof (db_recno_t);

  data.data = & mi;
  data.ulen = sizeof (mi);
  data.flags = DB_DBT_USERMEM;

  dbfs_use_txn (txn);

  if ((err = dbp->get (dbp, txn->db_txn, & key, & data, DB_RMW)))
    {
      dbfs_generate_int_event (EC_DbfsDbGet, err);
      return FALSE;
    }

  mi.ino_refcount -= 1;

  if (mi.ino_refcount == 0)
    {
      idset_add (txn->unref_inums, inum, 0);
    }

  if ((err = dbp->put (dbp, txn->db_txn, & key, & data, 0)))
    {
      dbfs_generate_int_event (EC_DbfsDbGet, err);
      return FALSE;
    }

  return TRUE;
}

gboolean
dbfs_inode_incr (RepoTxn *txn,
		 guint64  inum)
{
  DBT key, data;
  MajorInode mi;
  db_recno_t rn = inum;
  DB* dbp = txn->dbfs->major_inodes_dbp;
  int err;

  dbfs_clear_dbts (& key, & data);

  key.data = & rn;
  key.size = sizeof (db_recno_t);

  data.data = & mi;
  data.ulen = sizeof (mi);
  data.flags = DB_DBT_USERMEM;

  dbfs_use_txn (txn);

  if ((err = dbp->get (dbp, txn->db_txn, & key, & data, DB_RMW)))
    {
      dbfs_generate_int_event (EC_DbfsDbGet, err);
      return FALSE;
    }

  if (mi.ino_refcount == 0)
    {
      if (! idset_del (txn->unref_inums, inum, NULL))
	return FALSE;
    }

  mi.ino_refcount += 1;

  if ((err = dbp->put (dbp, txn->db_txn, & key, & data, 0)))
    {
      dbfs_generate_int_event (EC_DbfsDbGet, err);
      return FALSE;
    }

  return TRUE;
}
