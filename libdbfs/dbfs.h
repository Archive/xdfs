/* -*-Mode: C;-*-
 * $Id: dbfs.h 1.21 Wed, 03 May 2000 18:09:06 -0700 jmacd $
 *
 * Copyright (C) 1997, 1998, 1999, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#ifndef _DBFS_H_
#define _DBFS_H_

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <db.h>
#include <edsio.h>

typedef struct _DBFS                    DBFS;
typedef struct _Path                    Path;
typedef struct _RepoTxn                 RepoTxn;
typedef struct _BaseName                BaseName;
typedef struct _MinorInodeKey           MinorInodeKey;
typedef struct _MajorInode              MajorInode;
typedef struct _MinorInode              MinorInode;
typedef struct _Inode                   Inode;
typedef struct _WriteHandle             WriteHandle;
typedef struct _ReadHandle              ReadHandle;
typedef struct _PathToHostNative        PathToHostNative;
typedef struct _Cursor                  Cursor;
typedef struct _ViewDef                 ViewDef;
typedef struct _SkipList                SkipList;
typedef struct _SkipListNode            SkipListNode;
typedef struct _OpenDB                  OpenDB;
typedef struct _IDSet                   IDSet;
typedef struct _Allocator               Allocator;
typedef struct _RcsWalker               RcsWalker;
typedef struct _RcsFile                 RcsFile;
typedef struct _RcsVersion              RcsVersion;
typedef struct _RcsStats                RcsStats;
typedef struct _IntStat                 IntStat;
typedef struct _DblStat                 DblStat;
typedef struct _BinCounter              BinCounter;
typedef struct _LingerHandle            LingerHandle;

typedef struct _XdfsViewStats           XdfsViewStats;
typedef struct _XdfsParams              XdfsParams;
typedef struct _SerialXdfsState         XdfsState;
typedef struct _ConfigOption            ConfigOption;

typedef int                             SkipListData;
typedef struct _IDSetElt                IDSetElt;

#include <dbfs_edsio.h>

struct _IDSetElt {
  guint64   id;
  int       data;
  IDSetElt *next;
};

struct _IDSet {
  const char* name;
  IDSetElt *set;
};

enum _PathPrinter {
  PP_Auto,
  PP_String,
  PP_Hex
};

typedef enum _PathPrinter PathPrinter;

/* Type flag masks: all file types */
#define FT_IsAny        (0)
#define FT_IsPresent    (1 << 0)

/* Type flag masks: inode format */
#define FT_IsContainer  (1 << 1)
#define FT_IsSegment    (1 << 2)

/* Type flag masks: container types */
#define FT_IsWeak       (1 << 3)
#define FT_IsNumeric    (1 << 4)
#define FT_IsNonNumeric (1 << 5)
#define FT_IsLinked     (1 << 6)

/* Type flag masks: segment types */
#define FT_CanRead      (1 << 7)
#define FT_CanReplace   (1 << 8)

enum _FileType {
  /* Specific types */

  FT_Invalid      =                                                                              (1 << 10),
  FT_NotFound     = /* A name lookup didn't find an inode */                                     (1 << 11),
  FT_Null         = /* A name lookup found a null inode */                                       (1 << 12),
  FT_NotPresent   = FT_CanReplace /* A major inode was found, but no minor */                  | (1 << 13),
  FT_Directory    = FT_IsPresent | FT_IsContainer | FT_IsLinked | FT_IsNonNumeric              | (1 << 14),
  FT_Sequence     = FT_IsPresent | FT_IsContainer | FT_IsLinked | FT_IsNumeric                 | (1 << 15),
  FT_Index        = FT_IsPresent | FT_IsContainer | FT_IsLinked | FT_IsNonNumeric | FT_IsWeak  | (1 << 16),
  FT_Relation     = FT_IsPresent | FT_IsContainer                                              | (1 << 17),
  FT_Symlink      = FT_IsPresent | FT_IsSegment                                                | (1 << 18),
  FT_Segment      = FT_IsPresent | FT_IsSegment | FT_CanRead | FT_CanReplace                   | (1 << 19),
  FT_Indirect     = FT_IsPresent | FT_IsSegment                                                | (1 << 20),
  FT_View         = FT_IsPresent | FT_IsSegment | FT_CanRead                                   | (1 << 21)
};

enum _InodeFlags {
  IF_Ordered   = (1 << 0),       /* If set on a container, it is a
				  * B+Tree, otherwise it is hashed
				  * (Unordered). */
  IF_Short     = (1 << 1),       /* If set on a segment, content
				  * resides in the shorts db. */
  IF_New       = (1 << 2)        /* Newly created... */
};

struct _MajorInode { /* 8 bytes */
  guint64       ino_refcount;      /* Number of (hard) links */
};

struct _MinorInode { /* 24 bytes */
  guint32       ino_type;          /* See enum FileType */
  guint16       ino_stack_id;      /* For views, the stack_id */
  guint16       ino_flags;         /* See enum InodeFlags */
  guint64       ino_content_key;   /* The key of a long/db (file
				    * system) or short (shorts
				    * db) */
  guint64       ino_segment_len;   /* If IsSegment, the length */
};

struct _MinorInodeKey {
  guint64   inum;
  guint16   stck;
  BaseName *name;
};

struct _Inode {
  MinorInodeKey key;
  MinorInode    minor;
};

struct _OpenDB {
  /*OpenDB *next;*/
  Inode   ino;
  DB     *dbp;
  int     refcount;
};

struct _DBFS {

  /* Variables set once in dbfs_initialize.
   */
  DB *major_inodes_dbp;
  DB *minor_inodes_dbp;
  DB *shorts_dbp;
  DB *sequences_dbp;

  Inode root_ino;

  DB_ENV *env;

  guint32  fs_page_size;
  Path    *fs_base;

  /* @@@ Variables that need thread-safety...
   */
  GList  *open_dbs;
  int     open_db_count;

  GSList *root_txns;

  guint64  seq_last_path_val;
  gboolean seq_once;
};

struct _RepoTxn {
  RepoTxn    *parent;
  DB_TXN     *db_txn;
  DBFS       *dbfs;
  GSList     *children;
  int         flags;
  Allocator  *alloc;

  IDSet      *unref_inums;        /* Inums that are currently unreferenced
				   * in this txn, such as new allocations */

  IDSet      *unref_fs_aborts;    /* Files that will be unreferenced if this txn aborts */
  IDSet      *unref_fs_commits;   /* Files that will be unreferenced if this txn commits */

  IDSet      *unref_db_aborts;    /* DBs that will be unreferenced if this txn aborts */
  IDSet      *unref_db_commits;   /* DBs that will be unreferenced if this txn commits */
};

struct _BaseName {
  guint8  *data;
  guint32  len;
};

enum _PathType {
  PT_PathAbsolute, /* Path begins at the root. */
  PT_PathRelative, /* Path is relative: plevels is the number of parent
		    * links preceding. */
  PT_PathName      /* An ordinary path element. */
};

struct _PathToHostNative {
  const char *root_sep;
  int         root_sep_len;

  const char *sep;
  int         sep_len;

  const char *this_dir;
  int         this_dir_len;

  const char *parent_dir;
  int         parent_dir_len;

  void  (* bths) (PathToHostNative* pthn, PathPrinter pp, BaseName* bn, GString* result);
  Path* (* fhs)  (Allocator* alloc, PathToHostNative* pthn, const char* s);
};

struct _RcsWalker {
  void*    (* initialize)    (void);
  gboolean (* finalize)      (RcsStats* stats, void* data);
  gboolean (* onefile)       (RcsFile* rcs, RcsStats* stats, void* data);
  gboolean (* dateorder)     (RcsFile* rcs, RcsVersion* v, void* data);
  gboolean (* delta_orig)    (RcsFile* rcs, RcsVersion* from, RcsVersion *to, void* data);
  gboolean (* delta_date)    (RcsFile* rcs, RcsVersion* from, RcsVersion *to, void* data);
  int      min_versions;
  int      max_versions;
  gboolean write_files;
};

struct _RcsVersion {
  RcsFile    *rcs;
  time_t      date;
  int         dateseq;
  int         chain_length;
  char       *vname;
  off_t       size;
  int         cc;
  guint8*     segment;
  char       *filename;
  RcsVersion *parent;
  GSList     *children;
  guint       on_trunk : 1;
};

struct _RcsFile {
  char       *filename;
  char       *copyname;
  char       *headname;

  int         version_count;
  int         forward_count;
  int         reverse_count;
  int         branch_count;

  RcsVersion *versions;
  RcsVersion **versions_date;

  RcsVersion *head_version;
  RcsVersion *root_version;

  off_t       total_size;

  guint       atflag : 1;
};

struct _RcsStats {
  BinCounter *avg_version_size;
  IntStat* version_stat;
  IntStat* forward_stat;
  IntStat* reverse_stat;
  IntStat* branch_stat;
  IntStat* unencoded_stat;
  IntStat* literal_stat;
};

struct _IntStat {
  const char* name;
  int count;
  long long sum;
  long long min;
  long long max;

  GArray *values;
};

struct _DblStat {
  const char* name;
  int count;
  double sum;
  double min;
  double max;

  GArray *values;
};

struct _BinCounter {
  const char *name;
  GPtrArray  *bins;
};

enum _ConfigArgument {
  CO_Required,
  CO_Optional,
  CO_None
};

typedef enum _ConfigArgument ConfigArgument;

enum _ConfigOptionType {
  CD_Bool,
  CD_Int32,
  CD_Double,
  CD_String
};

typedef enum _ConfigOptionType ConfigOptionType;

enum _ConfigStyle {
  CS_Ignore,
  CS_UseAsFile,
  CS_Use
};

typedef enum _ConfigStyle ConfigStyle;

struct _ConfigOption {
  const char       *name;
  const char       *abbrev;
  ConfigStyle       style;
  ConfigArgument    arg;
  ConfigOptionType  type;
  void             *value;
  gboolean          found;
};

typedef gboolean (ViewBegin) (RepoTxn *txn,
			     Inode   *inop,
			     void    *private);

typedef gboolean (ViewEnd)   (RepoTxn *txn,
			      Inode   *inop,
			      void    *private);

typedef gboolean (ViewPgin)  (RepoTxn *txn,
			      Inode   *inop,
			      void    *private,
			      guint8  *pgbuf,
			      gsize    offset,
			      gsize    len);

struct _ViewDef {
  gint       view_id_int; /* Bogus for glib hash table */
  guint64    view_id; /* Used as the content_key in the view's inode */
  guint      view_private_size;
  ViewBegin *view_begin;
  ViewPgin  *view_pgin;
  ViewEnd   *view_end;
};

enum _CursorType {
  CT_MinorInodes,  /* Range query on index number in inodes table, retrieves all segments. */
  CT_Container /* Some query on an indexed relation. */
};

typedef enum _CursorType CursorType;

struct _Cursor {
  DB         *dbp;
  DB         *unuse_dbp;
  DBC        *dbc;
  Inode       inop;
  RepoTxn    *parent_txn;
  CursorType  type;

  BaseName    bn_tmp;

  gboolean    prefix_first;
  GByteArray *prefix_key;

  gboolean    failure;

  GByteArray *key_array;
  guint       key_len;
  GByteArray *data_array;
  guint       data_len;
};

struct _LingerHandle {
  RepoTxn    *txn;
  GHashTable *table;
  int         unique;
};

enum _ListFlags {
  LF_ShowNamed       = (1 << 1),
  LF_ShowStacked     = (1 << 2),
  LF_Recursive       = (1 << 3),
  LF_NotRecursive    = (1 << 4)
};

#define XP_IsEvolving  (1 << 0)
#define XP_IsReverse   (1 << 1)
#define XP_IsForward   (1 << 2)

enum _XdfsPolicy {
  XP_NoCompress  = 1,
  XP_ReverseJump = XP_IsEvolving | XP_IsReverse,
  XP_ForwardJump = XP_IsEvolving | XP_IsForward,
  XP_Forward     =                 XP_IsForward
};

typedef enum _XdfsPolicy  XdfsPolicy;

enum _XdfsFlags {
  XF_MD5Equality = (1 << 0),
  XF_SHAEquality = (1 << 1),
  XF_StoreDigest = (1 << 2)
};

typedef enum _XdfsFlags   XdfsFlags;

struct _XdfsParams {
  XdfsPolicy  policy;
  guint32     flags;
  guint32     cluster_max_versions;
  guint32     src_buffer_max_files;
  guint32     src_buffer_max_size;
  guint32     src_buffer_max_size_per_file;
  guint32     src_buffer_min_size_per_file;
};

enum _Follow {
  DbfsNoFollowLinks  = FALSE,
  DbfsFollowLinks = TRUE
};

enum _Overwrite {
  DbfsNoOverwrite = FALSE,
  DbfsOverwrite = TRUE
};

typedef enum _Overwrite   Overwrite;
typedef enum _ListFlags   ListFlags;
typedef enum _FileType    FileType;
typedef enum _Follow      Follow;
typedef enum _InodeFlags  InodeFlags;
typedef enum _PathType    PathType;

/* Constants
 */

#define ARRAY_SIZE(x) sizeof((x))/sizeof(*(x))

#define DBFS_DEFAULT_STACK_ID  0
#define DBFS_TX_MAX            65536 /* Since it uses a lot of child transactions... */
#define DBFS_LK_MAX            65536 /* Since it uses a lot of child transactions... */

#define INODE_NOTFOUND         0
#define INODE_NULL             1
#define INODE_ROOT             2

/* Initial values for the known sequences
 */

#define SEQUENCE_KEY_FSLOC 1
#define SEQUENCE_KEY_DBNAME 2

#define SEQUENCE_INIT_FSLOC  0
#define SEQUENCE_INIT_DBNAME 0

#define MAX_LINK_FOLLOW   20
#define FIRST_SEQUENCE_VALUE 0

extern PathToHostNative *_fs_pthn;
extern PathToHostNative *_print_pthn;
extern BaseName         *_dbfs_default_name;

Allocator*  allocator_new  (void);
void        allocator_free (Allocator* alloc);
void*       allocator_malloc (Allocator* alloc, guint size);
void*       allocator_calloc (Allocator* alloc, guint size);
void*       allocator_memdup (Allocator* alloc, const void* val, guint size);

#define alc_new(alloc,type)		((type*) allocator_malloc (alloc,sizeof (type)))
#define alc_new0(alloc,type)		((type*) allocator_calloc (alloc,sizeof (type)))

/* Path: DBFS independent selectors
 */
PathType    path_type                (Path* path);
Path*       path_dirname             (Allocator* alloc, Path* path);
BaseName*   path_basename            (Path* path); /* for named path elts */
guint32     path_parent_count        (Path* path); /* for relative path terminators */
gboolean    path_term                (Path* path); /* true for the terminators */

/* Path: Predicates
 */
gboolean    path_equal               (Path* path, Path* comp);

/* Path: To append single path elements.
 */
Path*       path_append_bytes        (Allocator* alloc, Path* path, PathPrinter print, const guint8* val, guint32 len);
Path*       path_append_basename     (Allocator* alloc, Path* path, PathPrinter print, BaseName *bn);
Path*       path_append_string       (Allocator* alloc, Path* path, PathPrinter print, const char* s);
Path*       path_append_format       (Allocator* alloc, Path* path, PathPrinter print, const char* fmt, ...);

/* Path: These are the three terminators.
 */
Path*       path_root                (Allocator* alloc);           /* / */
Path*       path_current             (Allocator* alloc);           /* ./ */
Path*       path_parent              (Allocator* alloc, guint32 levels); /* 3 => ./../../../ */

/* Path: Combinators
 */
Path*       path_append_path   (Allocator* alloc, Path* base, Path* append);
Path*       path_evaluate_path (Allocator* alloc, Path* base, Path* link);

/* Path: Conversions
 */
void        path_basename_to_canonical (BaseName *bn, GByteArray* result);
void        path_to_canonical          (Path *path, GByteArray* result);
Path*       path_from_canonical        (Allocator* alloc, const guint8* buf, guint32 len);

void        path_basename_to_host_string   (PathToHostNative* pthn, PathPrinter pp, BaseName* bn, GString* result);
void        path_to_host_string            (PathToHostNative* pthn, Path* path, GString* result);
Path*       path_from_host_string          (Allocator* alloc, PathToHostNative* pthn, const char* s);

/* Path: Basenames
 */
BaseName*  basename_new (Allocator    *alloc,
			 const guint8 *val,
			 guint         len);

/* Transactions
 */

#define DBFS_TXN_SYNC        (1 << 0)
#define DBFS_TXN_NOSYNC      (1 << 1)
#define DBFS_TXN_FLAT        (1 << 2)
#define DBFS_TXN_NESTED      (1 << 3)
#define DBFS_TXN_ABORTED     (1 << 4)
#define DBFS_TXN_NONBLOCKING (1 << 5)

gboolean    dbfs_library_init  (void);
gboolean    dbfs_library_close (void);
DBFS*       dbfs_create        (Path       *fs_base,
				Path       *fs_log_base);
DBFS*       dbfs_initialize    (Path       *fs_base);
gboolean    dbfs_close         (DBFS       *dbfs);
gboolean    dbfs_list          (RepoTxn    *txn,
				FileHandle *writeto,
				ListFlags   lf);

RepoTxn*    dbfs_txn_begin     (DBFS       *dbfs,
				int         flags);
void        dbfs_txn_abort     (RepoTxn    *txn);
gboolean    dbfs_txn_commit    (RepoTxn    *txn);
RepoTxn*    dbfs_txn_nest      (RepoTxn    *txn);

void        dbfs_set_fs_short_threshold (int val);

/* EDSIO stuff
 */

const char* eventdelivery_path_to_string  (Path* x);

/* Methods for locating and creating inodes.
 */

gboolean    dbfs_inode_new                        (RepoTxn  *txn,
					           Inode    *inop);

gboolean    dbfs_inode_find_stacked_named         (RepoTxn  *txn,
					           Inode    *root,
					           Path     *path,
						   guint16   stck,
						   BaseName *name,
						   Follow    follow_links,
					           FileType  type_mask,
					           Inode    *inop);

gboolean    dbfs_inode_find                       (RepoTxn  *txn,
					           Inode    *root,
					           Path     *path,
						   Follow    follow_links,
					           FileType  type_mask,
					           Inode    *inop);

gboolean    dbfs_inode_find_root                  (RepoTxn  *txn,
					           Path     *path,
						   Follow    follow_links,
					           FileType  type_mask,
					           Inode    *inop);

gboolean    dbfs_inode_find_seqno                 (RepoTxn  *txn,
					           Inode    *seq,
						   guint64   seqno,
						   Follow    follow_links,
					           FileType  type_mask,
					           Inode    *inop);

gboolean    dbfs_inode_delete                     (RepoTxn  *txn,
						   Inode    *inop);

gboolean    dbfs_inode_delete_named               (RepoTxn  *txn,
						   Inode    *inop,
						   BaseName *name);

gboolean    dbfs_inode_delete_stacked_named       (RepoTxn  *txn,
						   Inode    *inop,
						   guint16   stck,
						   BaseName *name);

/* Methods for containers
 */
gboolean    dbfs_make_sequence                    (RepoTxn  *txn,
						   Inode    *inop);

gboolean    dbfs_make_index                       (RepoTxn  *txn,
						   Inode    *inop,
						   gboolean  ordered);

gboolean    dbfs_make_directory                   (RepoTxn  *txn,
						   Inode    *inop,
						   gboolean  ordered);

gboolean    dbfs_sequence_max                     (RepoTxn  *txn,
						   Inode    *inop,
						   Inode    *max_out);

/* Methods for reading and replacing inode contents.
 */
FileHandle* dbfs_inode_open_replace               (RepoTxn  *txn,
					           Inode    *inop);

FileHandle* dbfs_inode_open_replace_named         (RepoTxn  *txn,
					           Inode    *inop,
					           BaseName *name,
						   Inode    *out);

FileHandle* dbfs_inode_open_replace_stacked       (RepoTxn  *txn,
					           Inode    *inop,
					           guint16   stack_id,
						   Inode    *out);

FileHandle* dbfs_inode_open_replace_stacked_named (RepoTxn  *txn,
						   Inode    *inop,
						   guint16   stack_id,
						   BaseName *name,
						   Inode    *out);

FileHandle* dbfs_inode_open_read                  (RepoTxn  *txn,
					           Inode    *inop);

FileHandle* dbfs_inode_open_read_named            (RepoTxn  *txn,
					           Inode    *inop,
					           BaseName *name,
						   Inode    *out);

FileHandle* dbfs_inode_open_read_stacked          (RepoTxn  *txn,
					           Inode    *inop,
					           guint16   stack_id,
						   Inode    *out);

FileHandle* dbfs_inode_open_read_stacked_named    (RepoTxn  *txn,
						   Inode    *inop,
						   guint16   stack_id,
						   BaseName *name,
						   Inode    *out);

FileHandle* dbfs_inode_open_linger                (LingerHandle *linger,
						   Inode        *inop);


/* Link operations
 */
gboolean    dbfs_link_create                      (RepoTxn  *txn,
					           Inode    *cont,
						   Inode    *inop,
						   BaseName *name,
						   gboolean  overwrite);

gboolean    dbfs_link_create_next                 (RepoTxn  *txn,
					           Inode    *cont,
					           Inode    *inop);

gboolean    dbfs_link_delete                      (RepoTxn  *txn,
						   Inode    *cont,
						   BaseName *name);

/* Cursor operations for containers
 */
Cursor*     dbfs_cursor_open                      (RepoTxn    *txn,
					           Inode      *inop,
						   CursorType  ctype);

gboolean    dbfs_cursor_inode                     (Cursor   *cursor,
						   FileType  type_mask,
					           Inode    *inop);

gboolean    dbfs_cursor_delete                    (Cursor   *cursor);

gboolean    dbfs_cursor_next                      (Cursor   *cursor);

gboolean    dbfs_cursor_close                     (Cursor   *cursor);

BaseName*   dbfs_cursor_basename                  (Cursor   *cursor);

gboolean    dbfs_cursor_inum                      (Cursor   *cursor,
						   guint64  *inum);

/* View operations
 */
gboolean    dbfs_make_view                        (RepoTxn  *txn,
					           Inode    *inop,
					           guint64   length,
						   guint16   stack_id,
					           ViewDef  *vd);

ViewDef*    dbfs_view_definition                  (guint64    view_id,
						   guint      view_private_size,
						   ViewBegin *begin,
						   ViewPgin  *pgin,
						   ViewEnd   *end);

ViewDef*    dbfs_view_definition_find             (guint64    view_id);

/* Lingering read handles
 */

LingerHandle* dbfs_linger_open                    (RepoTxn      *txn);

void          dbfs_linger_close                   (LingerHandle *linger);

/* Indirect link operations
 */
gboolean    dbfs_make_indirect_link               (RepoTxn  *txn,
					           Inode    *from,
					           Inode    *to);

gboolean    dbfs_make_indirect_link_named         (RepoTxn  *txn,
					           Inode    *from,
					           BaseName *name,
					           Inode    *to);

gboolean    dbfs_make_indirect_link_stacked_named (RepoTxn  *txn,
					           Inode    *from,
					           guint16   stack_id,
					           BaseName *name,
					           Inode    *to);

gboolean    dbfs_read_indirect_link               (RepoTxn  *txn,
					           Inode    *from,
					           FileType  type_mask,
					           Inode    *to);

gboolean    dbfs_read_indirect_link_named         (RepoTxn  *txn,
					           Inode    *from,
					           BaseName *name,
					           FileType  type_mask,
					           Inode    *to);

gboolean    dbfs_read_indirect_link_stacked_named (RepoTxn  *txn,
					           Inode    *from,
					           guint16   stack_id,
					           BaseName *name,
					           FileType  type_mask,
					           Inode    *to);

/* DBFS privates
 */

gboolean    dbfs_getlink                          (RepoTxn  *txn,
						   Inode    *cont_inop,
						   BaseName *name,
						   guint64  *inum);

gboolean    dbfs_putlink                          (RepoTxn  *txn,
						   Inode    *cont_inop,
						   BaseName *bn,
						   guint64   inum,
						   Overwrite ow);

gboolean    dbfs_putlink_next                     (RepoTxn  *txn,
						   Inode    *cont_inop,
						   guint64   inum);

gboolean    dbfs_dellink                          (RepoTxn  *txn,
						   Inode    *cont_inop,
						   BaseName *bn,
						   guint64   inum);

gboolean    dbfs_getnode                          (RepoTxn  *txn,
						   Inode    *inop,
						   gboolean  rmw);

gboolean    dbfs_putnode                          (RepoTxn  *txn,
						   Inode    *inop,
						   gboolean  nooverwrite);

gboolean    dbfs_delnode                          (RepoTxn  *txn,
						   Inode    *inop);

Path*       dbfs_read_short_path_segment          (RepoTxn  *txn,
						   Inode    *inop);

gboolean    dbfs_inum_allocate                    (RepoTxn  *txn,
						   guint64  *inum);

void        dbfs_clear_dbts                       (DBT* one,
						   DBT* two);

gboolean    dbfs_checktype                        (RepoTxn  *txn,
						   Inode    *inop,
						   FileType  type_mask);

void        dbfs_init_u64_key                     (DBT      *key,
						   guint64  *key_be,
						   guint64   key_native);

gboolean    dbfs_putshort                         (RepoTxn      *txn,
						   Inode        *inode,
						   const guint8 *short_buf);

gboolean    dbfs_getshort                         (RepoTxn *txn,
						   Inode   *inop,
						   guint8  *short_buf);

gboolean    dbfs_delshort                         (RepoTxn *txn,
						   Inode   *inop);

gboolean    dbfs_init_db                          (RepoTxn *txn,
						   Inode   *cont_inop);

DB*         dbfs_use_db                           (Allocator *alloc,
						   DBFS      *dbfs,
						   Inode     *cont_inop);

void        dbfs_unuse_db                         (DBFS    *dbfs,
						   Inode   *cont_inop,
						   DB      *dbp);

gboolean    dbfs_delete_db                        (DBFS    *dbfs,
						   guint64  inum);

void        dbfs_use_txn                          (RepoTxn *txn);

void        dbfs_minor_key                        (DBT        *key,
						   GByteArray *a,
						   Inode      *inop);

/* Reference count management stuff
 */

gboolean    dbfs_inode_decr                       (RepoTxn *txn,
						   guint64  inum);

gboolean    dbfs_inode_incr                       (RepoTxn *txn,
						   guint64  inum);

/* Sequences
 */

gboolean    sequence_allocate                     (DBFS    *dbfs,
						   guint64  seqkey,
						   guint64 *valp);

gboolean    sequence_allocate_fsloc               (DBFS    *dbfs,
						   guint64 *valp);

Path*       sequence_fsloc_path                   (Allocator *alloc,
						   guint64    key);

Path*       sequence_fsloc_absolute_path          (Allocator *alloc,
						   DBFS      *dbfs,
						   guint64    key);

gboolean    sequence_create_known                 (DBFS    *dbfs,
						   guint64  seqkey,
						   guint64  init_val);

/* IDSet stuff
 */

IDSet*      idset_new    (const char* name);
void        idset_free   (IDSet* ids);
void        idset_dmerge (IDSet* to, IDSet *free);
void        idset_add    (IDSet* ids, guint64 id, int data);
gboolean    idset_del    (IDSet* ids, guint64 id, int *data);

/* XDFS stuff
 */

gboolean            xdfs_library_init       (void);

gboolean            xdfs_location_create    (RepoTxn    *txn,
					     Path       *xdfs,
					     XdfsParams *params);

gboolean            xdfs_stat               (RepoTxn    *txn,
					     Path       *xdfs,
					     XdfsState  *state);

gboolean            xdfs_state_print        (FileHandle *handle,
					     Path       *xdfs,
					     XdfsState  *state);

gboolean            xdfs_insert_version     (RepoTxn    *txn,
					     Path       *xdfs,
					     FileHandle *in,
					     Inode      *inop_out);

gboolean            xdfs_container_sequence (RepoTxn    *txn,
					     Path       *xdfs,
					     Inode      *inop);

gboolean            xdfs_digest_index       (RepoTxn    *txn,
					     Path       *xdfs,
					     Inode      *inop);

gboolean            xdfs_extract_delta      (RepoTxn    *txn,
					     Path       *xdfs,
					     Inode      *from_inop,
					     Inode      *to_inop,
					     FileHandle *delta_out);

gboolean            xdfs_apply_delta        (FileHandle *from_in,
					     FileHandle *delta_in,
					     FileHandle *cons_out);

gboolean            xdfs_insert_delta       (RepoTxn    *txn,
					     Path       *xdfs,
					     Inode      *against_ino,
					     FileHandle *delta_in,
					     Inode      *result_out);

/* XDFS Skip Lists
 */
guint               xdfs_skip_list_insert (SkipList    *skp,
					   guint32      insert_offset,
					   guint32      insert_length,
					   SkipListData data);

SkipListNode*       xdfs_skip_list_search (SkipList* skp,
					   guint32   search_offset);

SkipListNode*       xdfs_skip_list_search_nearest (SkipList* skp,
						   guint32   search_offset);

SkipList*           xdfs_skip_list_new    (guint32   length);
void                xdfs_skip_list_free   (SkipList* skp);

void                xdfs_skip_list_print  (SkipList     *skp);

SkipListData        xdfs_skip_list_data   (SkipListNode* sln);

SkipListNode*       xdfs_skip_list_first  (SkipList     *skp);
SkipListNode*       xdfs_skip_list_next   (SkipListNode *sln);
guint               xdfs_skip_list_offset (SkipListNode *sln);
guint               xdfs_skip_list_length (SkipListNode *sln);

/* RCS inspection stuff
 */

void                rcswalk_init   (void);
gboolean            rcswalk        (RcsWalker *walker, const char* copy_base);
void                rcswalk_report (RcsStats* stats);

IntStat*            stat_int_new      (const char* name);
void                stat_int_add_item (IntStat* stat, long long v);
void                stat_int_report   (IntStat* stat);

DblStat*            stat_dbl_new      (const char* name);
void                stat_dbl_add_item (DblStat* stat, double v);
void                stat_dbl_report   (DblStat* stat);

BinCounter*         stat_bincount_new      (const char* name);
void                stat_bincount_add_item (BinCounter* bc, int bin, double val);
void                stat_bincount_report   (BinCounter* bc);

/* Experiment configuration stuff
 */

void                config_register   (ConfigOption *opts, int nopts);
gboolean            config_parse      (const char* config_file);
gboolean            config_done       (void);
void                config_help       (void);
void                config_set_string (const char* var, const char* val);
gboolean            config_clear_dir  (const char* dir);
gboolean            config_create_dir (const char* dir);
FILE*               config_output     (const char* fmt, ...);

#ifdef __cplusplus
}
#endif

#endif /* _DBFS_H_ */
