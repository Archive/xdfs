/* -*-Mode: C;-*-
 * $Id: dbfstest.c 1.1 Mon, 07 Feb 2000 12:42:45 -0800 jmacd $
 * file.c:
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include <sys/param.h>
#include <sys/stat.h>

#include <dirent.h>
#include <errno.h>
#include <fts.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "dbfs.h"
#include <edsiostdio.h>

gboolean
report_stats (int size, int count, char* path_str, gulong seconds, gulong micros)
{
  /* FreeBSD specific code taken from du.c */
  char          *argv[2];
  FTS		*fts;
  FTSENT        *p;
  long		 total_disk_usage = 0;
  long           total_file_space = size * count;

  argv[0] = path_str;
  argv[1] = NULL;

  if ((fts = fts_open(argv, FTS_PHYSICAL, NULL)) == NULL)
    goto abort;

  while ((p = fts_read(fts)) != NULL)
    {
      switch (p->fts_info)
	{
	case FTS_D:			/* Ignore. */
	  break;
	case FTS_DP:
	  p->fts_parent->fts_number += p->fts_number += p->fts_statp->st_blocks;
	  break;
	case FTS_DC:			/* Ignore. */
	  break;
	case FTS_DNR:			/* Warn, continue. */
	case FTS_ERR:
	case FTS_NS:
	  goto abort;
	default:
	  if (strncmp (p->fts_name, "__db", 4) != 0 &&
	      strncmp (p->fts_name, "log.", 4) != 0)
	    p->fts_parent->fts_number += p->fts_statp->st_blocks;
	}

      total_disk_usage = p->fts_parent->fts_number;
    }

  if (errno)
    goto abort;

  total_disk_usage *= 512;

  handle_printf (_stdout_handle, "real file space: %d Kb; dbfs file space: %d Kb; ratio: %f; time: %.2f\n",
		 total_disk_usage / 1024,
		 total_file_space / 1024,
		 (double) total_file_space / (double) total_disk_usage,
		 (double) seconds + (double) micros / 1000000.0);

  return TRUE;

 abort:

  abort ();
}

int
main (int argc, char ** argv)
{
  DBFS *dbfs = NULL;
  RepoTxn *txn = NULL;
  Path *path;
  int count, size, period, i;
  guint8 *buffer;
  GString *path_str;
  GTimer *timer = g_timer_new ();
  gulong seconds;
  gulong micros;

  if (! dbfs_library_init ())
    goto abort;

  if (argc != 5)
    {
      handle_printf (_stderr_handle, "usage: %s COUNT SIZE COMMIT_PERIOD PATH\n", argv[0]);
      return 2;
    }

  count = atoi (argv[1]);
  size = atoi (argv[2]);
  period = atoi (argv[3]);

  buffer = malloc (size);

  memset (buffer, 0x5a, size);

  if (! (path = path_from_host_string (NULL, _fs_pthn, argv[4])))
    goto abort;

  path_to_host_string (_fs_pthn, path, (path_str = g_string_new (NULL)));

  g_timer_start (timer);

  if (! (dbfs = dbfs_create (path)))
    goto abort;

  if (! (txn = dbfs_txn_begin (dbfs, 0)))
    goto abort;

  for (i = 0; i < count; i += 1)
    {
      Inode ino;
      FileHandle *fh;

      if (! dbfs_inode_new (txn, & ino))
	goto abort;

      if (! (fh = dbfs_inode_open_replace (txn, & ino)))
	goto abort;

      if (! handle_write (fh, buffer, size))
	goto abort;

      if (! handle_close (fh))
	goto abort;

      if ((i % period) == (period-1))
	{
	  if (! dbfs_txn_commit (txn))
	    goto abort;

	  if (! (txn = dbfs_txn_begin (dbfs, 0)))
	    goto abort;
	}

      handle_free (fh);
    }

  if (! dbfs_txn_commit (txn))
    goto abort;

  txn = NULL;

  if (! dbfs_close (dbfs))
    goto abort;

  dbfs = NULL;

  if (! dbfs_library_close ())
    return 2;

  g_timer_stop (timer);

  seconds = g_timer_elapsed (timer, & micros);

  if (! report_stats (size, count, path_str->str, seconds, micros))
    return 2;

  return 0;

 abort:

  if (txn)
    dbfs_txn_abort (txn);

  if (dbfs)
    dbfs_close (dbfs);

  dbfs_library_close ();

  return 2;
}
