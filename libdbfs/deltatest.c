/* -*-Mode: C;-*-
 * $Id: deltatest.c 1.11 Tue, 07 Mar 2000 07:10:54 -0800 jmacd $
 * file.c:
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "dbfs.h"
#include <xdelta.h>
#include <edsiostdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <math.h>

typedef struct _DeltaTest  DeltaTest;
typedef struct _DeltaStats DeltaStats;

FILE *big;

#define NTRIALS 4

struct _DeltaStats {
  IntStat* patch_stat;
  IntStat* control_stat;
  IntStat* delta_stat;
  DblStat* delta_frac_stat;

  double times[NTRIALS];
  double total_time;
  double total_time_variance;
};

#define DT_FLAG_NOCONTROL 1
#define DT_FLAG_DIFF_PENALTY 1

struct _DeltaTest {
  const char* name;
  gboolean (* delta) (RcsVersion* from, RcsVersion *to, DeltaTest *test, DeltaStats *stats, int round);
  int flags;
};

const char* tmp_file_3;
const char* tmp_file_4;

gint32       dtest_min_versions    = 0;
gint32       dtest_max_versions    = G_MAXINT;
const char*  dtest_method          = NULL;
const char*  dtest_mode            = NULL;

static ConfigOption options [] = {
  { "min_versions", "min",    CS_Use,    CO_Optional, CD_Int32,   & dtest_min_versions },
  { "max_versions", "max",    CS_Use,    CO_Optional, CD_Int32,   & dtest_max_versions },
  { "mode",         "md",     CS_Use,    CO_Required, CD_String,  & dtest_mode },
  { "method",       "mth",    CS_Use,    CO_Required, CD_String,  & dtest_method },
};

void
report_delta (RcsVersion *from, RcsVersion *to, DeltaTest *test, DeltaStats *stats, off_t c_size, off_t d_size)
{
  double frac;
  static int max_dsize = 0;
  static int max_fsize = 0;

  if (max_dsize < (c_size + d_size))
    {
      max_dsize = c_size + d_size;

      fprintf (big, "Largest delta so far: %s: %s -> %s: %d bytes\n", from->rcs->filename, from->vname, to->vname, max_dsize);
    }

  if (max_fsize < from->size)
    {
      max_fsize = from->size;

      fprintf (big, "Largest file so far: %s: %s: %d bytes\n", from->rcs->filename, from->vname, max_fsize);
    }

  if (max_fsize < to->size)
    {
      max_fsize = to->size;

      fprintf (big, "Largest file so far: %s: %s: %d bytes\n", to->rcs->filename, to->vname, max_fsize);
    }

  if (! stats)
    return;

  frac = (double) (c_size + d_size) / (double) to->size;

  stat_dbl_add_item (stats->delta_frac_stat, frac);
  stat_int_add_item (stats->delta_stat, c_size + d_size);
  stat_int_add_item (stats->control_stat, c_size);
  stat_int_add_item (stats->patch_stat, d_size);
}

gboolean
xdelta_test (RcsVersion *from, RcsVersion *to, DeltaTest *test, DeltaStats *stats, int round)
{
  XdeltaGenerator *gen = xdp_generator_new ();
  FileHandle *from_fh, *to_fh, *c_fh, *d_fh;
  XdeltaSource *from_src;
  XdeltaControl *control;
  struct stat buf;
  off_t d_size, c_size;

  if (! (from_fh = handle_read_file (from->filename)))
    g_error ("handle_read_file failed\n");

  if (! (from_src = xdp_source_new (from_fh, XS_Primary, NULL)))
    g_error ("xdp_source_new failed\n");

  xdp_source_add (gen, from_src);

  if (! (to_fh = handle_read_file (to->filename)))
    g_error ("handle_read_file failed\n");

  if (! (d_fh = handle_write_file (tmp_file_3)))
    g_error ("handle_write_file1 failed\n");

  if (! (c_fh = handle_write_file (tmp_file_4)))
    g_error ("handle_write_file2 failed\n");

  if (! (control = xdp_generate_delta (gen, to_fh, c_fh, d_fh)))
    abort ();

  if (! handle_close (to_fh))
    g_error ("handle_close1 failed\n");

  if (! handle_close (c_fh))
    g_error ("handle_close1 failed\n");

  if (stat (tmp_file_3, & buf) < 0)
    g_error ("stat failed\n");

  d_size = buf.st_size;

  if (stat (tmp_file_4, & buf) < 0)
    g_error ("stat failed\n");

  c_size = buf.st_size;

  if (round == 1)
    report_delta (from, to, test, stats, c_size, d_size);

  xdp_control_free (control);
  xdp_generator_free (gen);

  handle_free (to_fh);
  handle_free (d_fh);
  handle_free (c_fh);

  return TRUE;
}

gboolean
null_test (RcsVersion *from, RcsVersion *to, DeltaTest *test, DeltaStats *stats, int round)
{
  int pid;
  static GPtrArray *a = NULL;

  if (! a)
    a = g_ptr_array_new ();

  if ((pid = vfork ()) < 0)
    g_error ("vfork failed\n");

  if (pid == 0)
    {
      /* child */
      g_ptr_array_set_size (a, 0);

      g_ptr_array_add (a, "exec");

      g_ptr_array_add (a, NULL);

      /* exec */
      execv ("/local/jmacd/xdfs/libdbfs/tools/bin/exec", (char**) a->pdata);
      g_warning ("exec failed: %s", g_strerror (errno));
      _exit (127);
    }
  else
    {
      /* parent */
      int status;

      if (waitpid (pid, &status, 0) < 0)
	g_error ("waitpid failed\n");

      if (! WIFEXITED (status))
	g_error ("diff did not exit\n");

      if (WEXITSTATUS (status) == 127)
	g_error ("diff did not execute\n");

      if (round == 1)
	report_delta (from, to, test, stats, 0, 0);
    }

  return TRUE;
}

gboolean
diff_test (RcsVersion *from, RcsVersion *to, DeltaTest *test, DeltaStats *stats, int round)
{
  int pid;
  static GPtrArray *a = NULL;
  off_t d_size;

  if (! a)
    a = g_ptr_array_new ();

  if ((pid = vfork ()) < 0)
    g_error ("vfork failed\n");

  if (pid == 0)
    {
      /* child */
      int fd;

      if ((fd = open (tmp_file_3, O_WRONLY | O_TRUNC | O_CREAT, 0666)) < 0)
	g_error ("open tmp3 failed\n");

      dup2(fd, STDOUT_FILENO);

      g_ptr_array_set_size (a, 0);

      g_ptr_array_add (a, "diff");

      g_ptr_array_add (a, "-a");
      g_ptr_array_add (a, "--rcs");
      g_ptr_array_add (a, (char*) from->filename);
      g_ptr_array_add (a, (char*) to->filename);
      g_ptr_array_add (a, NULL);

      /* exec */
      execv ("/local/jmacd/xdfs/libdbfs/tools/bin/diff", (char**) a->pdata);
      g_warning ("exec failed: %s", g_strerror (errno));
      _exit (127);
    }
  else
    {
      /* parent */
      int status;
      struct stat buf;

      if (waitpid (pid, &status, 0) < 0)
	g_error ("waitpid failed\n");

      if (! WIFEXITED (status))
	g_error ("diff did not exit\n");

      if (WEXITSTATUS (status) == 127)
	g_error ("diff did not execute\n");

      if (stat (tmp_file_3, & buf) < 0)
	g_error ("stat failed\n");

      d_size = buf.st_size;

      if (round == 1)
	report_delta (from, to, test, stats, 0, d_size);
    }

  if (test->flags & DT_FLAG_DIFF_PENALTY)
    {
      FILE* pen = fopen (tmp_file_4, "w");
      struct stat buf;

      if (d_size > 0)
	fprintf (pen, "hi!\n");

      if (fclose (pen) < 0)
	g_error ("fclose failed");

      if (stat (tmp_file_4, & buf) < 0)
	g_error ("stat failed\n");
    }

  return TRUE;
}

int        all_tests_count;
DeltaTest *all_tests;

DeltaTest available_tests[] = {
  { "DiffPen", & diff_test,   DT_FLAG_NOCONTROL | DT_FLAG_DIFF_PENALTY },
  { "Diff",    & diff_test,   DT_FLAG_NOCONTROL },
  { "Xdelta",  & xdelta_test, 0 },
  { "NULL",    & null_test, 0 }
};

void*
dtest_initialize (void)
{
  DeltaStats* stats = g_new0 (DeltaStats, all_tests_count);
  int i;

  for (i = 0; i < all_tests_count; i += 1)
    {
      stats[i].delta_frac_stat   = stat_dbl_new ("DeltaFrac");
      stats[i].delta_stat        = stat_int_new ("Delta");
      stats[i].control_stat      = stat_int_new ("Control");
      stats[i].patch_stat        = stat_int_new ("Patch");
    }

  return stats;
}

gboolean
dtest_onefile (RcsFile *rcs, RcsStats* set, void* data)
{
  return TRUE;
}

void
dtest_report_time (DeltaTest *test, DeltaStats *stat)
{
  FILE* time;
  double stddev = sqrt (stat->total_time_variance);

  if (! (time = config_output ("%s.time", test->name)))
    abort ();

  fprintf (time, "----------------\n");
  fprintf (time, "Name:        %s\n", test->name);
  fprintf (time, "Mode:        %s\n", dtest_mode);
  fprintf (time, "Time:        %0.2f\n", stat->total_time);
  fprintf (time, "Stddev:      %0.4f\n", stddev);
  fprintf (time, "Time/Stddev: %0.4f\n", 100.0 * stddev / stat->total_time);
  fprintf (time, "Trials:      %d\n", NTRIALS);

  if (fclose (time) < 0)
    g_error ("fclose failed\n");
}

gboolean
dtest_finalize (RcsStats* set, void* data)
{
  DeltaStats* stats = data;
  int i;

  rcswalk_report (set);

  for (i = 0; i < all_tests_count; i += 1)
    {
      DeltaTest  *test = all_tests + i;
      DeltaStats *stat = stats + i;

      stat_int_report (stat->delta_stat);
      stat_dbl_report (stat->delta_frac_stat);

      if (! (test->flags & DT_FLAG_NOCONTROL))
	{
	  stat_int_report (stat->control_stat);
	  stat_int_report (stat->patch_stat);
	}

      dtest_report_time (test, stat);
    }

  g_free (stats);

  return TRUE;
}

static GTimer *timer = NULL;

gboolean
dtest_delta (RcsFile* rcs, RcsVersion* from, RcsVersion *to, void* data)
{
  DeltaStats *stats_array = data;
  int test_i, trial_i;
  static int progress = 0;

  if (! timer)
    timer = g_timer_new ();

  if ((++progress % 1000) == 0)
    fprintf (stdout, "%d\n", progress);

  /* Once through... not timed */
  for (test_i = 0; test_i < all_tests_count; test_i += 1)
    {
      DeltaTest  *test  = all_tests + test_i;

      if (! test->delta (from, to, test, NULL, 0))
	return FALSE;
    }

  for (trial_i = 0; trial_i < NTRIALS; trial_i += 1)
    {
      for (test_i = 0; test_i < all_tests_count; test_i += 1)
	{
	  DeltaTest  *test  = all_tests + test_i;
	  DeltaStats *stat = stats_array + test_i;

	  g_timer_reset (timer);
	  g_timer_start (timer);

	  if (! test->delta (from, to, test, stat, trial_i+1))
	    return FALSE;

	  g_timer_stop (timer);

	  stat->times[trial_i] = g_timer_elapsed (timer, NULL);
	}
    }

  for (test_i = 0; test_i < all_tests_count; test_i += 1)
    {
      /*DeltaTest  *test = all_tests + test_i;*/
      DeltaStats *stat = stats_array + test_i;
      double time_total = 0.0;
      double time_mean;
      double time_variance;
      double sum = 0.0;

      for (trial_i = 0; trial_i < NTRIALS; trial_i += 1)
	time_total += stat->times[trial_i];

      time_mean = time_total / (double) NTRIALS;

      for (trial_i = 0; trial_i < NTRIALS; trial_i += 1)
	{
	  double diff = (time_mean - stat->times[trial_i]);
	  double d2 = diff * diff;

	  sum += d2;
	}

      time_variance = sum / (double) NTRIALS;

      stat->total_time += time_mean;
      stat->total_time_variance += time_variance;
    }

  return TRUE;
}

RcsWalker dtest_walker = {
  & dtest_initialize,
  & dtest_finalize,
  & dtest_onefile,
  NULL,
  NULL,
  NULL,
  0,
  G_MAXINT,
  TRUE
};

int
main (int argc, char** argv)
{
  int i;
  int ret = 0;

  rcswalk_init ();

  config_register (options, ARRAY_SIZE (options));

  config_set_string ("rcswalk_experiment", "dt");

  if (argc < 2)
    {
      config_help ();
      goto bail;
    }

  for (i = 1; i < argc; i += 1)
    {
      if (! config_parse (argv[i]))
	goto bail;
    }

  if (! config_done ())
    goto bail;

  if (! edsio_library_init ())
    return 2;

  if (! xd_edsio_init ())
    return 2;

  if (strcmp (dtest_mode, "Orig") == 0)
    {
      dtest_walker.delta_orig = & dtest_delta;
    }
  else if (strcmp (dtest_mode, "Date") == 0)
    {
      dtest_walker.delta_date = & dtest_delta;
    }
  else
    {
      g_error ("Illegal mode: %s", dtest_mode);
    }

  dtest_walker.min_versions = dtest_min_versions;
  dtest_walker.max_versions = dtest_max_versions;

  for (i = 0; i < ARRAY_SIZE (available_tests); i += 1)
    {
      if (strcmp (dtest_method, available_tests[i].name) == 0)
	{
	  all_tests = available_tests + i;
	  all_tests_count = 1;
	  break;
	}
    }

  if (all_tests_count == 0)
    g_error ("No such test method: %s\n", dtest_method);

  tmp_file_3 = g_strdup_printf ("%s/dt3.%d", g_get_tmp_dir (), (int) getpid ());
  tmp_file_4 = g_strdup_printf ("%s/dt4.%d", g_get_tmp_dir (), (int) getpid ());

  if (! (big = config_output ("BigOnes")))
    goto bail;

  if (! rcswalk (& dtest_walker, NULL))
    goto bail;

  if (fclose (big) < 0)
    {
      g_error ("fclose failed");
      goto bail;
    }

  if (0)
    {
    bail:
      ret = 2;
    }

  unlink (tmp_file_3);
  unlink (tmp_file_4);

  return ret;
}
