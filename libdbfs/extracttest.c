/* -*-Mode: C;-*-
 * $Id$
 * file.c:
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "dbfs.h"
#include <xdelta.h>
#include <edsiostdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <dirent.h>

DBFS        *dbfs;
RepoTxn     *txn;

Path        *xdfs_loc;

char        *delta_file;
char        *orig_file;
char        *cons_file;

XdfsPolicy   extract_policy;

gint32       etest_min_versions    = 0;
gint32       etest_max_versions    = G_MAXINT;
const char*  etest_test            = NULL;
const char*  etest_base_dir        = NULL;
const char*  etest_log_dir         = NULL;

static ConfigOption options [] = {
  { "min_versions", "min",    CS_Use,    CO_Optional, CD_Int32,   & etest_min_versions },
  { "max_versions", "max",    CS_Use,    CO_Optional, CD_Int32,   & etest_max_versions },
  { "method",       "mth",    CS_Use,    CO_Required, CD_String,  & etest_test },
  { "archive_base", "ab",     CS_Ignore, CO_Required, CD_String,  & etest_base_dir },
  { "archive_log",  "al",     CS_Ignore, CO_Optional, CD_String,  & etest_log_dir }
};

gboolean
write_orig_file (Inode* ino)
{
  FileHandle *w, *r;

  if (! (r = dbfs_inode_open_read (txn, ino)))
    return FALSE;

  if (! (w = handle_write_file (orig_file)))
    return FALSE;

  if (! (handle_drain (r, w)))
    return FALSE;

  if (! handle_close (r))
    return FALSE;

  if (! handle_close (w))
    return FALSE;

  handle_free (r);
  handle_free (w);

  return TRUE;
}

gboolean
extract_test_pair (Inode *from_ino, Inode* to_ino, guint8* orig_digest)
{
  /* FROM has been copied to ORIG_FILE, for verification, should
   * produce ORIG_DIGEST.
   */

  FileHandle *delta_out;
  FileHandle *orig_in;
  FileHandle *delta_in;
  FileHandle *cons_out;
  const guint8* cons_digest;
  const MessageDigest *md = edsio_message_digest_md5 ();

  if (! (delta_out = handle_write_file (delta_file)))
    return FALSE;

  if (! xdfs_extract_delta (txn, xdfs_loc, from_ino, to_ino, delta_out))
    return FALSE;

  if (! handle_close (delta_out))
    return FALSE;

  handle_free (delta_out);

  if (! (orig_in = handle_read_file (orig_file)))
    return FALSE;

  if (! (delta_in = handle_read_file (delta_file)))
    return FALSE;

  if (! (cons_out = handle_write_file (cons_file)))
    return FALSE;

  if (! handle_digest_compute (cons_out, md))
    return FALSE;

  if (! xdfs_apply_delta (orig_in, delta_in, cons_out))
    return FALSE;

  g_assert (handle_length (cons_out) == to_ino->minor.ino_segment_len);

  if (! handle_close (orig_in))
    return FALSE;

  if (! handle_close (delta_in))
    return FALSE;

  if (! handle_close (cons_out))
    return FALSE;

  if (! (cons_digest = handle_digest (cons_out, md)))
    return FALSE;

  if (memcmp (cons_digest, orig_digest, md->cksum_len) != 0)
    g_error ("reconstruction failed\n");

  handle_free (orig_in);
  handle_free (delta_in);
  handle_free (cons_out);

  return TRUE;
}

gboolean
extract_test ()
{
  Cursor *c1 = NULL, *c2 = NULL;
  Inode idx;
  Inode ino1;
  Inode ino2;

  if (! xdfs_digest_index (txn, xdfs_loc, & idx))
    goto abort;

  if (! (c1 = dbfs_cursor_open (txn, & idx, CT_Container)))
    goto abort;

  while (dbfs_cursor_next (c1))
    {
      if (! dbfs_cursor_inode (c1, FT_IsAny, & ino1))
	goto abort;

      if (! write_orig_file (& ino1))
	goto abort;

      if (! (c2 = dbfs_cursor_open (txn, & idx, CT_Container)))
	goto abort;

      while (dbfs_cursor_next (c2))
	{
	  BaseName* b2 = dbfs_cursor_basename (c2);

	  if (! dbfs_cursor_inode (c2, FT_IsAny, & ino2))
	    goto abort;

	  if (! extract_test_pair (& ino1, & ino2, b2->data))
	    goto abort;
	}

      if (! dbfs_cursor_close (c2))
	goto abort;

      c2 = NULL;
    }

  if (! dbfs_cursor_close (c1))
    goto abort;

  c1 = NULL;

  return TRUE;

 abort:

  if (c1) dbfs_cursor_close (c1);
  if (c2) dbfs_cursor_close (c2);

  return FALSE;
}

gboolean
extract_create_xdfs (RepoTxn *txn, Path *loc)
{
  Inode ino;
  Inode root;
  XdfsParams params;

  if (! dbfs_inode_find_root (txn, path_root (txn->alloc), FALSE, FT_Directory, & root))
    return FALSE;

  if (! dbfs_inode_new (txn, & ino))
    return FALSE;

  if (! dbfs_make_directory (txn, & ino, TRUE))
    return FALSE;

  if (! dbfs_link_create (txn, & root, & ino, path_basename (loc), FALSE))
    return FALSE;

  memset (& params, 0, sizeof (params));

  params.policy               = extract_policy;
  params.flags                = XF_MD5Equality;
  params.cluster_max_versions = 3;

  if (! xdfs_location_create (txn, loc, & params))
    return FALSE;

  return TRUE;
}

gboolean
extract_dateorder (RcsFile* rcs, RcsVersion *v, void* data)
{
  FileHandle *fh;

  if (! txn)
    {
      if (! (txn = dbfs_txn_begin (dbfs, DBFS_TXN_FLAT)))
 	return FALSE;
    }

  if (! xdfs_loc)
    {
      xdfs_loc = path_append_bytes (txn->alloc, path_root (txn->alloc),
				    PP_String, rcs->filename, strlen (rcs->filename));

      if (! extract_create_xdfs (txn, xdfs_loc))
	return FALSE;
    }

  if (! (fh = handle_read_file (v->filename)))
    return FALSE;

  if (! xdfs_insert_version (txn, xdfs_loc, fh, NULL))
    return FALSE;

  handle_free (fh);

  return TRUE;
}

gboolean
extract_onefile (RcsFile *rcs, RcsStats* set, void* data)
{
  if (! extract_test ())
    return FALSE;

  if (! dbfs_txn_commit (txn))
    return FALSE;

  txn = NULL;
  xdfs_loc = NULL;

  return TRUE;
}

DBFS*
extract_init ()
{
  Path *archive_path;
  Path *log_path = NULL;

  if (! (archive_path = path_from_host_string (NULL, _fs_pthn, etest_base_dir)))
    return NULL;

  if (etest_log_dir && ! (log_path = path_from_host_string (NULL, _fs_pthn, etest_log_dir)))
    return NULL;

  return dbfs_create (archive_path, log_path);
}

RcsWalker extract_walker = {
  NULL,
  NULL,
  & extract_onefile,
  & extract_dateorder,
  NULL,
  NULL,
  0,
  G_MAXINT,
  TRUE
};

int
main (int argc, char** argv)
{
  int i;

  rcswalk_init ();

  orig_file = g_strdup_printf ("%s/st1.%d", g_get_tmp_dir (), (int) getpid ());
  cons_file = g_strdup_printf ("%s/st2.%d", g_get_tmp_dir (), (int) getpid ());
  delta_file = g_strdup_printf ("%s/st3.%d", g_get_tmp_dir (), (int) getpid ());

  config_register (options, ARRAY_SIZE (options));

  config_set_string ("rcswalk_experiment", "et");

  if (argc < 2)
    {
      config_help ();
      goto bail;
    }

  for (i = 1; i < argc; i += 1)
    {
      if (! config_parse (argv[i]))
	goto bail;
    }

  if (! config_done ())
    goto bail;

  if (strcmp (etest_test, "XDFS-rj") == 0)
    extract_policy = XP_ReverseJump;
  else if (strcmp (etest_test, "XDFS-fj") == 0)
    extract_policy = XP_ForwardJump;
  else if (strcmp (etest_test, "XDFS-f") == 0)
    extract_policy = XP_Forward;
  else
    g_error ("unknown TEST: %s\n", argv[1]);

  extract_walker.min_versions = etest_min_versions;
  extract_walker.max_versions = etest_max_versions;

  if (! config_clear_dir (etest_base_dir))
    goto bail;

  if (! config_clear_dir (etest_log_dir))
    goto bail;

  if (! dbfs_library_init ())
    goto bail;

  if (! xdfs_library_init ())
    goto bail;

  if (! (dbfs = extract_init ()))
    goto bail;

  if (! rcswalk (& extract_walker, NULL))
    goto bail;

  unlink (orig_file);
  unlink (cons_file);
  unlink (delta_file);

  return 0;

 bail:

  if (txn) dbfs_txn_abort (txn);

  if (dbfs) dbfs_close (dbfs);

  dbfs_library_close ();

  unlink (orig_file);
  unlink (cons_file);
  unlink (delta_file);

  return 2;
}
