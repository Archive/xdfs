/* -*-Mode: C;-*-
 * $Id: file.c 1.11 Tue, 02 May 2000 07:02:54 -0700 jmacd $
 * file.c:
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "dbfs.h"

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <edsiostdio.h>
#include <sys/mman.h>

static int dbfs_fs_short_threshold = 2048;

/*#define DBFS_FILES_IN_MEMPOOL*/

static gboolean     dbfs_list_recursive (RepoTxn    *txn,
					 FileHandle *writeto,
					 ListFlags   lf,
					 Path       *path,
					 guint64     inum,
					 guint16     stck);

static gboolean     dbfs_write_handle_close (FileHandle* fh);
static void         dbfs_write_handle_abort (FileHandle* fh);
static const gchar* dbfs_write_handle_name  (FileHandle *fh);
static void         dbfs_write_handle_free  (FileHandle* fh);
static gssize       dbfs_write_handle_map   (FileHandle *fh, guint pgno, const guint8** mem);
static gboolean     dbfs_write_handle_unmap (FileHandle *fh, guint pgno, const guint8** mem);

static gboolean     dbfs_read_handle_close (FileHandle* fh);
static void         dbfs_read_handle_abort (FileHandle* fh);
static const gchar* dbfs_read_handle_name  (FileHandle *fh);
static void         dbfs_read_handle_free  (FileHandle* fh);
static gssize       dbfs_read_handle_map   (FileHandle *fh, guint pgno, const guint8** mem);
static gboolean     dbfs_read_handle_unmap (FileHandle *fh, guint pgno, const guint8** mem);

const HandleFuncTable dbfs_write_func_table =
{
  dbfs_write_handle_close,
  dbfs_write_handle_abort,
  dbfs_write_handle_name,
  dbfs_write_handle_free,
  dbfs_write_handle_map,
  dbfs_write_handle_unmap,
  NULL,
  NULL,
  NULL
};

const HandleFuncTable dbfs_read_func_table =
{
  dbfs_read_handle_close,
  dbfs_read_handle_abort,
  dbfs_read_handle_name,
  dbfs_read_handle_free,
  dbfs_read_handle_map,
  dbfs_read_handle_unmap,
  NULL,
  NULL,
  NULL
};

struct _WriteHandle {
  FileHandle  fh;
  RepoTxn    *parent_txn;

  guint64     fsloc;
  Path       *fsloc_path;
  Inode      *inop;

  guint8     *pagebuf;

  /* Only one of these gets used.  fd=-1 is the initial value.
   */

  int         fd;
};

struct _ReadHandle {
  FileHandle    fh;
  RepoTxn      *parent_txn;
  Inode        *inop;

  DB_MPOOLFILE *mpf;
#ifndef DBFS_FILES_IN_MEMPOOL
  int           fd;
#endif
  gboolean      opened;

  guint8*       short_buf;

  ViewDef      *view_def;
  void         *view_private;
};

void
dbfs_set_fs_short_threshold (int val)
{
  dbfs_fs_short_threshold = val;
}

static gboolean
dbfs_inode_find_internal (RepoTxn  *txn,
			  Inode    *root,
			  Path     *path,
			  guint16   stck,
			  BaseName *name,
			  Follow    follow_links,
			  guint     follow_depth,
			  FileType  type_mask,
			  Inode    *inop,
			  Inode    *d_inop)
{
  gboolean already_got_default = FALSE;

  /* Step 0: Initialize the return inode, ikey.  Test recursion depth.
   */
  memset (inop, 0, sizeof (* inop));

  inop->minor.ino_type = FT_Invalid;
  inop->key.name = name;
  inop->key.stck = stck;

  if (follow_depth == MAX_LINK_FOLLOW)
    {
      dbfs_generate_path_event (EC_DbfsLinkDepthExceeded, path);
      return FALSE;
    }

  g_assert (root->key.stck == DBFS_DEFAULT_STACK_ID);
  g_assert (root->key.name->len == 0);

  /* Step 1: Special case for root directory, which has no parent.
   */
  if (path_term (path))
    {
      /* If the path has no basename, it is the root directory.
       */
      *inop = *root;

      /* The directory inode is invalid for this special case, but
       * that's acceptable because it should always exist and cannot
       * be created.
       */
      memset (d_inop, 0, sizeof (* d_inop));

      d_inop->minor.ino_type = FT_Invalid;

      already_got_default = TRUE;

      goto getnode;
    }
  else
    {
      /* Else make a recursive call to locate the parent directory.
       */
      Inode  d2_ino;
      Path  *d_path = path_dirname (txn->alloc, path);

      /* This is where d_inodep is set.  Recursive call follows
       * links, is not RMW, and expects a linked container.
       */
      if (! dbfs_inode_find_internal (txn,
				      root,
				      d_path,
				      DBFS_DEFAULT_STACK_ID,
				      _dbfs_default_name,
				      DbfsFollowLinks,
				      0, /* recursive follow depth */
				      FT_IsLinked,
				      d_inop,
				      & d2_ino))
	return FALSE;

      /* With the parent directory located, test for the link.  There
       * are several conditions:
       *
       *  1. the link is found,     ikeyp->inum is set correctly
       *  2. the link is null,      ikeyp->inum is set to INODE_NULL
       *  3. the link is not found, ikeyp->inum is set to INODE_NOTFOUND
       */

      if (! dbfs_getlink (txn, d_inop, path_basename (path), & inop->key.inum))
	return FALSE;
    }

  /* Step 2: Now ikeyp->inum has been set, treat three cases.
   */
  switch (inop->key.inum)
    {
      /* The first two cases occur when there is no major inum.
       */
    case INODE_NOTFOUND:
      inop->minor.ino_type = FT_NotFound;
      goto checktype;

    case INODE_NULL:
      inop->minor.ino_type = FT_Null;
      goto checktype;

    default:

      /* Since the inode is known, continue.
       */
      break;
    }

  /* Step 3: If following links, must check now.
   */
  if (follow_links)
    {
      guint indirect_follow_depth = 0;

      /* Search for the DEFAULT minor inode (not the NAME that was
       * supplied).  There are two conditions:
       *
       *  1. the node is present, inodep is filled in
       *  2. the node is not present, inodep->ino_type is set to FT_NotPresent
       */
      inop->key.name = _dbfs_default_name;
      inop->key.stck = DBFS_DEFAULT_STACK_ID;

    again:

      if (! dbfs_getnode (txn, inop, FALSE))
	return FALSE;

      already_got_default = TRUE;

      switch (inop->minor.ino_type)
	{
#if 0
	case FT_Symlink:
	  {
	    Path* link;

	    if (! (link = dbfs_read_short_path_segment (txn, inop)))
	      return FALSE;

	    /* Recurse on symlink.
	     */
	    return dbfs_inode_find_internal (txn,
					     (path_type (link) == PT_PathAbsolute) ? txn->dbfs->root_inop : d_inop,
					     link,
					     stck,
					     name,
					     follow_links,
					     follow_depth + 1,
					     type_mask,
					     inop,
					     d_inop);
	  }
	  break;
#endif
	case FT_Indirect:
	  {
	    if (indirect_follow_depth++ == MAX_LINK_FOLLOW)
	      {
		dbfs_generate_void_event (EC_DbfsIndirectLinkDepthExceeded);
		return FALSE;
	      }

	    inop->key.inum = inop->minor.ino_content_key;

	    /* Repeat on indirect link.
	     */
	    goto again;
	  }
	  break;
	default:
	  break;
	}
    }

 getnode:

  /* Step 4: Search for the actual segment name.  If the supplied name
   *         has length 0, then it is the default.  If default was already
   *         tested by the symlink code above, skip.
   */
  inop->key.name = name;
  inop->key.stck = stck;

  if (! (name->len == 0 && stck == DBFS_DEFAULT_STACK_ID && already_got_default))
    {
      /* Same two conditions as above: present or not.
       */
      if (! dbfs_getnode (txn, inop, FALSE))
	return FALSE;
    }

 checktype:

  /* Step 5: Test that the actual type overlaps the supplied mask
   */

  if (! dbfs_checktype (txn, inop, type_mask))
    return FALSE;

  return TRUE;
}

gboolean
dbfs_checktype (RepoTxn  *txn,
		Inode    *inop,
		FileType  type_mask)
{
  if ((inop->minor.ino_type & type_mask) != type_mask)
    {
      dbfs_generate_int_event (EC_DbfsInvalidFileType, inop->key.inum);
      return FALSE;
    }

  return TRUE;
}

gboolean
dbfs_inode_new (RepoTxn  *parent_txn,
		Inode    *inop)
{
  RepoTxn *txn = NULL;
  guint64 new_inum;

  if (! (txn = dbfs_txn_nest (parent_txn)))
    goto abort;

  /* Insert next seq. into major inodes table, to obtain a new inum.
   * This calls dbfs_zero_refs on the resulting inode, so no reference
   * count management happens here.
   */

  if (! dbfs_inum_allocate (txn, & new_inum))
    goto abort;

  /* Initialize result
   */

  inop->key.inum = new_inum;
  inop->key.stck = DBFS_DEFAULT_STACK_ID;
  inop->key.name = _dbfs_default_name;

  memset (& inop->minor, 0, sizeof (MinorInode));

  inop->minor.ino_type = FT_NotPresent;
  inop->minor.ino_flags = IF_New;

  if (! dbfs_txn_commit (txn))
    goto abort;

  return TRUE;

 abort:

  if (txn)
    dbfs_txn_abort (txn);

  return FALSE;
}

#if 0
static int open_read_count;
static int open_replace_count;

static int close_read_count;
static int close_replace_count;

static int free_read_count;
static int free_replace_count;
#endif

FileHandle*
dbfs_inode_open_replace (RepoTxn  *parent_txn,
			 Inode    *inop)
{
  RepoTxn *txn = NULL;
  WriteHandle *wh = g_new0 (WriteHandle, 1);

  if (! (txn = dbfs_txn_nest (parent_txn)))
    goto abort;

  if (! dbfs_getnode (txn, inop, TRUE))
    goto abort;

  if (! dbfs_checktype (txn, inop, FT_CanReplace))
    goto abort;

  wh->parent_txn = parent_txn;
  wh->fd = -1;
  wh->inop = inop;
  wh->pagebuf = g_malloc (parent_txn->dbfs->fs_page_size);

  wh->fh.table = & dbfs_write_func_table;
  wh->fh.fh_open_flags = wh->fh.fh_open_mode = HV_Replace;
  wh->fh.fh_has_len = TRUE;

  file_position_from_abs (wh->parent_txn->dbfs->fs_page_size, 0, & wh->fh.fh_cur_pos);
  file_position_from_abs (wh->parent_txn->dbfs->fs_page_size, 0, & wh->fh.fh_file_len);

  if (! dbfs_txn_commit (txn))
    goto abort;

  /*g_print ("replace open: %d; close: %d; free %d\n", ++open_replace_count, close_replace_count, free_replace_count);*/

  return & wh->fh;

 abort:

  if (txn)
    dbfs_txn_abort (txn);

  g_free (wh);

  return NULL;
}

FileHandle*
dbfs_inode_open_read (RepoTxn  *parent_txn,
		      Inode    *inop)
{
  RepoTxn *txn = NULL;
  ReadHandle *rh = g_new0 (ReadHandle, 1);

  if (! (txn = dbfs_txn_nest (parent_txn)))
    goto abort;

  if (! dbfs_getnode (txn, inop, FALSE))
    goto abort;

  if (! dbfs_checktype (txn, inop, FT_CanRead))
    goto abort;

  rh->parent_txn = parent_txn;
  rh->inop = inop;

  rh->fh.table = & dbfs_read_func_table;
  rh->fh.fh_open_flags = rh->fh.fh_open_mode = HV_Read;
  rh->fh.fh_has_len = TRUE;

  file_position_from_abs (rh->parent_txn->dbfs->fs_page_size, 0, & rh->fh.fh_cur_pos);
  file_position_from_abs (rh->parent_txn->dbfs->fs_page_size, inop->minor.ino_segment_len, & rh->fh.fh_file_len);

  if (! dbfs_txn_commit (txn))
    goto abort;

  /*g_print ("read open: %d; close: %d; free %d\n", ++open_read_count, close_read_count, free_read_count);*/

  return & rh->fh;

 abort:

  if (txn)
    dbfs_txn_abort (txn);

  g_free (rh);

  return NULL;
}

/* @@@ Issue: The deletion routines won't work if there are cycles...
 * For now I will assume there aren't, later I may want a more
 * complicated garbage collector.
 */

gboolean
dbfs_container_delete (RepoTxn *txn,
		       Inode   *inop)
{
  Cursor *c = NULL;

  if (! (c = dbfs_cursor_open (txn, inop, CT_Container)))
    goto abort;

  while (dbfs_cursor_next (c))
    {
      guint64 inum;

      if (! dbfs_cursor_inum (c, & inum))
	goto abort;

      if (! dbfs_inode_decr (txn, inum))
	goto abort;
    }

  if (! dbfs_cursor_close (c))
    goto abort;

  idset_add (txn->unref_db_commits, inop->minor.ino_content_key, 0);

  return TRUE;

 abort:

  if (c)
    dbfs_cursor_close (c);

  return FALSE;
}

gboolean
dbfs_view_delete (RepoTxn *txn,
		  Inode   *inop)
{
  /* @@@ Do nothing.  dbfs_txn_pre_commit is calling delete on the
   * stacked inodes anyway. */
  return TRUE;
}

gboolean
dbfs_inode_delete_internal (RepoTxn  *txn,
			    Inode    *inop,
			    gboolean  delnode)
{
  switch (inop->minor.ino_type)
    {
    case FT_Directory:
    case FT_Sequence:
    case FT_Index:

      if (! dbfs_container_delete (txn, inop))
	return FALSE;

      break;

    case FT_View:

      if (! dbfs_view_delete (txn, inop))
	return FALSE;

      break;

    case FT_Indirect:

      if (! dbfs_inode_decr (txn, inop->minor.ino_content_key))
	return FALSE;

      break;

    case FT_Segment:

      if (inop->minor.ino_flags & IF_Short)
	{
	  if (! dbfs_delshort (txn, inop))
	    return FALSE;
	}
      else
	{
	  /* This removes it from the list of FDs that will fsync()
	   * in pre-commit.
	   */
	  int fd;

	  if (idset_del (txn->unref_fs_aborts,  inop->minor.ino_content_key, & fd))
	    {
	      close (fd);
	    }

	  idset_add (txn->unref_fs_commits, inop->minor.ino_content_key, 0);
	}

      break;

    default:
      abort ();
    }

  if (delnode && ! dbfs_delnode (txn, inop))
    return FALSE;

  inop->minor.ino_type = FT_NotPresent;

  return TRUE;
}

gboolean
dbfs_inode_delete (RepoTxn  *parent_txn,
		   Inode    *inop)
{
  RepoTxn *txn = NULL;

  if (! (txn = dbfs_txn_nest (parent_txn)))
    goto abort;

  if (! dbfs_getnode (txn, inop, TRUE))
    goto abort;

  if (inop->key.inum <= INODE_ROOT)
    {
      dbfs_generate_void_event (EC_DbfsCannotDeleteRoot);
      goto abort;
    }

  if (! dbfs_checktype (txn, inop, FT_IsPresent))
    goto abort;

  if (! dbfs_inode_delete_internal (txn, inop, TRUE))
    goto abort;

  if (! dbfs_txn_commit (txn))
    goto abort;

  return TRUE;

 abort:

  if (txn)
    dbfs_txn_abort (txn);

  return FALSE;
}

gboolean
dbfs_inode_find_stacked_named (RepoTxn  *txn,
			       Inode    *root,
			       Path     *path,
			       guint16   stck,
			       BaseName *name,
			       Follow    follow_links,
			       FileType  type_mask,
			       Inode    *inop)
{
  Inode d_ino;

  return dbfs_inode_find_internal (txn,
				   root,
				   path,
				   stck,
				   name,
				   follow_links,
				   0, /* link depth */
				   type_mask,
				   inop,
				   & d_ino);
}

gboolean
dbfs_inode_find (RepoTxn  *txn,
		 Inode    *root,
		 Path     *path,
		 Follow    follow_links,
		 FileType  type_mask,
		 Inode    *inop)
{
  return dbfs_inode_find_stacked_named (txn,
					root,
					path,
					DBFS_DEFAULT_STACK_ID,
					_dbfs_default_name,
					follow_links,
					type_mask,
					inop);
}

gboolean
dbfs_inode_find_root (RepoTxn  *txn,
		      Path     *path,
		      Follow    follow_links,
		      FileType  type_mask,
		      Inode    *inop)
{
  return dbfs_inode_find (txn,
			  & txn->dbfs->root_ino,
			  path,
			  follow_links,
			  type_mask,
			  inop);
}

gboolean
dbfs_inode_find_seqno (RepoTxn  *txn,
		       Inode    *seq,
		       guint64   seqno,
		       Follow    follow_links,
		       FileType  type_mask,
		       Inode    *inop)
{
  db_recno_t seqno_rn = seqno;

  return dbfs_inode_find (txn,
			  seq,
			  path_append_bytes (txn->alloc, path_current (txn->alloc), PP_Hex, (guint8*) & seqno_rn, sizeof (db_recno_t)),
			  follow_links,
			  type_mask,
			  inop);
}

gboolean
dbfs_inode_delete_stacked_named (RepoTxn  *txn,
				 Inode    *inop,
				 guint16   stack_id,
				 BaseName *name)
{
  Inode copy;

  memcpy (& copy, inop, sizeof (copy));

  copy.key.stck = stack_id;
  copy.key.name = name;

  return dbfs_inode_delete (txn, & copy);
}

gboolean
dbfs_inode_delete_named (RepoTxn  *txn,
			 Inode    *inop,
			 BaseName *name)
{
  Inode copy;

  memcpy (& copy, inop, sizeof (copy));

  copy.key.name = name;

  return dbfs_inode_delete (txn, & copy);
}

FileHandle*
dbfs_inode_open_replace_stacked_named (RepoTxn  *txn,
				       Inode    *inop,
				       guint16   stack_id,
				       BaseName *name,
				       Inode    *out)
{
  memcpy (out, inop, sizeof (Inode));

  out->key.stck = stack_id;
  out->key.name = name;

  return dbfs_inode_open_replace (txn, out);
}

FileHandle*
dbfs_inode_open_replace_named (RepoTxn  *txn,
			       Inode    *inop,
			       BaseName *name,
			       Inode    *out)
{
  return dbfs_inode_open_replace_stacked_named (txn, inop, inop->key.stck, name, out);
}

FileHandle*
dbfs_inode_open_replace_stacked (RepoTxn  *txn,
				 Inode    *inop,
				 guint16   stack_id,
				 Inode    *out)
{
  return dbfs_inode_open_replace_stacked_named (txn, inop, stack_id, inop->key.name, out);
}

FileHandle*
dbfs_inode_open_read_stacked_named    (RepoTxn  *txn,
				       Inode    *inop,
				       guint16   stack_id,
				       BaseName *name,
				       Inode    *out)
{
  memcpy (out, inop, sizeof (Inode));

  out->key.stck = stack_id;
  out->key.name = name;

  return dbfs_inode_open_read (txn, out);
}

FileHandle*
dbfs_inode_open_read_named (RepoTxn  *txn,
			    Inode    *inop,
			    BaseName *name,
			    Inode    *out)
{
  return dbfs_inode_open_read_stacked_named (txn, inop, inop->key.stck, name, out);
}

FileHandle*
dbfs_inode_open_read_stacked (RepoTxn  *txn,
			      Inode    *inop,
			      guint16   stack_id,
			      Inode    *out)
{
  return dbfs_inode_open_read_stacked_named (txn, inop, stack_id, inop->key.name, out);
}

/* @@@ Issue: the getnode() and checktype() in a common access method
 * should try its read locks in the parent txn so that failure properly
 * indicates that the record was read, even if modification fails.
 */

gboolean
dbfs_link_create (RepoTxn  *parent_txn,
		  Inode    *cont,
		  Inode    *inop,
		  BaseName *name,
		  gboolean  overwrite)
{
  RepoTxn *txn = NULL;

  if (! (txn = dbfs_txn_nest (parent_txn)))
    goto abort;

  if (! dbfs_getnode (txn, cont, TRUE))
    goto abort;

  if (! dbfs_checktype (txn, cont, FT_IsContainer | FT_IsLinked | FT_IsNonNumeric))
    goto abort;

  if (! dbfs_putlink (txn, cont, name, inop->key.inum, overwrite))
    goto abort;

  if (! dbfs_inode_incr (txn, inop->key.inum))
    goto abort;

  if (! dbfs_txn_commit (txn))
    goto abort;

  return TRUE;

 abort:

  if (txn)
    dbfs_txn_abort (txn);

  return FALSE;
}

gboolean
dbfs_link_create_next (RepoTxn  *parent_txn,
		       Inode    *cont,
		       Inode    *inop)
{
  RepoTxn *txn = NULL;

  if (! (txn = dbfs_txn_nest (parent_txn)))
    goto abort;

  if (! dbfs_getnode (txn, cont, TRUE))
    goto abort;

  if (! dbfs_checktype (txn, cont, FT_IsContainer | FT_IsLinked | FT_IsNumeric))
    goto abort;

  if (! dbfs_putlink_next (txn, cont, inop->key.inum))
    goto abort;

  if (! dbfs_inode_incr (txn, inop->key.inum))
    goto abort;

  if (! dbfs_txn_commit (txn))
    goto abort;

  return TRUE;

 abort:

  if (txn)
    dbfs_txn_abort (txn);

  return FALSE;
}

/* Cursors
 */

Cursor*
dbfs_cursor_open (RepoTxn    *parent_txn,
		  Inode      *inop,
		  CursorType  type)
{
  Cursor* c = g_new0 (Cursor, 1);
  int err;

  c->parent_txn = parent_txn;
  c->type = type;

  c->key_array = g_byte_array_new ();
  c->data_array = g_byte_array_new ();

  g_byte_array_set_size (c->key_array, 32);
  g_byte_array_set_size (c->data_array, 32);

  if (type == CT_Container)
    {
      if (! dbfs_getnode (parent_txn, inop, FALSE))
	goto abort;

      if (! (c->unuse_dbp = c->dbp = dbfs_use_db (parent_txn->alloc, parent_txn->dbfs, inop)))
	goto abort;
    }
  else
    {
      c->dbp = parent_txn->dbfs->minor_inodes_dbp;

      c->prefix_key = g_byte_array_new ();
      c->prefix_first = TRUE;

      dbfs_minor_key (NULL, c->prefix_key, inop);

      g_byte_array_set_size (c->prefix_key, 8 /* @@@ was 10 */);
    }

  c->inop = *inop;

  dbfs_use_txn (parent_txn);

  if ((err = c->dbp->cursor (c->dbp, parent_txn->db_txn, & c->dbc, 0)))
    {
      dbfs_generate_int_event (EC_DbfsDbCursor, err);
      goto abort;
    }

  return c;

 abort:

  if (c->unuse_dbp)
    dbfs_unuse_db (parent_txn->dbfs, inop, c->unuse_dbp);

  g_free (c);

  return NULL;
}

gboolean
dbfs_cursor_close (Cursor   *cursor)
{
  int err;
  DBC *dbc = cursor->dbc;
  gboolean fail = cursor->failure;

  if (cursor->unuse_dbp)
    dbfs_unuse_db (cursor->parent_txn->dbfs, & cursor->inop, cursor->unuse_dbp);

  g_free (cursor);

  if ((err = dbc->c_close (dbc)))
    {
      dbfs_generate_int_event (EC_DbfsDbCursorClose, err);
      return FALSE;
    }

  return ! fail;
}

gboolean
dbfs_cursor_inode (Cursor   *cursor,
		   FileType  type_mask,
		   Inode    *inop)
{
  if (! dbfs_cursor_inum (cursor, & inop->key.inum))
    return FALSE;

  if (cursor->type == CT_Container)
    {
      inop->key.stck = DBFS_DEFAULT_STACK_ID;
      inop->key.name = _dbfs_default_name;
      inop->minor.ino_flags = 0;

      if (! dbfs_getnode (cursor->parent_txn, inop, FALSE))
	return FALSE;
    }
  else
    {
      /* @@@ was:
	 inop->key.stck = cursor->inop.key.stck;*/
      memcpy (& inop->key.stck, cursor->key_array->data + 8, 2);
      inop->key.name = & cursor->bn_tmp;

      cursor->bn_tmp.len = cursor->key_len - 10;
      cursor->bn_tmp.data = cursor->key_array->data + 10;

      g_assert (cursor->data_len == sizeof (MinorInode));

      memcpy (& inop->minor, cursor->data_array->data, sizeof (MinorInode));
    }

  if (! dbfs_checktype (cursor->parent_txn, inop, type_mask))
    return FALSE;

  return TRUE;
}

gboolean
dbfs_cursor_inum (Cursor   *cursor,
		  guint64  *inum)
{
  if (cursor->type == CT_Container)
    memcpy (inum, cursor->data_array->data, 8);
  else
    (* inum) = cursor->inop.key.inum;

  return TRUE;
}

BaseName*
dbfs_cursor_basename (Cursor   *cursor)
{
  BaseName *bn = alc_new (cursor->parent_txn->alloc, BaseName);

  if (cursor->type == CT_Container)
    {
      bn->len = cursor->key_len;
      bn->data = allocator_memdup (cursor->parent_txn->alloc, cursor->key_array->data, bn->len);
    }
  else
    {
      bn->len = cursor->key_len - 10;
      bn->data = allocator_memdup (cursor->parent_txn->alloc, cursor->key_array->data + 10, bn->len);
    }

  return bn;
}

gboolean
dbfs_cursor_delete (Cursor   *cursor)
{
  int err;

  /* @@@ Problem here: no nested transaction for the decrement because
   * cursor uses the parent.
   */

  if (cursor->type == CT_Container)
    {
      guint64 inum;

      if (! dbfs_cursor_inum (cursor, & inum))
	return FALSE;

      if (! dbfs_inode_decr (cursor->parent_txn, inum))
	return FALSE;
    }
  else
    {
      Inode ino;

      if (! dbfs_cursor_inode (cursor, FT_IsPresent, & ino))
	return FALSE;

      if (! dbfs_inode_delete_internal (cursor->parent_txn, & ino, FALSE))
	return FALSE;
    }

  if ((err = cursor->dbc->c_del (cursor->dbc, 0)))
    {
      dbfs_generate_int_event (EC_DbfsDbCursorDel, err);
      return FALSE;
    }

  return TRUE;
}

gboolean
dbfs_cursor_next (Cursor   *cursor)
{
  DBT key, data;
  int err;
  int flag = DB_NEXT;

  dbfs_clear_dbts (& key, & data);

 again:

  key.data = cursor->key_array->data;
  key.ulen = cursor->key_array->len;
  key.flags = DB_DBT_USERMEM;

  data.data = cursor->data_array->data;
  data.ulen = cursor->data_array->len;
  data.flags = DB_DBT_USERMEM;

  if (cursor->prefix_first)
    {
      flag = DB_SET_RANGE;

      key.size = cursor->prefix_key->len;
      memcpy (key.data, cursor->prefix_key->data, key.size);

      cursor->prefix_first = FALSE;
    }

  if ((err = cursor->dbc->c_get (cursor->dbc, & key, & data, flag)))
    {
      if (err == ENOMEM)
	{
	  if (key.size > key.ulen)
	    g_byte_array_set_size (cursor->key_array, key.size);

	  if (data.size > data.ulen)
	    g_byte_array_set_size (cursor->data_array, data.size);

	  goto again;
	}
      else if (err == DB_NOTFOUND)
	{
	  return FALSE;
	}
      else
	{
	  cursor->failure = TRUE;
	  dbfs_generate_int_event (EC_DbfsDbCursorGet, err);
	  return FALSE;
	}
    }

  if (cursor->prefix_key)
    {
      if (key.size < cursor->prefix_key->len ||
	  memcmp (cursor->prefix_key->data, key.data, cursor->prefix_key->len) != 0)
	return FALSE;
    }

  cursor->key_len = key.size;
  cursor->data_len = data.size;

  return TRUE;
}

/* View stuff
 */

gboolean
dbfs_make_view (RepoTxn  *parent_txn,
		Inode    *inop,
		guint64   length,
		guint16   stack_id,
		ViewDef  *vd)
{
  RepoTxn *txn = NULL;

  if (! (txn = dbfs_txn_nest (parent_txn)))
    goto abort;

  if (! dbfs_getnode (txn, inop, TRUE))
    goto abort;

  if (! dbfs_checktype (txn, inop, FT_NotPresent))
    goto abort;

  inop->minor.ino_type = FT_View;
  inop->minor.ino_stack_id = stack_id;
  inop->minor.ino_flags = 0;
  inop->minor.ino_content_key = vd->view_id;
  inop->minor.ino_segment_len = length;

  if (! dbfs_putnode (txn, inop, TRUE))
    goto abort;

  if (! dbfs_txn_commit (txn))
    goto abort;

  return TRUE;

 abort:

  if (txn)
    dbfs_txn_abort (txn);

  return FALSE;
}

gboolean
dbfs_make_indirect_link (RepoTxn  *parent_txn,
			 Inode    *from,
			 Inode    *to)
{
  RepoTxn *txn = NULL;

  if (! (txn = dbfs_txn_nest (parent_txn)))
    goto abort;

  if (! dbfs_getnode (txn, from, TRUE))
    goto abort;

  if (! dbfs_checktype (txn, from, FT_NotPresent))
    goto abort;

  from->minor.ino_type = FT_Indirect;
  from->minor.ino_stack_id = 0;
  from->minor.ino_flags = 0;
  from->minor.ino_content_key = to->key.inum;
  from->minor.ino_segment_len = 0;

  if (! dbfs_inode_incr (txn, to->key.inum))
    goto abort;

  if (! dbfs_putnode (txn, from, TRUE))
    goto abort;

  if (! dbfs_txn_commit (txn))
    goto abort;

  return TRUE;

 abort:

  if (txn)
    dbfs_txn_abort (txn);

  return FALSE;
}

gboolean
dbfs_make_indirect_link_named (RepoTxn  *txn,
			       Inode    *from,
			       BaseName *name,
			       Inode    *to)
{
  Inode copy;

  memcpy (& copy, from, sizeof (copy));

  copy.key.name = name;

  return dbfs_make_indirect_link (txn, & copy, to);
}

gboolean
dbfs_make_indirect_link_stacked_named (RepoTxn  *txn,
				       Inode    *from,
				       guint16   stack_id,
				       BaseName *name,
				       Inode    *to)
{
  Inode copy;

  memcpy (& copy, from, sizeof (copy));

  copy.key.stck = stack_id;
  copy.key.name = name;

  return dbfs_make_indirect_link (txn, & copy, to);
}

gboolean
dbfs_read_indirect_link (RepoTxn  *parent_txn,
			 Inode    *from,
			 FileType  type_mask,
			 Inode    *to)
{
  RepoTxn *txn = NULL;

  if (! (txn = dbfs_txn_nest (parent_txn)))
    goto abort;

  if (! dbfs_getnode (txn, from, TRUE))
    goto abort;

  memset (to, 0, sizeof (Inode));

  if (from->minor.ino_type == FT_NotPresent)
    {
      to->minor.ino_type = FT_NotFound;
    }
  else
    {
      if (! dbfs_checktype (txn, from, FT_Indirect))
	goto abort;

      to->key.inum = from->minor.ino_content_key;
      to->key.stck = DBFS_DEFAULT_STACK_ID;
      to->key.name = _dbfs_default_name;

      if (! dbfs_getnode (txn, to, FALSE))
	goto abort;
    }

  if (! dbfs_checktype (txn, to, type_mask))
    goto abort;

  if (! dbfs_txn_commit (txn))
    goto abort;

  return TRUE;

 abort:

  if (txn)
    dbfs_txn_abort (txn);

  return FALSE;
}

gboolean
dbfs_read_indirect_link_named (RepoTxn  *txn,
			       Inode    *from,
			       BaseName *name,
			       FileType  type_mask,
			       Inode    *to)
{
  Inode copy;

  memcpy (& copy, from, sizeof (copy));

  copy.key.name = name;

  return dbfs_read_indirect_link (txn, & copy, type_mask, to);
}

gboolean
dbfs_read_indirect_link_stacked_named (RepoTxn  *txn,
				       Inode    *from,
				       guint16   stack_id,
				       BaseName *name,
				       FileType  type_mask,
				       Inode    *to)
{
  Inode copy;

  memcpy (& copy, from, sizeof (copy));

  copy.key.stck = stack_id;
  copy.key.name = name;

  return dbfs_read_indirect_link (txn, & copy, type_mask, to);
}

static gboolean
dbfs_make_container (RepoTxn  *parent_txn,
		     Inode    *inop,
		     FileType  cont_type,
		     gboolean  ordered)
{
  RepoTxn *txn = NULL;

  if (! (txn = dbfs_txn_nest (parent_txn)))
    goto abort;

  if (! dbfs_getnode (txn, inop, TRUE))
    goto abort;

  if (! dbfs_checktype (txn, inop, FT_NotPresent))
    goto abort;

  inop->minor.ino_type = cont_type;
  inop->minor.ino_stack_id = 0;
  inop->minor.ino_flags = ordered ? IF_Ordered : 0;
  inop->minor.ino_content_key = 0;
  inop->minor.ino_segment_len = 0;

  if (! dbfs_init_db (txn, inop))
    goto abort;

  if (! dbfs_putnode (txn, inop, TRUE))
    goto abort;

  if (! dbfs_txn_commit (txn))
    goto abort;

  return TRUE;

 abort:

  if (txn)
    dbfs_txn_abort (txn);

  return FALSE;
}

gboolean
dbfs_make_sequence (RepoTxn  *txn,
		    Inode    *inop)
{
  return dbfs_make_container (txn, inop, FT_Sequence, FALSE);
}

gboolean
dbfs_make_index (RepoTxn  *txn,
		 Inode    *inop,
		 gboolean  ordered)
{
  return dbfs_make_container (txn, inop, FT_Index, ordered);
}

gboolean
dbfs_make_directory (RepoTxn  *txn,
		     Inode    *inop,
		     gboolean  ordered)
{
  return dbfs_make_container (txn, inop, FT_Directory, ordered);
}

#if 0
Path*
dbfs_read_short_path_segment (RepoTxn  *txn,
			      Inode    *inop)
{
  /* Only need this for supporting symlinks
   */
}
#endif

/**********************************************************************/
/*			     Write Handle                             */
/**********************************************************************/

gboolean
dbfs_write_handle_unmap (FileHandle *fh, guint pgno, const guint8** mem)
{
  WriteHandle *wh = (WriteHandle*) fh;

  gssize rem = file_position_rem_on_page_no (& fh->fh_file_len, pgno);

  /* Otherwise this screws up the following statements logic, which
   * causes the first page to be retained by the write handle.
   */
  g_assert (dbfs_fs_short_threshold < fh->fh_cur_pos.page_size);

  if (pgno == 0 && rem <= dbfs_fs_short_threshold)
    goto done;

  if (wh->fd < 0)
    {
      GString *fsloc_str = g_string_new (NULL);

      if (! sequence_allocate_fsloc (wh->parent_txn->dbfs, & wh->fsloc))
	return FALSE;

      if (! (wh->fsloc_path = sequence_fsloc_absolute_path (wh->parent_txn->alloc, wh->parent_txn->dbfs, wh->fsloc)))
	return FALSE;

      path_to_host_string (_fs_pthn, wh->fsloc_path, fsloc_str);

      if ((wh->fd = open (fsloc_str->str, O_WRONLY | O_CREAT | O_TRUNC, 0666)) < 0)
	{
	  dbfs_generate_path_event (EC_DbfsOpenWriteFailed, wh->fsloc_path);
	  return FALSE;
	}

      /* Here, duplicate the FD for a possible later call to fsync.
       */
      idset_add (wh->parent_txn->unref_fs_aborts, wh->fsloc, dup (wh->fd));

      g_string_free (fsloc_str, TRUE);
    }

  g_assert (*mem == wh->pagebuf);

#ifdef DBFS_FILES_IN_MEMPOOL
  /* Pad the page to page_size, so that mmap works in the DB2 mpool
   */

  memset (wh->pagebuf + rem, 0, wh->parent_txn->dbfs->fs_page_size - rem);

  rem = wh->parent_txn->dbfs->fs_page_size;
#endif

  if (write (wh->fd, *mem, rem) != rem)
    {
      dbfs_generate_path_event (EC_DbfsWriteFailed, wh->fsloc_path);
      return FALSE;
    }

 done:

  fh->fh_cur_page = NULL;

  return TRUE;
}

gssize
dbfs_write_handle_map (FileHandle *fh, guint pgno, const guint8** mem)
{
  WriteHandle* wh = (WriteHandle*) fh;

  gssize res = file_position_rem_on_page_no (& fh->fh_file_len, pgno);

  g_assert (res >= 0);

  (*mem) = wh->pagebuf;

  return res;
}

gboolean
dbfs_write_handle_close (FileHandle* fh)
{
  WriteHandle *wh = (WriteHandle*) fh;
  gboolean has_short = wh->fd < 0;
  RepoTxn *txn = NULL;

  if (! (txn = dbfs_txn_nest (wh->parent_txn)))
    goto abort;

  if (wh->inop->minor.ino_type == FT_Segment)
    {
      if (! dbfs_inode_delete_internal (txn, wh->inop, FALSE))
	goto abort;
    }

  wh->inop->minor.ino_segment_len = handle_length (fh);
  wh->inop->minor.ino_stack_id = 0;

  if (has_short)
    {
      wh->inop->minor.ino_flags = IF_Short;
      wh->inop->minor.ino_type = FT_Segment;

      if (! dbfs_putshort (txn, wh->inop, wh->pagebuf))
	goto abort;
    }
  else
    {
      if (close (wh->fd) < 0)
	{
	  dbfs_generate_path_event (EC_DbfsCloseFailed, wh->fsloc_path);
	  goto abort;
	}

      wh->inop->minor.ino_flags = 0;
      wh->inop->minor.ino_type = FT_Segment;
      wh->inop->minor.ino_content_key = wh->fsloc;
    }

  if (! dbfs_putnode (txn, wh->inop, FALSE))
    goto abort;

  if (! dbfs_txn_commit (txn))
    goto abort;

  /*g_print ("replace open: %d; close: %d; free %d\n", open_replace_count, ++close_replace_count, free_replace_count);*/

  return TRUE;

 abort:

  if (txn)
    dbfs_txn_abort (txn);

  return FALSE;
}

void
dbfs_write_handle_abort (FileHandle* fh)
{
}

void
dbfs_write_handle_free (FileHandle* fh)
{
  WriteHandle *wh = (WriteHandle*) fh;

  /*g_print ("replace open: %d; close: %d; free %d\n", open_replace_count, close_replace_count, ++free_replace_count);*/

  g_free (wh->pagebuf);
  g_free (wh);
}

const gchar*
dbfs_write_handle_name (FileHandle *fh)
{
  abort ();
}

/**********************************************************************/
/*			      Read Handle                             */
/**********************************************************************/

gboolean
dbfs_read_handle_map (FileHandle *fh, guint pgno_u, const guint8** mem)
{
  ReadHandle* rh = (ReadHandle*) fh;
  int err;
  db_pgno_t pgno = pgno_u;
  gssize res = file_position_rem_on_page_no (& fh->fh_file_len, pgno_u);

  g_assert (res > 0);

  if (rh->inop->minor.ino_flags & IF_Short)
    {
      if (! rh->short_buf)
	{
	  rh->short_buf = g_malloc (rh->inop->minor.ino_segment_len);

	  if (! dbfs_getshort (rh->parent_txn, rh->inop, rh->short_buf))
	    return -1;
	}

      (* mem) = rh->short_buf;

      return res;
    }

  /* @@@ Note: using the parent_txn here... is that right?  Avoids self-deadlock. */

  if (rh->inop->minor.ino_type == FT_Segment)
    {
      if (! rh->opened)
	{
	  Path *p;
	  GString *fsloc_str = g_string_new (NULL);

#ifdef DBFS_FILES_IN_MEMPOOL
	  if (! (p = sequence_fsloc_path (rh->parent_txn->alloc, rh->inop->minor.ino_content_key)))
	    return -1;

	  path_to_host_string (_fs_pthn, p, fsloc_str);

 	  if ((err = memp_fopen (rh->parent_txn->dbfs->env, fsloc_str->str, DB_RDONLY, 0666,
				 rh->parent_txn->dbfs->fs_page_size, NULL, & rh->mpf)))
	    {
	      dbfs_generate_int_event (EC_DbfsDbMempFopen, err);
	      return -1;
	    }
#else
	  if (! (p = sequence_fsloc_absolute_path (rh->parent_txn->alloc, rh->parent_txn->dbfs, rh->inop->minor.ino_content_key)))
	    return -1;

	  path_to_host_string (_fs_pthn, p, fsloc_str);

	  if ((rh->fd = open (fsloc_str->str, O_RDONLY)) < 0)
	    {
	      dbfs_generate_path_event (EC_DbfsOpenReadFailed, p);
	      return -1;
	    }

	  g_assert (rh->fd > 0);
#endif
	  /*g_print ("fopen %p %s\n", rh->mpf, fsloc_str->str);*/

	  rh->opened = TRUE;

	  g_string_free (fsloc_str, TRUE);
	}

      /*g_print ("fget %p %d\n", rh->mpf, pgno);*/

#ifdef DBFS_FILES_IN_MEMPOOL
      if ((err = memp_fget (rh->mpf, & pgno, 0, (void**) mem)))
	{
	  dbfs_generate_int_event (EC_DbfsDbMempFget, err);
	  return -1;
	}
#else
      if (((*mem) = mmap (NULL, res, PROT_READ, MAP_PRIVATE, rh->fd, (pgno * rh->parent_txn->dbfs->fs_page_size))) == MAP_FAILED)
	{
	  dbfs_generate_int_event (EC_DbfsMmapFailed, errno);
	  return -1;
	}
#endif
    }
  else
    {
      if (! rh->opened)
	{
	  DB_MPOOL_FINFO finfo;

	  memset (& finfo, 0, sizeof (finfo));

	  finfo.clear_len = 1;

	  if (! (rh->view_def = dbfs_view_definition_find (rh->inop->minor.ino_content_key)))
	    {
	      dbfs_generate_int_event (EC_DbfsNoSuchView, rh->inop->minor.ino_content_key);
	      return -1;
	    }

	  rh->opened = TRUE;

	  if (rh->view_def->view_private_size > 0)
	    rh->view_private = g_malloc0 (rh->view_def->view_private_size);

	  if (! rh->view_def->view_begin (rh->parent_txn, rh->inop, rh->view_private))
	    return -1;

	  if ((err = memp_fopen (rh->parent_txn->dbfs->env, NULL, 0, 0666,
				 rh->parent_txn->dbfs->fs_page_size, & finfo, & rh->mpf)))
	    {
	      dbfs_generate_int_event (EC_DbfsDbMempFopen, err);
	      return -1;
	    }
	}

      if ((err = memp_fget (rh->mpf, & pgno, DB_MPOOL_CREATE, (void**) mem)))
	{
	  dbfs_generate_int_event (EC_DbfsDbMempFget, err);
	  return -1;
	}

      if (! rh->view_def->view_pgin (rh->parent_txn,
				     rh->inop,
				     rh->view_private,
				     (guint8*) (* mem),
				     pgno * rh->parent_txn->dbfs->fs_page_size,
				     res))
	return -1;
    }

  return res;
}

gboolean
dbfs_read_handle_unmap (FileHandle *fh, guint pgno, const guint8** mem)
{
  ReadHandle* rh = (ReadHandle*) fh;
#ifndef DBFS_FILES_IN_MEMPOOL
  gssize res = file_position_rem_on_page_no (& fh->fh_file_len, pgno);
#endif
  int err;

  if (rh->inop->minor.ino_flags & IF_Short)
    goto done;

  if (rh->mpf && (err = memp_fput (rh->mpf, (void*) *mem, DB_MPOOL_CLEAN)))
    {
      dbfs_generate_int_event (EC_DbfsDbMempFput, err);
      return FALSE;
    }

#ifndef DBFS_FILES_IN_MEMPOOL
  if (rh->fd > 0 && (munmap ((void*) *mem, res) < 0))
    {
      dbfs_generate_int_event (EC_DbfsMunmapFailed, errno);
      return FALSE;
    }
#endif

  /*g_print ("fput %p %d\n", rh->mpf, pgno);*/

 done:

  (* mem) = NULL;

  return TRUE;
}

gboolean
dbfs_read_handle_close (FileHandle* fh)
{
  ReadHandle* rh = (ReadHandle*) fh;
  int err;

  if (rh->mpf && (err = memp_fclose (rh->mpf)))
    {
      dbfs_generate_int_event (EC_DbfsDbMempFclose, err);
      return FALSE;
    }

#ifndef DBFS_FILES_IN_MEMPOOL
  if (rh->fd > 0)
    close (rh->fd);
#endif

  if (rh->view_def)
    {
      if (! rh->view_def->view_end (rh->parent_txn, rh->inop, rh->view_private))
	return FALSE;
    }

  /*g_print ("read open: %d; close: %d; free %d\n", open_read_count, ++close_read_count, free_read_count);*/

  return TRUE;
}

void
dbfs_read_handle_abort (FileHandle* fh)
{
}

void
dbfs_read_handle_free (FileHandle* fh)
{
  ReadHandle* rh = (ReadHandle*) fh;

  if (rh->short_buf)
    {
      g_free (rh->short_buf);
    }

  if (rh->view_private)
    g_free (rh->view_private);

  /*g_print ("read open: %d; close: %d; free %d\n", open_read_count, close_read_count, ++free_read_count);*/

  g_free (fh);
}

const gchar*
dbfs_read_handle_name (FileHandle *fh)
{
  abort ();
}

/* Other stuff
 */

static const char*
dbfs_inode_format_type (Inode *inop)
{
  switch (inop->minor.ino_type)
    {
    case FT_Invalid:
      return "Invalid";
    case FT_NotFound:
      return "NotFound";
    case FT_Null:
      return "Null";
    case FT_NotPresent:
      return "NotPresent";
    case FT_Directory:
      return "Directory";
    case FT_Sequence:
      return "Sequence";
    case FT_Index:
      return "Index";
    case FT_Relation:
      return "Relation";
    case FT_Symlink:
      return "Symlink";
    case FT_Segment:
      return "Segment";
    case FT_Indirect:
      return "Indirect";
    case FT_View:
      return "View";
    }

  abort ();
}

static gboolean
dbfs_list_container (RepoTxn    *txn,
		     FileHandle *writeto,
		     ListFlags   lf,
		     Path       *path,
		     Inode      *inop)
{
  gboolean directory_recurse = (lf & LF_Recursive);
  Cursor* c = NULL;

  if (! (c = dbfs_cursor_open (txn, inop, CT_Container)))
    goto abort;

  while (dbfs_cursor_next (c))
    {
      Inode one_ino;

      if (! dbfs_cursor_inode (c, FT_IsAny, & one_ino))
	goto abort;

      if (! dbfs_list_recursive (txn, writeto, lf | LF_NotRecursive,
				 path_append_basename (txn->alloc, path, PP_Auto, dbfs_cursor_basename (c)),
				 one_ino.key.inum, one_ino.key.stck))
	goto abort;
    }

  if (! dbfs_cursor_close (c))
    return FALSE;

  c = NULL;

  if (directory_recurse)
    {
      if (! (c = dbfs_cursor_open (txn, inop, CT_Container)))
	goto abort;

      while (dbfs_cursor_next (c))
	{
	  Inode one_ino;

	  if (! dbfs_cursor_inode (c, FT_IsAny, & one_ino))
	    goto abort;

	  switch (one_ino.minor.ino_type)
	    {
	    case FT_Directory:
	    case FT_Sequence:
	    case FT_Index:

	      {
		Path* p = path_append_basename (txn->alloc, path, PP_Auto, dbfs_cursor_basename (c));
		GString *s = g_string_new (NULL);

		path_to_host_string (_print_pthn, p, s);

		if (! handle_printf (writeto, "Directory %s:\n", s->str))
		  goto abort;

		if (! dbfs_list_recursive (txn, writeto, lf, p, one_ino.key.inum, one_ino.key.stck))
		  goto abort;

		g_string_free (s, TRUE);
	      }

	      break;
	    }
	}

      if (! dbfs_cursor_close (c))
	return FALSE;

      c = NULL;
    }

  return TRUE;

 abort:

  if (c)
    dbfs_cursor_close (c);

  return FALSE;
}

static gboolean
dbfs_print_segment (RepoTxn    *txn,
		    FileHandle *writeto,
		    ListFlags   lf,
		    Path       *path,
		    Inode      *inop)
{
  GString *s = g_string_new (NULL), *t = g_string_new (NULL);
  path_to_host_string (_print_pthn, path, s);
  path_basename_to_host_string (_print_pthn, PP_Hex, inop->key.name, t);

  handle_printf (writeto, "%04qd %02d %8qd %-9s %s %s\n", inop->key.inum, inop->key.stck, inop->minor.ino_segment_len, dbfs_inode_format_type (inop), s->str, t->str);

  g_string_free (s, TRUE);
  g_string_free (t, TRUE);
  return TRUE;
}

static gboolean
dbfs_print_container (RepoTxn    *txn,
		      FileHandle *writeto,
		      ListFlags   lf,
		      Path       *path,
		      Inode      *inop)
{
  return dbfs_print_segment (txn, writeto, lf, path, inop);
}

static gboolean
dbfs_print_view (RepoTxn    *txn,
		 FileHandle *writeto,
		 ListFlags   lf,
		 Path       *path,
		 Inode      *inop)
{
  return dbfs_print_segment (txn, writeto, lf, path, inop);
}

static gboolean
dbfs_print_indirect (RepoTxn    *txn,
		     FileHandle *writeto,
		     ListFlags   lf,
		     Path       *path,
		     Inode      *inop)
{
  return dbfs_print_segment (txn, writeto, lf, path, inop);
}

gboolean
dbfs_list_recursive (RepoTxn    *txn,
		     FileHandle *writeto,
		     ListFlags   lf,
		     Path       *path,
		     guint64     inum,
		     guint16     stck)
{
  Cursor *c = NULL;
  Inode ino;
  gboolean recursive = !(lf & LF_NotRecursive);

  ino.key.inum = inum;
  ino.key.stck = stck;
  ino.key.name = _dbfs_default_name;

  if (! (c = dbfs_cursor_open (txn, & ino, CT_MinorInodes)))
    return FALSE;

  while (dbfs_cursor_next (c))
    {
      if (! dbfs_cursor_inode (c, FT_IsPresent, & ino))
	return FALSE;

      if (! (lf & LF_ShowNamed) && ino.key.name->len > 0)
	continue;

      switch (ino.minor.ino_type)
	{
	case FT_Directory:
	case FT_Sequence:
	case FT_Index:

	  if (recursive)
	    {
	      if (! dbfs_list_container (txn, writeto, lf, path, & ino))
		goto abort;
	    }
	  else
	    {
	      if (! dbfs_print_container (txn, writeto, lf, path, & ino))
		goto abort;
	    }

	  break;

	case FT_Segment:
	  if (! dbfs_print_segment (txn, writeto, lf, path, & ino))
	    goto abort;
	  break;

	case FT_Indirect:
	  if (! dbfs_print_indirect (txn, writeto, lf, path, & ino))
	    goto abort;
	  break;

	case FT_View:
	  if (! dbfs_print_view (txn, writeto, lf, path, & ino))
	    goto abort;

	  /* @@@ Changed the meaning of CT_MinorInodes to deal with XDFS
	   * placing stuff in stacks that aren't viewed. */
	  /*if (! dbfs_list_recursive (txn, writeto, lf, path, inum, ino.minor.ino_stack_id))
	    goto abort;*/

	  break;

	case FT_Symlink:
	case FT_Relation:
	default:
	  abort ();
	}
    }

  if (! dbfs_cursor_close (c))
    return FALSE;

  return TRUE;

 abort:

  if (c)
    dbfs_cursor_close (c);

  return FALSE;
}

gboolean
dbfs_list (RepoTxn    *txn,
	   FileHandle *writeto,
	   ListFlags   lf)
{
  if (! handle_printf (writeto, "Directory /:\n"))
    return FALSE;

  return dbfs_list_recursive (txn, writeto, lf, path_root (txn->alloc), INODE_ROOT, DBFS_DEFAULT_STACK_ID);
}

void
dbfs_print (RepoTxn *txn)
{
  dbfs_list (txn, _stdout_handle, LF_Recursive | LF_ShowNamed | LF_ShowStacked);
}

/* Lingering read handles
 */

gint
inum_equal (gconstpointer v1,
	     gconstpointer v2)
{
  return *((const guint64*) v1) == *((const guint64*) v2);
}

guint
inum_hash (gconstpointer v)
{
  return (guint) (*(const guint64*) v);
}

void
dbfs_linger_close_fh (gpointer	key,
		      gpointer	value,
		      gpointer	user_data)
{
  handle_close (value);
  handle_free (value);
}

LingerHandle*
dbfs_linger_open (RepoTxn *txn)
{
  LingerHandle* lh = g_new0 (LingerHandle, 1);

  lh->txn = txn;
  lh->table = g_hash_table_new (inum_hash, inum_equal);

  return lh;
}

void
dbfs_linger_close (LingerHandle *lh)
{
  g_hash_table_foreach (lh->table, dbfs_linger_close_fh, NULL);
  g_hash_table_destroy (lh->table);
  g_free (lh);
}

FileHandle*
dbfs_inode_open_linger (LingerHandle *lh,
			Inode        *inop)
{
  FileHandle *fh;

  if ((fh = g_hash_table_lookup (lh->table, & inop->key.inum)))
    return fh;

  if (! (fh = dbfs_inode_open_read (lh->txn, inop)))
    return NULL;

  g_hash_table_insert (lh->table, & inop->key.inum, fh);

  lh->unique += 1;

  return fh;
}
