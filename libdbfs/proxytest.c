/* -*-Mode: C;-*-
 * $Id: proxytest.c 1.3 Sun, 16 Apr 2000 12:37:07 -0700 jmacd $
 * file.c:
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "dbfs.h"
#include <xdelta.h>
#include <edsiostdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <dirent.h>

typedef struct _ProxySet ProxySet;

struct _ProxySet {
  DBFS        *dbfs;
  RepoTxn     *txn;
};

ProxySet server;
ProxySet client;

Path        *xdfs_loc;

char        *tmp_file;

long long one_encoded = 0;
long long one_unencoded = 0;

long long total_encoded = 0;
long long total_unencoded = 0;

XdfsPolicy   proxy_policy;

gboolean     ptest_verify          = FALSE;
gint32       ptest_min_versions    = 0;
gint32       ptest_max_versions    = G_MAXINT;
const char*  ptest_method          = NULL;
const char*  ptest_base_dir1       = NULL;
const char*  ptest_base_dir2       = NULL;

static ConfigOption options [] = {
  { "min_versions",      "min",    CS_Use,    CO_Optional, CD_Int32,   & ptest_min_versions },
  { "max_versions",      "max",    CS_Use,    CO_Optional, CD_Int32,   & ptest_max_versions },
  { "verify",            "vfy",    CS_Use,    CO_None,     CD_Bool,    & ptest_verify },
  { "method",            "mth",    CS_Use,    CO_Required, CD_String,  & ptest_method },
  { "archive_base1",     "ab1",    CS_Ignore, CO_Required, CD_String,  & ptest_base_dir1 },
  { "archive_base2",     "ab2",    CS_Ignore, CO_Required, CD_String,  & ptest_base_dir2 },
};

gboolean
proxy_create_xdfs (RepoTxn *txn, Path *loc)
{
  Inode ino;
  Inode root;
  XdfsParams params;

  if (! dbfs_inode_find_root (txn, path_root (txn->alloc), FALSE, FT_Directory, & root))
    return FALSE;

  if (! dbfs_inode_new (txn, & ino))
    return FALSE;

  if (! dbfs_make_directory (txn, & ino, TRUE))
    return FALSE;

  if (! dbfs_link_create (txn, & root, & ino, path_basename (loc), FALSE))
    return FALSE;

  memset (& params, 0, sizeof (params));

  params.policy = proxy_policy;
  params.flags  = XF_MD5Equality | XF_StoreDigest;

  if (! xdfs_location_create (txn, loc, & params))
    return FALSE;

  return TRUE;
}

gboolean
proxy_insert (RepoTxn* txn, const char* filename)
{
  FileHandle *dfh;
  const guint8* computed_digest;
  guint8 read_digest[16];
  Inode ino, d_ino;
  BaseName bn;
  const MessageDigest* md;
  FileHandle *fh;

  /* FH is the file handle for reading the new version. */
  if (! (fh = handle_read_file (filename)))
    return FALSE;

  /* Just for demonstration purposes, compute the MD5 of this
   * file handle.  The EDSIO library computes the MD5 as XDFS
   * reads the file handle, and the result is available after
   * it is fully read.
   */
  md = edsio_message_digest_md5 ();

  if (! handle_digest_compute (fh, md))
    return FALSE;

  /* INO is the inode returned by XDFS that stores the new version. */
  if (! xdfs_insert_version (txn, xdfs_loc, fh, & ino))
    return FALSE;

  /* Now get the computed digest value from the read file handle.
   * (XDFS performs the same operation on the write handle as it
   * copies the file into its archive location.
   */
  if (! (computed_digest = handle_digest (fh, md)))
    return FALSE;

  /* More demonstration: The XF_StoreDigest flag provided in
   * proxy_create_xdfs() causes XDFS to store the digest value in a
   * minor inode along with each inserted version.  Use the value
   * computed here to verify that it is set correctly.
   */

  /* First build a BaseName object from the message digest name which
   * is in this case "MD5".  Its size includes the terminating zero.
   */
  bn.data = (char*) md->name;
  bn.len  = strlen (bn.data) + 1;

  /* The call to open_read_named opens a named minor inode related to
   * INO.  D_INO is set to the minor inode being read, taking its
   * major inode number, its stack identifier from INO, and its name
   * from BN. */

  if (! (dfh = dbfs_inode_open_read_named (txn, & ino, & bn, & d_ino)))
    return FALSE;

  /* The read handle DFH should contain an MD5 checksum, verify its
   * length. */
  g_assert (handle_length (dfh) == md->cksum_len);
  g_assert (handle_length (dfh) == 16); /* MD5 == 16 bytes */

  /* Read and compare values.
   */
  if (handle_read (dfh, read_digest, 16) != 16)
    return FALSE;

  if (memcmp (read_digest, computed_digest, 16) != 0)
    {
      /* If the comparison fails...
       */
      g_error ("digest comparison failed");
    }

  /* Cleanup
   */

  handle_free  (fh);
  handle_close (dfh);
  handle_free  (dfh);

  return TRUE;
}

gboolean
proxy_last_version (RepoTxn *txn, Inode *inop_out)
{
  Inode seq_ino;

  if (! xdfs_container_sequence (txn, xdfs_loc, & seq_ino))
    return FALSE;

  if (! dbfs_sequence_max (txn, & seq_ino, inop_out))
    return FALSE;

  return TRUE;
}

gboolean
proxy_md5 (RepoTxn *txn, Inode *inop, guint8 read_digest[16])
{
  FileHandle *dfh;
  Inode d_ino;
  BaseName bn;
  const MessageDigest* md;

  md = edsio_message_digest_md5 ();

  bn.data = (char*) md->name;
  bn.len  = strlen (bn.data) + 1;

  if (! (dfh = dbfs_inode_open_read_named (txn, inop, & bn, & d_ino)))
    return FALSE;

  g_assert (handle_length (dfh) == md->cksum_len);
  g_assert (handle_length (dfh) == 16); /* MD5 == 16 bytes */

  if (handle_read (dfh, read_digest, 16) != 16)
    return FALSE;

  handle_close (dfh);
  handle_free  (dfh);

  return TRUE;
}

gboolean
proxy_lookup_md5 (RepoTxn* txn, guint8 md5[16], Inode* ino)
{
  Inode idx_ino;
  Path *p = path_append_bytes (txn->alloc, path_root (txn->alloc), PP_Hex, md5, 16);

  if (! xdfs_digest_index (txn, xdfs_loc, & idx_ino))
    return FALSE;

  if (! dbfs_inode_find (txn, & idx_ino, p, DbfsNoFollowLinks, FT_CanRead, ino))
    return FALSE;

  return TRUE;
}

gboolean
proxy_verify (const char* file, RepoTxn* txn, Inode *check)
{
  FileHandle *orig, *retr;
  guint8 obuf[1024], rbuf[1024];
  int offset = 0;
  int oc, rc;
  int i;

  if (! (orig = handle_read_file (file)))
    return FALSE;

  if (! (retr = dbfs_inode_open_read (txn, check)))
    return FALSE;

  for (;;)
    {
      oc = handle_read (orig, obuf, 1024);
      rc = handle_read (retr, rbuf, 1024);

      if (oc < 0 || rc < 0)
	g_error ("read failed: verify\n");

      if (oc != rc)
	{
	  fprintf (stderr, "verify failed: different lengths: %d != %d\n", oc, rc);
	  return FALSE;
	}

      if (oc == 0)
	break;

      for (i = 0; i < oc; i += 1)
	{
	  if (obuf[i] != rbuf[i])
	    {
	      fprintf (stderr, "verify failed: different content at offset: %d\n", offset+i);
	      return FALSE;
	    }
	}

      offset += oc;
    }

  handle_close (orig);
  handle_close (retr);

  handle_free (orig);
  handle_free (retr);

  return TRUE;
}

gboolean
proxy_dateorder (RcsFile* rcs, RcsVersion *v, void* data)
{
  static int progress = 0;

  Inode  server_current_ino;
  Inode  server_client_ino;

  Inode  client_current_ino;
  Inode  client_update_ino;

  guint8 server_current_md5[16];
  guint8 client_current_md5[16];

  FileHandle *d_out;
  FileHandle *d_in;

  if ((++progress % 1000) == 0)
    fprintf (stderr, "i %d\n", progress);

  if (! server.txn)
    {
      if (! (server.txn = dbfs_txn_begin (server.dbfs, DBFS_TXN_FLAT)))
 	return FALSE;

      if (! (client.txn = dbfs_txn_begin (client.dbfs, DBFS_TXN_FLAT)))
 	return FALSE;
    }

  if (! xdfs_loc)
    {
      xdfs_loc = path_append_bytes (server.txn->alloc, path_root (server.txn->alloc),
				    PP_String, rcs->filename, strlen (rcs->filename));

      if (! proxy_create_xdfs (server.txn, xdfs_loc))
	return FALSE;

      if (! proxy_create_xdfs (client.txn, xdfs_loc))
	return FALSE;
    }

  /*g_print ("Proxytest...: %d: %s: %s\n", progress, rcs->filename, v->vname);*/

  /* Step 1: New version created at the server side.
   */
  if (! proxy_insert (server.txn, v->filename))
    return FALSE;

  /* Step 2: Get the server's new version (and its MD5...)
   */

  if (! proxy_last_version (server.txn, & server_current_ino))
    return FALSE;

  if (! proxy_md5 (server.txn, & server_current_ino, server_current_md5))
    return FALSE;

  /* Step 3: Get the MD5 of the client's latest version.
   */
  if (! proxy_last_version (client.txn, & client_current_ino))
    return FALSE;

  /* Note the test for whether there was a last version at all.  The
   * server case above is guaranteed to have one because it was just
   * insterted in step 1, but the client is not.
   */
  if (client_current_ino.minor.ino_type == FT_NotFound)
    {
      /* Step 3.5: Client has no past versions.  Insert the server
       * version, return.
       */

      if (! proxy_insert (client.txn, v->filename))
	return FALSE;

      return TRUE;
    }

  if (! proxy_md5 (client.txn, & client_current_ino, client_current_md5))
    return FALSE;

  /* Step 4: Lookup the client's MD5 on the server side.
   */
  if (! proxy_lookup_md5 (server.txn, client_current_md5, & server_client_ino))
    return FALSE;

  /* Step 5: Extract a delta for the client.
   */
  if (! (d_out = handle_write_file (tmp_file)))
    return FALSE;

  if (! xdfs_extract_delta (server.txn, xdfs_loc, & server_client_ino, & server_current_ino, d_out))
    return FALSE;

  /* Step 5.5: Close and free the handle.  Now the delta is sitting in tmp_file.
   */
  if (! handle_close (d_out))
    return FALSE;

  handle_free (d_out);

  /* Step 6: Insert the delta on the client side.
   */
  if (! (d_in = handle_read_file (tmp_file)))
    return FALSE;

  if (! xdfs_insert_delta (client.txn, xdfs_loc, & client_current_ino, d_in, & client_update_ino))
    return FALSE;

  /* Step 6.5: Close and free the handle.
   */
  if (! handle_close (d_in))
    return FALSE;

  handle_free (d_in);

  /* Step 7: Verify that client_update_ino matches the new version.
   */
  if (! proxy_verify (v->filename, client.txn, & client_update_ino))
    return FALSE;

  return TRUE;
}

gboolean
proxy_onefile (RcsFile *rcs, RcsStats* set, void* data)
{
  if (server.txn)
    {
      if (! dbfs_txn_commit (server.txn))
	return FALSE;

      server.txn = NULL;

      if (! dbfs_txn_commit (client.txn))
	return FALSE;

      client.txn = NULL;

      total_unencoded += one_unencoded;
      total_encoded   += one_encoded;

      one_unencoded = 0;
      one_encoded = 0;
    }

  xdfs_loc = NULL;

  return TRUE;
}

DBFS*
proxy_init (const char* dir)
{
  Path *archive_path;

  if (! (archive_path = path_from_host_string (NULL, _fs_pthn, dir)))
    return NULL;

  return dbfs_create (archive_path, NULL);
}

RcsWalker proxy_walker = {
  NULL,
  NULL,
  & proxy_onefile,
  & proxy_dateorder,
  NULL,
  NULL,
  0,
  G_MAXINT,
  TRUE
};

int
main (int argc, char** argv)
{
  int   i;

  rcswalk_init ();

  tmp_file = g_strdup_printf ("%s/pt.%d", g_get_tmp_dir (), (int) getpid ());

  config_register (options, ARRAY_SIZE (options));

  config_set_string ("rcswalk_experiment", "pt");

  if (argc < 2)
    {
      config_help ();
      goto bail;
    }

  for (i = 1; i < argc; i += 1)
    {
      if (! config_parse (argv[i]))
	goto bail;
    }

  if (! config_done ())
    goto bail;

  if (strcmp (ptest_method, "XDFS-rj") == 0)
    proxy_policy = XP_ReverseJump;
  else if (strcmp (ptest_method, "XDFS-fj") == 0)
    proxy_policy = XP_ForwardJump;
  else if (strcmp (ptest_method, "XDFS-f") == 0)
    proxy_policy = XP_Forward;
  else
    g_error ("unknown TEST: %s\n", ptest_method);

  proxy_walker.min_versions = ptest_min_versions;
  proxy_walker.max_versions = ptest_max_versions;

  fprintf (stderr, "proxytest: verification %s\n", ptest_verify ? "on" : "off");

  if (! config_clear_dir (ptest_base_dir1))
    goto bail;

  if (! config_clear_dir (ptest_base_dir2))
    goto bail;

  if (! dbfs_library_init ())
    goto bail;

  if (! xdfs_library_init ())
    goto bail;

  if (! (server.dbfs = proxy_init (ptest_base_dir1)))
    goto bail;

  if (! (client.dbfs = proxy_init (ptest_base_dir2)))
    goto bail;

  if (! rcswalk (& proxy_walker, NULL))
    goto bail;

  unlink (tmp_file);

  return 0;

 bail:

  if (server.txn) dbfs_txn_abort (server.txn);
  if (client.txn) dbfs_txn_abort (client.txn);

  if (server.dbfs) dbfs_close (server.dbfs);
  if (client.dbfs) dbfs_close (client.dbfs);

  dbfs_library_close ();

  unlink (tmp_file);

  return 2;
}
