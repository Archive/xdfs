#include <edsio.h>
#include <edsiostdio.h>
#include <ctype.h>
#include "dbfs.h"

/* Warning: very cheesy!
 */

#ifdef DEBUG_EXTRACT
  FileHandle *fh2 = handle_read_file (filename);

  guint8* debug_buf = g_malloc (buflen);

  if (! handle_read (fh2, debug_buf, buflen))
    g_error ("read failed");
#endif

gboolean
rcs_count (const char* filename, guint *encoded_size)
{
  char *readbuf0, *readbuf;
  gboolean in_string = FALSE;
  gboolean in_text = FALSE;
  guint string_start = 0;
  guint string_end = 0;
  guint current_pos = 0;
  /*char *current_delta = NULL;*/
  FileHandle *fh = handle_read_file (filename);
  guint buflen = handle_length (fh);

  (* encoded_size) = 0;

  readbuf0 = g_new (guint8, buflen);

  for (;;)
    {
      int c = handle_gets (fh, readbuf0, buflen);

      readbuf = readbuf0;

      if (c < 0)
	break;

      if (strncmp (readbuf, "text", 4) == 0)
	in_text = TRUE;

      if (! in_string && readbuf[0] == '@')
	{
	  string_start = current_pos + 1;
	  in_string = TRUE;
	  readbuf += 1;
	}

      current_pos += c;

      if (in_string)
	{
	  while ((readbuf = strchr (readbuf, '@')))
	    {
	      if (readbuf[1] == '@')
		{
		  string_start += 1; /* @@@ bogus, just counting. */
		  readbuf += 2;
		  continue;
		}

	      in_string = FALSE;
	      break;
	    }

	  string_end = current_pos - 2;

	  if (in_text && ! in_string)
	    {
	      in_text = FALSE;

	      /*g_free (current_delta);
		current_delta = NULL;*/

	      (* encoded_size) += (string_end - string_start);
	    }

	  continue;
	}

      if (isdigit (readbuf[0]))
	{
#if 0
	  (* strchr (readbuf, '\n')) = 0;
	  if (current_delta)
	    g_free (current_delta);
	  current_delta = g_strdup (readbuf);
#endif
	}
    }

  handle_close (fh);

  g_free (readbuf0);

#if 0
  if (current_delta)
    g_free (current_delta);
#endif

  return TRUE;
}

#ifndef IN_ARCHIVETEST
int
main (int argc, char** argv)
{
  guint size;

  if (argc != 2)
    g_error ("usage: %s RCS_file\n", argv[0]);

  if (! rcs_count (argv[1], &size))
    g_error ("rcs_parse failed");

  return 0;
}
#endif
