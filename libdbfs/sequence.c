/* -*-Mode: C;-*-
 * $Id: sequence.c 1.4 Mon, 01 May 2000 12:46:35 -0700 jmacd $
 *
 * Copyright (C) 1999, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "dbfs.h"

#include <errno.h>
#include <sys/types.h> /* Needed for mkdir() */
#include <sys/stat.h>

gboolean
sequence_allocate (DBFS* dbfs, guint64 seqkey, guint64* valp)
{
  DB_TXN *db_txn = NULL;
  DBT key, data;
  db_recno_t rn;
  int err;
  DB *seq_dbp = dbfs->sequences_dbp;

  if ((err = txn_begin (dbfs->env, NULL, & db_txn, DB_TXN_NOSYNC)))
    {
      dbfs_generate_int_event (EC_DbfsDbTxnBegin, err);
      return FALSE;
    }

  dbfs_clear_dbts (& key, & data);

  rn = seqkey;

  key.data = & rn;
  key.size = sizeof (rn);

  data.data = valp;
  data.ulen = sizeof (guint64);
  data.flags = DB_DBT_USERMEM;

  if ((err = seq_dbp->get (seq_dbp, db_txn, & key, & data, 0)))
    {
      dbfs_generate_int_event (EC_DbfsDbGet, err);
      goto cleanup;
    }

  if (data.size != sizeof (guint64))
    {
      dbfs_generate_void_event (EC_DbfsInvalidRecordLength);
      goto cleanup;
    }

  (* valp) += 1;

  if ((err = seq_dbp->put (seq_dbp, db_txn, & key, & data, 0)))
    {
      dbfs_generate_int_event (EC_DbfsDbPut, err);
      goto cleanup;
    }

  if ((err = txn_commit (db_txn, 0)))
    {
      dbfs_generate_int_event (EC_DbfsDbTxnCommit, err);
      return FALSE;
    }

  return TRUE;

 cleanup:

  if (db_txn && (err = txn_abort (db_txn)))
    dbfs_generate_int_event (EC_DbfsDbTxnAbort, err);

  return FALSE;
}

static gboolean
sequence_create_internal (DBFS* dbfs, guint64 *seqkey, guint64 init_val)
{
  DB_TXN *db_txn = NULL;
  DBT key, data;
  guint64 data_be;
  db_recno_t rn;
  int err;
  DB *seq_dbp = dbfs->sequences_dbp;

  if ((err = txn_begin (dbfs->env, NULL, & db_txn, DB_TXN_NOSYNC)))
    {
      dbfs_generate_int_event (EC_DbfsDbTxnBegin, err);
      return FALSE;
    }

  dbfs_clear_dbts (& key, & data);

  key.data = & rn;
  key.size = sizeof (db_recno_t);
  key.ulen = sizeof (db_recno_t);
  key.flags = DB_DBT_USERMEM;

  dbfs_init_u64_key (& data, & data_be, init_val);

  if ((err = seq_dbp->put (seq_dbp, db_txn, & key, & data, DB_APPEND)))
    {
      dbfs_generate_int_event (EC_DbfsDbPut, err);
      goto cleanup;
    }

  if ((err = txn_commit (db_txn, 0)))
    {
      dbfs_generate_int_event (EC_DbfsDbTxnCommit, err);
      return FALSE;
    }

  (* seqkey) = rn;

  return TRUE;

 cleanup:

  if (db_txn && (err = txn_abort (db_txn)))
    dbfs_generate_int_event (EC_DbfsDbTxnAbort, err);

  return FALSE;
}

gboolean
sequence_create_known (DBFS* dbfs, guint64 seqkey_assert, guint64 init_val)
{
  guint64 seqkey;

  if (! sequence_create_internal (dbfs, & seqkey, init_val))
    return FALSE;

  g_assert (seqkey_assert == seqkey);

  return TRUE;
}

gboolean
sequence_allocate_fsloc (DBFS* dbfs, guint64* valp)
{
  Path *last_dir, *last_path, *this_path, *this_dir;
  Allocator* alloc = allocator_new ();
  int err;

  if (! sequence_allocate (dbfs, SEQUENCE_KEY_FSLOC, valp))
    return FALSE;

  if (! (this_path = sequence_fsloc_absolute_path (alloc, dbfs, *valp)))
    return FALSE;

  this_dir = path_dirname (alloc, this_path);

  if (! (last_path = sequence_fsloc_absolute_path (alloc, dbfs, dbfs->seq_last_path_val)))
    return FALSE;

  last_dir = path_dirname (alloc, last_path);

  if (! dbfs->seq_once || ! path_equal (this_dir, last_dir))
    {
      GString *dir_str = g_string_new (NULL);

      path_to_host_string (_fs_pthn, this_dir, dir_str);

      if ((err = mkdir (dir_str->str, 0777)) && errno != EEXIST)
	{
	  dbfs_generate_pathint_event (EC_DbfsMkdirFailed, this_dir, err);
	  return FALSE;
	}

      g_string_free (dir_str, TRUE);

      dbfs->seq_last_path_val = *valp;
      dbfs->seq_once = TRUE;
    }

  allocator_free (alloc);

  return TRUE;
}

Path*
sequence_fsloc_absolute_path (Allocator* alloc, DBFS *dbfs, guint64 key)
{
  Path *p = sequence_fsloc_path (alloc, key);

  if (! p)
    return NULL;

  return path_append_path (alloc, dbfs->fs_base, p);
}

Path*
sequence_fsloc_path (Allocator* alloc, guint64 key)
{
  guint8   split_buf[16];
  int      split_i = 0;
  Path    *p = path_append_string (alloc, path_current (alloc), PP_Hex, "files");

  do
    {
      split_buf[split_i++] = key & 0x3f;
      key >>= 6;
    }
  while (key != 0);

  for (; --split_i >= 1;)
    p = path_append_format (alloc, p, PP_Hex, "d%02o", split_buf[split_i]);

  p = path_append_format (alloc, p, PP_Hex, "f%02o", split_buf[0]);

  return p;
}

/* IDSet */

IDSet*
idset_new (const char* name)
{
  IDSet *ids = g_new0 (IDSet, 1);

  ids->name = name;

  return ids;
}

void
idset_free (IDSet* ids)
{
  IDSetElt *s = ids->set;
  IDSetElt *tmp;

  while (s)
    {
      tmp = s;
      s = s->next;

      g_free (tmp);
    }

  g_free (ids);
}

void
idset_add (IDSet* ids, guint64 id, int data)
{
  IDSetElt *elt = g_new0 (IDSetElt, 1);

  elt->id = id;
  elt->data = data;
  elt->next = ids->set;
  ids->set = elt;
}

gboolean
idset_del (IDSet* ids, guint64 id, int *data)
{
  IDSetElt *tmp;
  IDSetElt *prev;

  prev = NULL;
  tmp = ids->set;

  while (tmp)
    {
      if (tmp->id == id)
	{
	  if (prev)
	    prev->next = tmp->next;
	  else if (ids->set == tmp)
	    ids->set = ids->set->next;

	  if (data)
	    (*data) = tmp->data;

	  g_free (tmp);

	  return TRUE;
	}

      prev = tmp;
      tmp = tmp->next;
    }

  return FALSE;
}

void
idset_dmerge (IDSet* to, IDSet *free)
{
  IDSetElt *tmp = free->set;

  for (; tmp && tmp->next != NULL; tmp = tmp->next)
    ;

  if (tmp)
    {
      tmp->next = to->set;
      to->set = tmp;
    }

  g_free (free);
}
