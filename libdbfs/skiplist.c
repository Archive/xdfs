/* -*-Mode: C;-*-
 * PRCS - The Project Revision Control System
 * Copyright (C) 1997, 1998, 1999  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: skiplist.c 1.5 Mon, 21 Feb 2000 07:52:33 -0800 jmacd $
 */

#include "dbfs.h"

/* I've taken this skip list implementation from the top-down 1-2-3
 * skip list example code in J. Ian Munro, Thomas Papadakis, and
 * Robert Sedgewick's "Deterministic Skip Lists" paper.  It is a very
 * nice data structure, they argue, because it is exceptionally simple
 * to understand and implement, yet competes with the more complicated
 * balanced tree approaches for time and space.  I have to agree, but
 * it is more complicated with the addition of intervals. I should
 * probably not use the top-down 1-2-3 skip list for intervals,
 * because its not a truly top-down process.  This means potentially
 * linear performance for overlapping insert regions, but its quite
 * contrived.  I'll weigh the impact of this decision later.  */

struct _SkipList {
  guint32 length;

  SkipListNode* head;
  SkipListNode* bottom;
  SkipListNode* tail;

  GMemChunk *chunk;
};

struct _SkipListNode {
  guint32       low;
  guint32       high;
  SkipListData  data;
  SkipListNode *right;
  SkipListNode *down;
};

SkipList*
xdfs_skip_list_new (guint32 length)
{
  SkipList *skp = g_new0 (SkipList, 1);

  skp->chunk  = g_mem_chunk_create (SkipListNode, 64, G_ALLOC_ONLY);

  skp->length = length;

  skp->head   = g_chunk_new (SkipListNode, skp->chunk);
  skp->bottom = g_chunk_new (SkipListNode, skp->chunk);
  skp->tail   = g_chunk_new (SkipListNode, skp->chunk);

  skp->head->low      = skp->length;
  skp->head->high     = skp->length;
  skp->head->right    = skp->tail;
  skp->head->down     = skp->bottom;

  skp->tail->low      = skp->length + 1;
  skp->tail->high     = skp->length + 1;
  skp->tail->right    = skp->tail;
  skp->tail->down     = NULL;

  skp->bottom->low    = 0;
  skp->bottom->high   = 0;
  skp->bottom->right  = skp->bottom;
  skp->bottom->down   = skp->bottom;

  return skp;
}

void
xdfs_skip_list_free (SkipList* skp)
{
  g_mem_chunk_destroy (skp->chunk);
  g_free (skp);
}

SkipListNode*
xdfs_skip_list_search (SkipList* skp,
		       guint32   search_offset)
{
  SkipListNode *x;
  SkipListNode *bottom = skp->bottom;

  g_assert (search_offset < skp->length);

  bottom->low  = search_offset;
  bottom->high = search_offset;

  for (x = skp->head; x != bottom; x = x->down)
    {
      /* [while (v > x->key) x = x->r] */
      while (search_offset >= x->high)
	x = x->right;

      /* [if (x->d == bottom) return ((v == x->key) ? x : bottom)] */
      if (x->down == bottom)
	{
	  if ((search_offset >= x->low) && (search_offset < x->high))
	    return x;
	  else
	    return NULL;
	}
    }

  abort ();
}

SkipListNode*
xdfs_skip_list_search_nearest (SkipList* skp,
			       guint32   search_offset)
{
  SkipListNode *x;
  SkipListNode *bottom = skp->bottom;

  g_assert (search_offset < skp->length);

  bottom->low  = search_offset;
  bottom->high = search_offset;

  for (x = skp->head; x != bottom; x = x->down)
    {
      /* [while (v > x->key) x = x->r] */
      while (search_offset >= x->high)
	x = x->right;

      /* [if (x->d == bottom) return ((v == x->key) ? x : bottom)] */
      if (x->down == bottom)
	return x;
    }

  abort ();
}

guint
xdfs_skip_list_insert (SkipList    *skp,
		       guint32      insert_offset,
		       guint32      insert_length,
		       SkipListData data)
{
  SkipListNode *x;
  SkipListNode *bottom = skp->bottom;
  guint         inserted = 0;

  g_assert (insert_offset < skp->length);
  g_assert (insert_length > 0);

 reenter:

  bottom->low    = insert_offset;
  bottom->high   = insert_offset + insert_length;
  bottom->data   = data;

  for (x = skp->head; x != bottom; x = x->down)
    {
      /* [while (v > x->key) x = x->r] */
      while (insert_offset >= x->high)
	x = x->right;

      /* [if ((x->d == bottom) && (v == x->key)) return 0] */
      while ((x->down == bottom) && (insert_offset >= x->low) && (insert_offset < x->high))
	{
	  /* insert_offset is inside another interval, check length. */
	  guint32 diff;

	  if ((insert_offset + insert_length) < x->high)
	    {
	      /* It is contained within the existing interval. */
	      return inserted;
	    }

	  /* Consume the overlapping region. */
	  diff = x->high - insert_offset;
	  insert_length -= diff;
	  insert_offset += diff;

	  /* @@@ Note: this makes xdfs_extract_from_chain work.  Clean this up.
	   */
	  data += diff;

	  if (insert_length == 0)
	    return inserted;

#if 0
	  /* Test if the interval can be extended */
	  if (insert_offset < x->right->low && x->data == data)
	    {
	      diff = MIN (x->right->low, insert_offset + insert_length) - insert_offset;

	      insert_length -= diff;
	      insert_offset += diff;
	      inserted      += diff;
	      x->high       += diff;

	      if (insert_length == 0)
		return inserted;
	    }
#endif
	  /* Here's the potentially bad, not-top-down re-entry */
	  goto reenter;
	}

      /* [if ((x->d == bottom) || (x->key == x->d->r->r->r->key)
       *    {
       *      t = (node*) malloc (sizeof (struct node));
       *      t->r = x->r; t->d = x->d->r->r; x->r = t;
       *      t->key = x->key; x->key = x->d->r->key;
       *    }] */
      if ((x->down == bottom) || (x->high == x->down->right->right->right->high))
	{
	  SkipListNode *t = g_chunk_new (SkipListNode, skp->chunk);
	  SkipListNode *xdr = x->down->right;

	  /* t is about to become what x was, and
	   * the inserted interval replaces x.
	   */

	  t->low   = x->low;
	  t->high  = x->high;
	  t->data  = x->data;
	  t->right = x->right;
	  t->down  = x->down->right->right;

	  x->low   = xdr->low;
	  x->high  = xdr->high;
	  x->data  = xdr->data;
	  x->right = t;

	  if (x->down == bottom)
	    {
	      /* This means a new leaf node is being inserted, but we
	       * have to check for overlap between x (new value) and
	       * x->right (old x). */
	      guint32 length;

	      if (x->right->low < x->high)
		x->high = x->right->low;

	      length = x->high - x->low;

	      data          += length;
	      insert_length -= length;
	      insert_offset += length;
	      inserted      += length;
	    }
	}
    }

  /* the first insertion */
  if (skp->head->right != skp->tail)
    {
      SkipListNode *t = g_chunk_new (SkipListNode, skp->chunk);

      t->down  = skp->head;
      t->right = skp->tail;
      t->low   = skp->length;
      t->high  = skp->length;

      skp->head = t;
    }

  if (insert_length > 0)
    goto reenter;

  return inserted;
}

static void
xdfs_skip_list_print_line (SkipListNode *n)
{
  for (; n != n->right; n = n->right)
    {
      g_print ("[%d-%d: %d] ", n->low, n->high, n->data);
    }

  g_print ("\n");
}

void
xdfs_skip_list_print (SkipList *skp)
{
  SkipListNode *n = skp->head;

  g_print ("skip list\n");

  for (; n != n->down; n = n->down)
    xdfs_skip_list_print_line (n);

  g_print ("\n");
}

SkipListData
xdfs_skip_list_data (SkipListNode *sln)
{
  return sln->data;
}

SkipListNode*
xdfs_skip_list_first (SkipList *skp)
{
  SkipListNode *x = skp->head->down;

  if (x->down == x)
    return NULL;

  while (x->down->down != x->down)
    x = x->down;

  return x;
}

SkipListNode*
xdfs_skip_list_next (SkipListNode *sln)
{
  if (sln->right->right == sln->right)
    return NULL;

  return sln->right;
}

guint
xdfs_skip_list_offset (SkipListNode *sln)
{
  return sln->low;
}

guint
xdfs_skip_list_length (SkipListNode *sln)
{
  return sln->high - sln->low;
}
