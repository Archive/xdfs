/* -*-Mode: C;-*-
 * PRCS - The Project Revision Control System
 * Copyright (C) 1997, 1998, 1999  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: skiplisttest.c 1.1 Mon, 07 Feb 2000 12:42:45 -0800 jmacd $
 */

#include "dbfs.h"

#define LENGTH 32

const char* identity (void* data) { return data; }

void
test1 ()
{
  SkipList *skp = xdfs_skip_list_new (LENGTH);

  g_assert (xdfs_skip_list_insert (skp, 0,  10, "0-10")    == 10);
  g_assert (xdfs_skip_list_insert (skp, 0,  5,  "(error)") == 0);
  g_assert (xdfs_skip_list_insert (skp, 3,  5,  "(error)") == 0);
  g_assert (xdfs_skip_list_insert (skp, 0,  15, "10-15")   == 5);
  g_assert (xdfs_skip_list_insert (skp, 15, 1,  "15-16")   == 1);
  g_assert (xdfs_skip_list_insert (skp, 31, 1,  "31-32")   == 1);
  g_assert (xdfs_skip_list_insert (skp, 30, 2,  "30-31")   == 1);
  g_assert (xdfs_skip_list_insert (skp, 24, 2,  "24-26")   == 2);
  g_assert (xdfs_skip_list_insert (skp, 22, 6,  "22-24,26-28") == 4);

  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search (skp, 0)), "0-10") == 0);
  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search (skp, 1)), "0-10") == 0);
  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search (skp, 3)), "0-10") == 0);
  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search (skp, 4)), "0-10") == 0);
  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search (skp, 5)), "0-10") == 0);
  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search (skp, 6)), "0-10") == 0);
  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search (skp, 7)), "0-10") == 0);
  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search (skp, 8)), "0-10") == 0);
  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search (skp, 9)),  "0-10") == 0);
  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search (skp, 10)), "10-15") == 0);
  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search (skp, 11)), "10-15") == 0);
  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search (skp, 12)), "10-15") == 0);
  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search (skp, 13)), "10-15") == 0);
  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search (skp, 14)), "10-15") == 0);
  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search (skp, 15)), "15-16") == 0);

  g_assert (! xdfs_skip_list_search (skp, 16));
  g_assert (! xdfs_skip_list_search (skp, 17));
  g_assert (! xdfs_skip_list_search (skp, 18));
  g_assert (! xdfs_skip_list_search (skp, 19));
  g_assert (! xdfs_skip_list_search (skp, 20));
  g_assert (! xdfs_skip_list_search (skp, 21));

  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search_nearest (skp, 17)), "22-24,26-28") == 0);

  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search (skp, 22)), "22-24,26-28") == 0);
  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search (skp, 23)), "22-24,26-28") == 0);
  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search (skp, 24)), "24-26") == 0);
  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search (skp, 25)), "24-26") == 0);
  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search (skp, 26)), "22-24,26-28") == 0);
  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search (skp, 26)), "22-24,26-28") == 0);

  g_assert (! xdfs_skip_list_search (skp, 28));
  g_assert (! xdfs_skip_list_search (skp, 29));

  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search (skp, 30)), "30-31") == 0);
  g_assert (strcmp (xdfs_skip_list_data (xdfs_skip_list_search (skp, 31)), "31-32") == 0);
}

void
test2 ()
{
  SkipList *skp = xdfs_skip_list_new (100);

  g_assert (xdfs_skip_list_insert (skp, 9,   1, "9") == 1);
  g_assert (xdfs_skip_list_insert (skp, 13,   1, "13") == 1);
  g_assert (xdfs_skip_list_insert (skp, 30,   1, "30") == 1);
  g_assert (xdfs_skip_list_insert (skp, 39,   1, "39") == 1);
  g_assert (xdfs_skip_list_insert (skp, 41,   1, "41") == 1);
  g_assert (xdfs_skip_list_insert (skp, 48,   1, "48") == 1);
  g_assert (xdfs_skip_list_insert (skp, 51,   1, "51") == 1);
  g_assert (xdfs_skip_list_insert (skp, 53,   1, "53") == 1);
  g_assert (xdfs_skip_list_insert (skp, 60,   1, "60") == 1);
}

int main()
{
  test2 ();
  test1 ();

  return 0;
}
