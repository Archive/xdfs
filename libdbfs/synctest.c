/* -*-Mode: C;-*-
 * $Id: synctest.c 1.8 Mon, 28 Feb 2000 10:07:45 -0800 jmacd $
 * file.c:
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "dbfs.h"
#include <xdelta.h>
#include <edsiostdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <dirent.h>

#define SYNC_MIN_VERSIONS 0

typedef struct _SyncSet SyncSet;

struct _SyncSet {
  DBFS        *dbfs;
  RepoTxn     *txn;
  Inode        last_shared;
  gboolean     share;
};

SyncSet ss1;
SyncSet ss2;

Path        *xdfs_loc;

char        *tmp_file;

long long one_encoded = 0;
long long one_unencoded = 0;

long long total_encoded = 0;
long long total_unencoded = 0;

XdfsPolicy   sync_policy;

gboolean     stest_verify          = FALSE;
gint32       stest_min_versions    = 0;
gint32       stest_max_versions    = G_MAXINT;
const char*  stest_method          = NULL;
double       stest_prob_local_1;
double       stest_prob_shared;
const char*  stest_base_dir1       = NULL;
const char*  stest_base_dir2       = NULL;

FILE* result_out;

static ConfigOption options [] = {
  { "min_versions",      "min",    CS_Use,    CO_Optional, CD_Int32,   & stest_min_versions },
  { "max_versions",      "max",    CS_Use,    CO_Optional, CD_Int32,   & stest_max_versions },
  { "verify",            "vfy",    CS_Use,    CO_None,     CD_Bool,    & stest_verify },
  { "method",            "mth",    CS_Use,    CO_Required, CD_String,  & stest_method },
  { "stest_prob_shared", "ps",     CS_Use,    CO_Required, CD_Double,  & stest_prob_shared },
  { "stest_prob_local1", "pl1",    CS_Use,    CO_Required, CD_Double,  & stest_prob_local_1 },
  { "archive_base1",     "ab1",    CS_Ignore, CO_Required, CD_String,  & stest_base_dir1 },
  { "archive_base2",     "ab2",    CS_Ignore, CO_Required, CD_String,  & stest_base_dir2 },
};

gboolean
sync_create_xdfs (RepoTxn *txn, Path *loc)
{
  Inode ino;
  Inode root;
  XdfsParams params;

  if (! dbfs_inode_find_root (txn, path_root (txn->alloc), FALSE, FT_Directory, & root))
    return FALSE;

  if (! dbfs_inode_new (txn, & ino))
    return FALSE;

  if (! dbfs_make_directory (txn, & ino, TRUE))
    return FALSE;

  if (! dbfs_link_create (txn, & root, & ino, path_basename (loc), FALSE))
    return FALSE;

  memset (& params, 0, sizeof (params));

  params.policy = sync_policy;

  if (! xdfs_location_create (txn, loc, & params))
    return FALSE;

  return TRUE;
}

gboolean
sync_verify_insert (SyncSet *local, SyncSet *remote)
{
  FileHandle *orig, *retr;
  guint8 obuf[1024], rbuf[1024];
  int offset = 0;
  int oc, rc;
  int i;

  if (! (orig = dbfs_inode_open_read (local->txn, & local->last_shared)))
    return FALSE;

  if (! (retr = dbfs_inode_open_read (remote->txn, & remote->last_shared)))
    return FALSE;

  for (;;)
    {
      oc = handle_read (orig, obuf, 1024);
      rc = handle_read (retr, rbuf, 1024);

      if (oc < 0 || rc < 0)
	g_error ("read failed: verify\n");

      if (oc != rc)
	{
	  fprintf (stderr, "verify failed: different lengths: %d != %d\n", oc, rc);
	  return FALSE;
	}

      if (oc == 0)
	break;

      for (i = 0; i < oc; i += 1)
	{
	  if (obuf[i] != rbuf[i])
	    {
	      fprintf (stderr, "verify failed: different content at offset: %d\n", offset+i);
	      return FALSE;
	    }
	}

      offset += oc;
    }

  handle_close (orig);
  handle_close (retr);

  handle_free (orig);
  handle_free (retr);

  return TRUE;
}

gboolean
sync_insert (SyncSet *local, SyncSet *remote, const char* filename)
{
  FileHandle *fh;
  gboolean shared = (drand48 () < stest_prob_shared);
  Inode local_ino;

  if (! (fh = handle_read_file (filename)))
    return FALSE;

  if (! xdfs_insert_version (local->txn, xdfs_loc, fh, & local_ino))
    return FALSE;

  handle_free (fh);

  if (shared && local->share)
    {
      FileHandle *d_out;
      FileHandle *d_in;
      Inode remote_ino;

      if (! (d_out = handle_write_file (tmp_file)))
	return FALSE;

      if (! xdfs_extract_delta (local->txn, xdfs_loc, & local->last_shared, & local_ino, d_out))
	return FALSE;

      if (! handle_close (d_out))
	return FALSE;

      one_encoded   += handle_length (d_out);
      one_unencoded += local_ino.minor.ino_segment_len;

      handle_free (d_out);

      if (! (d_in = handle_read_file (tmp_file)))
	return FALSE;

      if (! xdfs_insert_delta (remote->txn, xdfs_loc, & remote->last_shared, d_in, & remote_ino))
	return FALSE;

      if (! handle_close (d_in))
	return FALSE;

      handle_free (d_in);

      local->last_shared = local_ino;
      remote->last_shared = remote_ino;

      if (stest_verify && ! sync_verify_insert (local, remote))
	return FALSE;
    }
  else if (shared)
    {
      local->last_shared = local_ino;

      if (! (fh = handle_read_file (filename)))
	return FALSE;

      if (! xdfs_insert_version (remote->txn, xdfs_loc, fh, & local_ino))
	return FALSE;

      handle_free (fh);

      one_encoded   += local_ino.minor.ino_segment_len;
      one_unencoded += local_ino.minor.ino_segment_len;

      remote->last_shared = local_ino;

      local->share = TRUE;
      remote->share = TRUE;
    }

  return TRUE;
}

gboolean
sync_dateorder (RcsFile* rcs, RcsVersion *v, void* data)
{
  static int progress = 0;

  if ((++progress % 1000) == 0)
    fprintf (stderr, "i %d\n", progress);

  if (! ss1.txn)
    {
      if (! (ss1.txn = dbfs_txn_begin (ss1.dbfs, DBFS_TXN_FLAT)))
 	return FALSE;

      if (! (ss2.txn = dbfs_txn_begin (ss2.dbfs, DBFS_TXN_FLAT)))
 	return FALSE;
    }

  if (! xdfs_loc)
    {
      xdfs_loc = path_append_bytes (ss1.txn->alloc, path_root (ss1.txn->alloc),
				    PP_String, rcs->filename, strlen (rcs->filename));

      if (! sync_create_xdfs (ss1.txn, xdfs_loc))
	return FALSE;

      if (! sync_create_xdfs (ss2.txn, xdfs_loc))
	return FALSE;
    }

  if (drand48 () < stest_prob_local_1)
    return sync_insert (& ss1, & ss2, v->filename);
  else
    return sync_insert (& ss2, & ss1, v->filename);
}

gboolean
sync_onefile (RcsFile *rcs, RcsStats* set, void* data)
{
  if (ss1.txn)
    {
      if (! dbfs_txn_commit (ss1.txn))
	return FALSE;

      ss1.share = FALSE;
      ss1.txn = NULL;

      if (! dbfs_txn_commit (ss2.txn))
	return FALSE;

      ss2.txn = NULL;
      ss2.share = FALSE;

      fprintf (result_out, "%s %qd %qd %0.2f%%\n",
	       rcs->filename,
	       one_encoded,
	       one_unencoded,
	       100.0 * (1.0 - ((double) one_encoded / (double) one_unencoded)));

      total_unencoded += one_unencoded;
      total_encoded   += one_encoded;

      one_unencoded = 0;
      one_encoded = 0;
    }

  xdfs_loc = NULL;

  return TRUE;
}

DBFS*
sync_init (const char* dir)
{
  Path *archive_path;

  if (! (archive_path = path_from_host_string (NULL, _fs_pthn, dir)))
    return NULL;

  return dbfs_create (archive_path, NULL);
}

RcsWalker sync_walker = {
  NULL,
  NULL,
  & sync_onefile,
  & sync_dateorder,
  NULL,
  NULL,
  SYNC_MIN_VERSIONS,
  G_MAXINT,
  TRUE
};

int
main (int argc, char** argv)
{
  int   i;

  rcswalk_init ();

  tmp_file = g_strdup_printf ("%s/st.%d", g_get_tmp_dir (), (int) getpid ());

  config_register (options, ARRAY_SIZE (options));

  config_set_string ("rcswalk_experiment", "st");

  if (argc < 2)
    {
      config_help ();
      goto bail;
    }

  for (i = 1; i < argc; i += 1)
    {
      if (! config_parse (argv[i]))
	goto bail;
    }

  if (! config_done ())
    goto bail;

  if (strcmp (stest_method, "XDFS-rj") == 0)
    sync_policy = XP_ReverseJump;
  else if (strcmp (stest_method, "XDFS-fj") == 0)
    sync_policy = XP_ForwardJump;
  else if (strcmp (stest_method, "XDFS-f") == 0)
    sync_policy = XP_Forward;
  else
    g_error ("unknown TEST: %s\n", stest_method);

  g_assert (stest_prob_local_1 >= 0.0 && stest_prob_local_1 <= 1.0);
  g_assert (stest_prob_shared >= 0.0 && stest_prob_shared <= 1.0);

  sync_walker.min_versions = stest_min_versions;
  sync_walker.max_versions = stest_max_versions;

  fprintf (stderr, "synctest: verification %s\n", stest_verify ? "on" : "off");

  if (! config_clear_dir (stest_base_dir1))
    goto bail;

  if (! config_clear_dir (stest_base_dir2))
    goto bail;

  if (! (result_out = config_output ("Result")))
    goto bail;

  fprintf (result_out, "File Encoded Unencoded Compression\n");

  if (! dbfs_library_init ())
    goto bail;

  if (! xdfs_library_init ())
    goto bail;

  if (! (ss1.dbfs = sync_init (stest_base_dir1)))
    goto bail;

  if (! (ss2.dbfs = sync_init (stest_base_dir2)))
    goto bail;

  if (! rcswalk (& sync_walker, NULL))
    goto bail;

  if (fclose (result_out))
    g_error ("fclose failed");

  if (! (result_out = config_output ("TotalResult")))
    goto bail;

  fprintf (result_out, "----------------\n");
  fprintf (result_out, "Name:        %s\n",  stest_method);
  fprintf (result_out, "Encoded:     %qd\n", total_encoded);
  fprintf (result_out, "Unencoded:   %qd\n", total_unencoded);
  fprintf (result_out, "Compression: %0.2f%%\n", 100.0 * (1.0 - ((double) total_encoded / (double) total_unencoded)));

  if (fclose (result_out))
    g_error ("fclose failed");

  unlink (tmp_file);

  return 0;

 bail:

  if (ss1.txn) dbfs_txn_abort (ss1.txn);
  if (ss2.txn) dbfs_txn_abort (ss2.txn);

  if (ss1.dbfs) dbfs_close (ss1.dbfs);
  if (ss2.dbfs) dbfs_close (ss2.dbfs);

  dbfs_library_close ();

  unlink (tmp_file);

  return 2;
}
