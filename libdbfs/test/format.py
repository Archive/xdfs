# My first Python program!

import string

ofname = "tables/st.total"
stats = [ "Encoded", "Unencoded", "Compression" ]
rname = "TotalResult"
dirs = [ ("ex=st,id=freebsd,mth=XDFS-fj,ps=1.00,pl1=1.00", "XDFS-fj"),
         ("ex=st,id=freebsd,mth=XDFS-rj,ps=1.00,pl1=1.00", "XDFS-rj"),
         ("ex=st,id=http,mth=XDFS-f,ps=1.00,pl1=1.00",     "XDFS-f"),
         ("ex=st,id=http,mth=XDFS-fj,ps=1.00,pl1=1.00",    "XDFS-fj"),
         ("ex=st,id=http,mth=XDFS-rj,ps=1.00,pl1=1.00",   "XDFS-rj") ]

#  ofname = "tables/at.full"
#  dirs = [ ("ex=at,id=freebsd,mth=RCS-tree",           "RCS-t"),
#           ("ex=at,id=freebsd,mth=RCS-linear",         "RCS-l"),
#           ("ex=at,id=freebsd,mth=FS-none",            "FS-none"),
#           ("ex=at,id=freebsd,mth=XDFS-none",          "XDFS-none"),
#           ("ex=at,id=freebsd,mth=XDFS-fj",            "XDFS-f"),
#           ("ex=at,id=freebsd,mth=XDFS-rj",            "XDFS-r") ];

#  ofname = "tables/at.large"
#  dirs = [ ("ex=at,id=freebsd,min=10,mth=RCS-tree",       "RCS-t"),
#           ("ex=at,id=freebsd,min=10,mth=RCS-linear",     "RCS-l"),
#           ("ex=at,id=freebsd,min=10,mth=FS-none",        "FS-none"),
#           ("ex=at,id=freebsd,min=10,mth=XDFS-none",      "XDFS-none"),
#           ("ex=at,id=freebsd,min=10,mth=XDFS-fj",        "XDFS-f"),
#           ("ex=at,id=freebsd,min=10,mth=XDFS-rj,cmv=40", "XDFS-r") ];

#  ofname = "tables/at.cmv"
#  dirs = [ ("ex=at,id=freebsd,min=10,mth=XDFS-rj,cmv=5",  "5"),
#           ("ex=at,id=freebsd,min=10,mth=XDFS-rj,cmv=10", "10"),
#           ("ex=at,id=freebsd,min=10,mth=XDFS-rj,cmv=20", "20"),
#           ("ex=at,id=freebsd,min=10,mth=XDFS-rj,cmv=40", "40") ]

stats = [ "Insert time", "Retrieve time", "Storage", "Actual compression", "Ideal", "Ideal compression", "Storage overhead" ];
rname = "Result.data"

# End config

statstab = { };
width = 12

for elt in stats:
    statstab[elt] = elt

# Open the output
of = open (ofname, "w")

# Scan each file
for dirtup in dirs:
    # Open the file
    ifn = "output/" + dirtup[0] + "/" + rname;
    f = open (ifn);

    row = { }

    # Read it line by line
    line = f.readline();
    while line:

        fields = string.split (line, ":")

        key = fields[0]

        if statstab.has_key (key):
            row[key] = string.split (string.strip (fields[1])) [0]

        # Read another line
        line = f.readline();

    of.write (string.ljust (dirtup[1], width))

    for d in stats:
        val = string.replace (row[d], "%", "\\%")
        of.write (" & " + string.ljust (val, width))

    of.write ("\\\\ \\hline\n")

# Close the output
of.close()


# DEAD

#  ofname = "tables/at.sbmf"
#  dirs = [ ("ex=at,id=freebsd,min=10,mth=XDFS-rj,sbmf=0",              "0 & 0"),
#           ("ex=at,id=freebsd,min=10,mth=XDFS-rj,sbmf=4,sbmspf=256",   "4 & 256"),
#           ("ex=at,id=freebsd,min=10,mth=XDFS-rj,sbmf=4,sbmspf=512",   "4 & 512"),
#           ("ex=at,id=freebsd,min=10,mth=XDFS-rj,sbmf=4,sbmspf=1024",  "4 & 1024"),
#           ("ex=at,id=freebsd,min=10,mth=XDFS-rj,sbmf=4,sbmspf=2048",  "4 & 2048"),
#           ("ex=at,id=freebsd,min=10,mth=XDFS-rj,sbmf=4,sbmspf=4096",  "4 & 4096"),
#           ("ex=at,id=freebsd,min=10,mth=XDFS-rj,sbmf=8,sbmspf=256",   "8 & 256"),
#           ("ex=at,id=freebsd,min=10,mth=XDFS-rj,sbmf=8,sbmspf=512",   "8 & 512"),
#           ("ex=at,id=freebsd,min=10,mth=XDFS-rj,sbmf=8,sbmspf=1024",  "8 & 1024"),
#           ("ex=at,id=freebsd,min=10,mth=XDFS-rj,sbmf=8,sbmspf=2048",  "8 & 2048"),
#           ("ex=at,id=freebsd,min=10,mth=XDFS-rj,sbmf=8,sbmspf=4096",  "8 & 4096"),
#           ("ex=at,id=freebsd,min=10,mth=XDFS-rj,sbmf=14,sbmspf=256",  "14 & 256"),
#           ("ex=at,id=freebsd,min=10,mth=XDFS-rj,sbmf=14,sbmspf=512",  "14 & 512"),
#           ("ex=at,id=freebsd,min=10,mth=XDFS-rj,sbmf=14,sbmspf=1024", "14 & 1024"),
#           ("ex=at,id=freebsd,min=10,mth=XDFS-rj,sbmf=14,sbmspf=2048", "14 & 2048"),
#           ("ex=at,id=freebsd,min=10,mth=XDFS-rj,sbmf=14,sbmspf=4096", "14 & 4096") ]
