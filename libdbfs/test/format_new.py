import math
import os
import string

# @@@ Why is there a variance in RCS-linear's ideal storage???
# @@@ Why is there a large, seemingly systematic time variance for FreeBSD-full???

methods = [
    ("ex=at,id=freebsd,mth=FS-none",                    "FS-none"),
    ("ex=at,id=freebsd,mth=RCS-tree",                   "RCS-t"),
    ("ex=at,id=freebsd,mth=RCS-linear",                 "RCS-l"),
    ("ex=at,id=freebsd,mth=XDFS-none",                  "XDFS-none"),
    ("ex=at,id=freebsd,mth=XDFS-fj",                    "XDFS-f"),
    ("ex=at,id=freebsd,mth=XDFS-rj",                    "XDFS-r (cmv=40)"),
    ("ex=at,id=freebsd,mth=XDFS-rj,sbmf=14,sbmspf=256", "XDFS-r (srcbuf)")
    ];
base       = "ex=at,id=freebsd,mth=FS-none"

# Data set config

#  methods = [
#      ("ex=at,id=freebsd,min=10,mth=FS-none",                     "FS-none"),
#      ("ex=at,id=freebsd,min=10,mth=RCS-tree",                    "RCS-t"),
#      ("ex=at,id=freebsd,min=10,mth=RCS-linear",                  "RCS-l"),
#      ("ex=at,id=freebsd,min=10,mth=XDFS-none",                   "XDFS-none"),
#      ("ex=at,id=freebsd,min=10,mth=XDFS-fj",                     "XDFS-f"),
#      ("ex=at,id=freebsd,min=10,mth=XDFS-rj,cmv=40",              "XDFS-r (cmv=40)"),
#      ("ex=at,id=freebsd,min=10,mth=XDFS-rj,cmv=20",              "XDFS-r (cmv=20)"),
#      ("ex=at,id=freebsd,min=10,mth=XDFS-rj,cmv=10",              "XDFS-r (cmv=10)"),
#      ("ex=at,id=freebsd,min=10,mth=XDFS-rj,cmv=5",               "XDFS-r (cmv=5)"),
#      ("ex=at,id=freebsd,min=10,mth=XDFS-rj,sbmf=14,sbmspf=256",  "XDFS-r (srcbuf)")
#       ];
#  base       = "ex=at,id=freebsd,min=10,mth=FS-none"

# Table config

output     = "Result.data"
rsets      = [ "output/", "output2/" ]
base_stats = [ "Insert time", "Retrieve time", "Unencoded", "Storage", "Ideal" ]

# End config

def sum2(x,y):
    return x+y

def diff2(x,y):
    return x-y

def square(x):
    return x*x

# Make a table of statistic names
base_stats_table = { };

for elt in base_stats:
    base_stats_table[elt] = elt

# A class to represent each method's data
meth_by_lname = { }
meth_by_pname = { }

class Method:
    def __init__(self,lname,pname):
        self.lname = lname
        self.pname = pname
        self.stable = { }
        meth_by_lname[lname] = self
        meth_by_pname[pname] = self

    def read_data(self,filename):
        file = open (filename)

        line = file.readline()

        while line:
            fields = string.split(line,":")
            key = fields[0]

            if (base_stats_table.has_key (key)):

                value = string.atof (string.split (string.strip (fields[1])) [0])

                if (self.stable.has_key (key)):
                    self.stable[key] = self.stable[key] + [ value ]
                else:
                    self.stable[key] = [ value ]

            line = file.readline ()

        file.close()


def avg(meth,name):
    l = meth.stable[name]
    return reduce(sum2,l) / len(l)

def mklist(elt,count):
    l = []
    while count > 0:
        l = l + [elt]
        count = count - 1
    return l

def stddev(meth,name):
    l = meth.stable[name]
    c = len(l)
    a = reduce(sum2,l) / c
    as = mklist (a,c)
    diffs = map (diff2, l, as)
    squares = map (square, diffs)
    sum = reduce(sum2, squares)
    return math.sqrt (sum / c)

# Instantiate all methods
for elt in methods:
    Method(elt[0],elt[1])

base_meth = meth_by_lname[base]

# Formatting

class Format:
    def __init__(self,title,fmt_func):
        self.title = title
        self.fmt_func = fmt_func

def fmttm(x,s):
    return '%d' % x + " sec. $\\pm$ " + '%0.1f' % (100.0*s/x) + "\\%"

def fmtmb(x):
    x = x / 1000000.0

    if x >= 100.0:
        fmt = '%d'
    elif x >= 10.0:
        fmt = '%0.1f'
    else:
        fmt = '%0.2f'

    return fmt % x + " MB"

def fmtfact(n,x,y):
    r = x/y
    return (('%%0.%df' % n) % r)

fmt_stats = [ Format("Ideal storage",      lambda meth: fmtmb(avg(meth,"Ideal"))),
              Format("Ideal comp.",        lambda meth: fmtfact(3,avg(meth,"Ideal"), avg (meth, "Unencoded"))),
              Format("Actual storage",     lambda meth: fmtmb(avg(meth,"Storage"))),
              Format("Overhead factor",    lambda meth: fmtfact(3,avg(meth,"Storage"),avg(meth,"Ideal"))),
              Format("Actual comp.",       lambda meth: fmtfact(3,avg(meth,"Storage"),avg (meth, "Unencoded"))),
              ]

#  fmt_stats = [ Format("Insertion time",   lambda meth: fmttm(avg(meth,"Insert time"), stddev(meth,"Insert time"))),
#                Format("Insertion factor", lambda meth: fmtfact(2,avg(meth,"Insert time"), avg(base_meth,"Insert time"))),
#                Format("Retrieval time",   lambda meth: fmttm(avg(meth,"Retrieve time"), stddev(meth,"Retrieve time"))),
#                Format("Retrieval factor", lambda meth: fmtfact(2,avg(meth,"Retrieve time"), avg(base_meth,"Retrieve time"))),
#                ]

# The cell width
width = 18

# Read data for each result set...
for rset in rsets:

    # For each method...
    for meth in meth_by_lname.values():

        rname = rset + meth.lname + "/" + output

        meth.read_data (rname)

# Output raw

if 1:
    str = string.ljust ("Method", width)
    for stat in fmt_stats:
        str = str + "& " + string.ljust (stat.title, width)
    str = str + "\\\\ \\hline"
    print str
    for mn in methods:
        meth = meth_by_lname[mn[0]]
        str = string.ljust (meth.pname, width)
        for stat in fmt_stats:
            str = str + "& " + string.ljust (stat.fmt_func(meth), width)
        print str + "\\\\ \\hline"

# Output plot

if 0:
    tmpname = "/tmp/rat.tmp"
    tmpf = open (tmpname, "w")

    #tmpf.write ("set grid\n")
    tmpf.write ("set size square\n")
    tmpf.write ("set key right\n")
    tmpf.write ("set pointsize 1.5\n")
    tmpf.write ("set xlabel \"Actual compression\"\n")
    tmpf.write ("set ylabel \"Relative Insertion Time\"\n")
    #tmpf.write ("set format x \"%.0s%cB\"\n")
    #tmpf.write ("set xtics 0, 1000000000, 4000000000\n")
    #tmpf.write ("set xtics (\"0\" 0, \"500 MB\" 500000000, \"1 GB\" 1000000000, \"1.5 GB\" 1500000000, \"2 GB\" 2000000000, \"2.5 GB\" 2500000000, \"3 GB\" 3000000000)\n")


    if 1:
        tmpf.write ("set terminal postscript eps 22\n")
        tmpf.write ("set term post portrait color \"Times-Roman\" 20\n")
        tmpf.write ("set output 'plots/TimeSpace.eps'\n")

    pstr = "plot [0:1.5] [0:7] "

    setno = 0

    for mn in methods:
        meth = meth_by_lname[mn[0]]

        dname = "/tmp/dat.tmp" + meth.pname
        df = open (dname, "w")

        pstr = pstr + "'" + dname + "' using 1:2 title \"" + meth.pname + "\""

        setno = setno + 1

        if (setno < len(methods)):
            pstr = pstr + ", "

        storage = avg(meth,"Storage")
        ideal_storage = avg(meth,"Unencoded")
        insert = avg(meth,"Insert time")
        ideal_insert = avg(base_meth,"Insert time")

        df.write ('%e' % (storage/ideal_storage) + ' %e' % (insert/ideal_insert) + "\n")
        df.close ()

    tmpf.write (pstr + "\n");

    tmpf.close ()

    os.system ("gnuplot -persist " + tmpname)
