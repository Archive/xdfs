import string
import os

tmpnam = "/tmp/id.tmp"

inputs = [ ("ex=at,id=freebsd,min=10,mth=RCS-tree",       "RCS-tree"),
           ("ex=at,id=freebsd,min=10,mth=RCS-linear",     "RCS-linear"),
           ("ex=at,id=freebsd,min=10,mth=FS-none",        "FS-none"),
           ("ex=at,id=freebsd,min=10,mth=XDFS-none",      "XDFS-none"),
           ("ex=at,id=freebsd,min=10,mth=XDFS-fj",        "XDFS-fj"),
           ("ex=at,id=freebsd,min=10,mth=XDFS-rj,sbmf=0", "XDFS-rj")
           ]

names = [ "InsertTime", "RetrieveTime", "InsertSize", "RetrieveSize", "InsertTimeSizeRatio", "RetrieveTimeSizeRatio"  ]
prefix = "Full"

for name in names:

    tmpf = open (tmpnam, "w")

    tmpf.write ("set grid\n")
    tmpf.write ("set key left\n")
    tmpf.write ("set terminal postscript eps 22\n")
    tmpf.write ("set term post portrait color \"Times-Roman\" 20\n")
    tmpf.write ("set linestyle 1 pt 1\n")
    tmpf.write ("set linestyle 2 pt 2\n")
    tmpf.write ("set linestyle 3 pt 3\n")
    tmpf.write ("set linestyle 4 pt 4\n")
    tmpf.write ("set linestyle 5 pt 8\n")
    tmpf.write ("set linestyle 6 pt 6\n")
    tmpf.write ("set output 'plots/" + prefix + "." + name + ".eps'\n")

    tmpf.write ("plot [0:400] ") #  [0:5e-8]

    i = 0

    for inp in inputs:
        i = i + 1
        str = "'output/" + inp[0] + "/" + name + ".avg' using 1 title \"" + inp[1] + "\" ls " + '%d' % i + ",";
        tmpf.write (str)

    tmpf.write ("0 notitle\n")

    tmpf.close ()

    os.system ("gnuplot -persist " + tmpnam)
