#!/usr/local/bin/gnuplot -persist

set terminal postscript eps 22
set term post portrait color "Times-Roman" 20
set output 'plots/Compression0.1.eps'

set xlabel "Versions in Cluster"
set ylabel "Compression"

set multiplot

set function style lines
plot [1:10] [0:1] (1/x)*(1+0.1*(x-1)) title "XDFS-r"

set function style points
set pointsize 0.2
plot [1:10] [0:1] (1/(2*x))*(0.1*(x*x) +0.1*x - 2*0.1 + 2) title "XDFS-f"
