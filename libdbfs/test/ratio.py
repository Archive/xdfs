import string
import os

#  inputs = [
#             ("ex=at,id=freebsd,min=10,mth=RCS-tree",       "RCS-t"),
#             ("ex=at,id=freebsd,min=10,mth=RCS-linear",     "RCS-l"),
#             ("ex=at,id=freebsd,min=10,mth=XDFS-none",      "XDFS-none"),
#             ("ex=at,id=freebsd,min=10,mth=XDFS-fj",        "XDFS-f"),
#             ("ex=at,id=freebsd,min=10,mth=XDFS-rj,cmv=40", "XDFS-r")
#             ]
#  normal = "output/ex=at,id=freebsd,min=10,mth=FS-none"
#  output = "plots/InsTimeRatio.large.eps"

inputs = [
           ("ex=at,id=freebsd,mth=RCS-tree",       "RCS-t"),
           ("ex=at,id=freebsd,mth=RCS-linear",     "RCS-l"),
           ("ex=at,id=freebsd,mth=XDFS-none",      "XDFS-none"),
           ("ex=at,id=freebsd,mth=XDFS-fj",        "XDFS-f"),
           ("ex=at,id=freebsd,mth=XDFS-rj",        "XDFS-r")
           ]
normal = "output/ex=at,id=freebsd,mth=FS-none"
output = "plots/InsTimeRatio.full.eps"

basename = "InsertTime.avg"

#

tmpname = "/tmp/rat.tmp"
tmpf = open (tmpname, "w")

tmpf.write ("set grid\n")
tmpf.write ("set key left\n")

if 1:
    tmpf.write ("set terminal postscript eps 22\n")
    tmpf.write ("set term post portrait color \"Times-Roman\" 20\n")
    tmpf.write ("set output '" + output + "'\n")

tmpf.write ("set linestyle 1 pt 1\n")
tmpf.write ("set linestyle 2 pt 2\n")
tmpf.write ("set linestyle 3 pt 3\n")
tmpf.write ("set linestyle 4 pt 4\n")
tmpf.write ("set linestyle 5 pt 8\n")
tmpf.write ("set linestyle 6 pt 6\n")

tmpf.write ("set xlabel \"Insertion Number\"\n")
tmpf.write ("set ylabel \"Normalized Average Insertion Time\"\n")

tmpf.write ("plot [0:400] ");

i = 0

for inp in inputs:

    dname = tmpname + inp[1];
    dfile = open (dname, "w");
    iname = "output/" + inp[0] + "/" + basename;
    ifile = open (iname, "r")
    bfile = open (normal + "/" + basename, "r")

    ients = ifile.readlines()
    bents = bfile.readlines()

    for x,y in map(None, ients, bents):

        print y
        print x

        iv = float(string.split(x)[0])
        bv = float(string.split(y)[0])
        r = iv / bv

        dfile.write ("%e\n" % r)

    dfile.close ()

    i = i + 1
    str = "'" + dname + "' using 1 title \"" + inp[1] + "\" ls " + '%d' % i;

    if i < len(inputs):
         str = str + ", "
    tmpf.write (str)

tmpf.close ()

os.system ("gnuplot -persist " + tmpname)
