import string
import os
import math

inputs = [
           ("ex=at,id=freebsd,min=10,mth=RCS-tree",       "RCS-t"),
           ("ex=at,id=freebsd,min=10,mth=RCS-linear",     "RCS-l"),
           ("ex=at,id=freebsd,min=10,mth=XDFS-none",      "XDFS-none"),
           ("ex=at,id=freebsd,min=10,mth=XDFS-fj",        "XDFS-f"),
           ("ex=at,id=freebsd,min=10,mth=XDFS-rj,cmv=40", "XDFS-r")
           ]
base   = "output2"
normal = "ex=at,id=freebsd,min=10,mth=FS-none"
output = "plots/InsTimeRatio.large.eps"

basename = "InsertTime.raw"

#

tmpname = "/tmp/rat.tmp"
tmpf = open (tmpname, "w")

tmpf.write ("set grid\n")
tmpf.write ("set key left\n")

if 1:
    tmpf.write ("set terminal postscript eps 22\n")
    tmpf.write ("set term post portrait color \"Times-Roman\" 20\n")
    tmpf.write ("set output '" + output + "'\n")

tmpf.write ("set linestyle 1 pt 1\n")
tmpf.write ("set linestyle 2 pt 2\n")
tmpf.write ("set linestyle 3 pt 3\n")
tmpf.write ("set linestyle 4 pt 4\n")
tmpf.write ("set linestyle 5 pt 8\n")
tmpf.write ("set linestyle 6 pt 6\n")

tmpf.write ("set xlabel \"Insertion Number\"\n")
tmpf.write ("set ylabel \"Average Normalized Insertion Time\"\n")

tmpf.write ("plot [0:400]");

lsno = 0

def div2(x,y):
    return x/y

def sum2(x,y):
    return x+y

def square(x):
    return x*x

for inp in inputs:

    dname = tmpname + inp[1];
    dfile = open (dname, "w");

    iname = base + "/" + inp[0] + "/" + basename;
    bname = base + "/" + normal + "/" + basename;

    ifile = open (iname, "r")
    bfile = open (bname, "r")

    ients = ifile.readlines()
    bents = bfile.readlines()

    row = 1

    headstr = ""

    for irow,brow in map(None, ients, bents):

        ivals = string.split(irow)
        bvals = string.split(brow)

        nvals = map(div2, map(string.atof,ivals), map(string.atof,bvals))

        trials = len(nvals)
        sum    = reduce(sum2, nvals)
        avg    = sum / trials

        variance = reduce(sum2, map(square, map(lambda x: x-avg, nvals))) / trials

        if row < 4:
            headstr = headstr + '%f' % avg + " "

        dfile.write ("%e " % row)
        dfile.write ("%e " % avg)
        dfile.write ("%e\n" % math.sqrt(variance))

        row = row + 1

    print inp[1] + " ratios are " + headstr

    dfile.close ()

    lsno = lsno + 1
    str = "'" + dname + "' using 1:2 title \"" + inp[1] + "\" ls " + '%d' % lsno;

    if lsno < len(inputs):
         str = str + ", "
    tmpf.write (str)

tmpf.close ()

os.system ("gnuplot -persist " + tmpname)
