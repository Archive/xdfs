#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "config.h"

#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif

#include "maketime.h"

int main (int argc, char** argv)
{
  char buf[1024];
  int i;

  memset (buf, 0, 1024);

  for (i = 1; i < argc; i += 1)
    {
      strcat (buf, argv[i]);
      strcat (buf, " ");
    }

  printf ("%d\n", (int) str2time(buf, 0, 0));
  exit (0);
}
