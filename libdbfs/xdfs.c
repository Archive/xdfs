/* -*-Mode: C;-*-
 * $Id: xdfs.c 1.21 Wed, 03 May 2000 10:36:27 -0700 jmacd $
 *
 * Copyright (C) 1999, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "dbfs.h"
#include "xdelta.h"
#include "edsiostdio.h"

/*#define DEBUG_XDFS*/

#define XDFS_INSERT_CURRENT

static Path* xidx_path;
static Path* xstate_path;
static Path* xseq_path;
static Path* xcur_path;
static Path* xsrcdir_path;

static ViewDef                   *xdfs_reconstruct_view;
static const guint64              xdfs_reconstruct_view_id = 67;
static const guint16              xdfs_stack_id = 21;

typedef struct _XdfsLocation            XdfsLocation;
typedef struct _XdfsSourceStats         XdfsSourceStats;
typedef struct _SerialXdfsInstruction   XdfsInstruction;
typedef struct _XdfsLocalIndex          XdfsLocalIndex;
typedef struct _XdfsExtraction          XdfsExtraction;
typedef struct _XdfsExtrHalf            XdfsExtrHalf;
typedef struct _XdfsExtrElt             XdfsExtrElt;

struct _XdfsExtrElt {
  Inode          *this_inop;
  XdeltaControl  *control;
  XdfsLocalIndex *src_lis;
  Inode          *src_inos;
  int             primary_index;
  Inode          *primary_inop;
};

struct _XdfsExtrHalf {
  int            chain_len;
  GSList        *chain;
  Inode         *loc_inop;
  Inode         *orig_inop;
};

struct _XdfsExtraction {
  XdfsLocation *loc;

  XdfsExtrHalf from;
  XdfsExtrHalf to;
};

struct _XdfsLocalIndex {
  const XdfsInstruction *inst;
  guint                  inst_len;
  guint                  length;
  Inode                 *inop;
  GSList                *free_list;
  GArray                *inst_array;
  XdfsInstruction        if_seg;
  LingerHandle          *linger;
  gboolean               literal;
};

struct _XdfsLocation {
  Inode  loc_ino;
  Inode  seq_ino;
  Inode  idx_ino;
  Inode  srcdir_ino;

  const MessageDigest *md;
  gboolean has_src_buffer;

  XdfsState *state;
};

static gboolean xdfs_insert_version_loc (RepoTxn      *txn,
					 XdfsLocation *loc,
					 FileHandle   *in,
					 Inode        *inop_out);

static gboolean xdfs_insert_new_version  (RepoTxn         *txn,
				        XdfsLocation    *loc,
				        Inode           *copy_ino);
static gboolean xdfs_source_reverse_segments (RepoTxn         *txn,
					      XdfsLocation    *loc,
					      XdeltaGenerator *gen);
static gboolean xdfs_source_segment    (RepoTxn         *txn,
					XdfsLocation    *loc,
				        XdeltaGenerator *gen,
					XdeltaSourceType type,
				        Inode           *from);
static gboolean xdfs_source_evolve     (RepoTxn         *txn,
					XdfsLocation    *loc,
					Inode           *patch_ino);
static gboolean xdfs_reject_delta      (RepoTxn         *txn,
				        XdfsLocation    *loc,
					Inode           *copy_ino,
				        Inode           *to_ino,
				        Inode           *patch_ino,
					Inode           *cont_ino);
static gboolean xdfs_control_read      (RepoTxn         *txn,
					Inode           *inode,
					XdeltaControl  **control_out);
static gboolean xdfs_control_write     (RepoTxn         *txn,
					Inode           *inode,
					XdeltaControl   *control,
					Inode           *cino_out);
static gboolean xdfs_location_init     (RepoTxn         *txn,
					Path            *xdfs,
					XdfsLocation    *loc);

static ViewPgin  xdfs_view_page_in;
static ViewBegin xdfs_view_begin;
static ViewEnd   xdfs_view_end;

/**********************************************************************/
/*			   Maintenence Code                           */
/**********************************************************************/

gboolean
xdfs_library_init (void)
{
  if (! xd_edsio_init ())
    return FALSE;

  xidx_path    = path_append_string (NULL, path_current (NULL), PP_String, "xidx");
  xseq_path    = path_append_string (NULL, path_current (NULL), PP_String, "xseq");
  xstate_path  = path_append_string (NULL, path_current (NULL), PP_String, "xsta");
  xcur_path    = path_append_string (NULL, path_current (NULL), PP_String, "xcur");
  xsrcdir_path = path_append_string (NULL, path_current (NULL), PP_String, "xsrc");

  xdfs_reconstruct_view = dbfs_view_definition (xdfs_reconstruct_view_id,
						sizeof (XdfsLocalIndex),
						xdfs_view_begin,
						xdfs_view_page_in,
						xdfs_view_end);

  return TRUE;
}

gboolean
xdfs_container_sequence (RepoTxn    *txn,
			 Path       *xdfs,
			 Inode      *inop)
{
  XdfsLocation loc;

  if (! xdfs_location_init (txn, xdfs, & loc))
    return FALSE;

  (* inop) = loc.seq_ino;

  g_free (loc.state);

  return TRUE;
}

gboolean
xdfs_digest_index (RepoTxn    *txn,
		   Path       *xdfs,
		   Inode      *inop)
{
  XdfsLocation loc;

  if (! xdfs_location_init (txn, xdfs, & loc))
    return FALSE;

  g_assert (loc.md);

  (* inop) = loc.idx_ino;

  g_free (loc.state);

  return TRUE;
}

gboolean
xdfs_state_read (RepoTxn      *txn,
		 XdfsLocation *loc)
{
  SerialSource *src;
  FileHandle   *set_in;

  if (! (set_in = dbfs_inode_open_read (txn, & loc->loc_ino)))
    return FALSE;

  src = handle_source (set_in);

  if (! unserialize_xdfsstate (src, & loc->state))
    return FALSE;

  if (! handle_close (set_in))
    return FALSE;

  handle_free (set_in);
  src->source_free (src);

  return TRUE;
}

gboolean
xdfs_state_write (RepoTxn      *txn,
		  XdfsLocation *loc)
{
  SerialSink *sink;
  FileHandle *set_out;

  if (! (set_out = dbfs_inode_open_replace (txn, & loc->loc_ino)))
    return FALSE;

  sink = handle_sink (set_out, NULL, NULL, NULL, NULL);

  if (! serialize_xdfsstate_obj (sink, loc->state))
    return FALSE;

  if (! handle_close (set_out))
    return FALSE;

  handle_free (set_out);
  sink->sink_free (sink);

  return TRUE;
}

gboolean
xdfs_stat (RepoTxn    *txn,
	   Path       *xdfs,
	   XdfsState  *state)
{
  XdfsLocation loc;

  if (! xdfs_location_init (txn, xdfs, & loc))
    return FALSE;

  memcpy (state, loc.state, sizeof (*state));

  g_free (loc.state);

  return TRUE;
}

const char*
xdfs_policy_to_string (XdfsPolicy policy)
{
  switch (policy)
    {
    case XP_ReverseJump:
      return "Reverse Jump";
    case XP_ForwardJump:
      return "Forward Jump";
    case XP_Forward:
      return "Forward";
    case XP_NoCompress:
      return "Forward";
    }

  return "Error";
}

const char*
xdfs_flags_to_string (int flags)
{
  static char buf[128];

  buf[0] = 0;

  if (flags & XF_MD5Equality)
    strcat (buf, "MD5 ");

  if (flags & XF_SHAEquality)
    strcat (buf, "SHA ");

  if (flags & XF_StoreDigest)
    strcat (buf, "Digest ");

  return buf;
}

gboolean
xdfs_state_print (FileHandle *handle,
		  Path       *xdfs,
		  XdfsState  *state)
{
  GString *ps = g_string_new (NULL);

  guint32 encoded_size = state->literal_size + state->control_size + state->patch_size;

  path_to_host_string (_fs_pthn, xdfs, ps);

  if (! handle_printf (handle, "XDFS stats for file:         %s\n", ps->str) ||
      ! handle_printf (handle, "Flags:                       %s\n", xdfs_flags_to_string (state->flags)) ||
      ! handle_printf (handle, "Version count:               %d\n", state->total_version_count) ||
      ! handle_printf (handle, "Unique Version count:        %d\n", state->unique_version_count) ||
      ! handle_printf (handle, "Average file size:           %d\n",
		       state->unencoded_size / state->unique_version_count) ||
      ! handle_printf (handle, "Unencoded size:              %d\n", state->unencoded_size) ||
      ! handle_printf (handle, "Encoded size:                %d\n", encoded_size) ||
      ! handle_printf (handle, "Literal size:                %d\n", state->literal_size) ||
      ! handle_printf (handle, "Control size:                %d\n", state->control_size) ||
      ! handle_printf (handle, "Patch size:                  %d\n", state->patch_size) ||
      ! handle_printf (handle, "Ideal compression:           %0.2f%%\n",
		       100.0 * (1.0 - (double) encoded_size / (double) state->unencoded_size)) ||
      ! handle_printf (handle, "Policy:                      %s\n", xdfs_policy_to_string (state->policy)) ||
      ! handle_printf (handle, "Patch files:                 %d\n", state->patches) ||
      ! handle_printf (handle, "Literal files:               %d\n", state->literals) ||
      ! handle_printf (handle, "Current cluster versions:    %d\n", state->cluster_versions) ||
      ! handle_printf (handle, "Max cluster versions:        %d\n", state->cluster_max_versions) ||
      ! handle_printf (handle, "Source buffer copy length:   %d (%0.2f%%)\n",
		       state->src_buffer_copied,
		       100.0 * ((double) state->src_buffer_copied / (double) state->patch_size)) ||
      ! handle_printf (handle, "Source buffer files:         %d\n", state->src_buffer_files) ||
      ! handle_printf (handle, "Source buffer size:          %d\n", state->src_buffer_size) ||
      ! handle_printf (handle, "Source buffer max files:     %d\n", state->src_buffer_max_files) ||
      ! handle_printf (handle, "Source buffer max size:      %d\n", state->src_buffer_max_size) ||
      ! handle_printf (handle, "Source buffer max size/file: %d\n", state->src_buffer_max_size_per_file) ||
      ! handle_printf (handle, "Source buffer min size/file: %d\n\n", state->src_buffer_min_size_per_file))
    return FALSE;

  g_string_free (ps, TRUE);

  return TRUE;
}

gboolean
xdfs_state_init (XdfsLocation *loc,
		 XdfsState    *state,
		 XdfsParams   *params)
{
  loc->state = state;

  memset (state, 0, sizeof (*state));

  /* Default values */
  state->policy                       = XP_ForwardJump;
  state->cluster_max_versions         = 40;
  state->src_buffer_max_files         = 0;
  state->src_buffer_max_size          = (1 << 26);
  state->src_buffer_max_size_per_file = (1 << 24);
  state->src_buffer_min_size_per_file = 2048;

  /* User inputs */
  if (params)
    {
      state->flags = params->flags;

      if (params->policy != 0)
	state->policy = params->policy;

      if (params->cluster_max_versions != 0)
	state->cluster_max_versions = params->cluster_max_versions;

      if (params->src_buffer_max_files != 0)
	state->src_buffer_max_files = params->src_buffer_max_files;

      if (params->src_buffer_max_size != 0)
	state->src_buffer_max_size = params->src_buffer_max_size;

      if (params->src_buffer_max_size_per_file != 0)
	state->src_buffer_max_size_per_file = params->src_buffer_max_size_per_file;

      if (params->src_buffer_min_size_per_file != 0)
	state->src_buffer_min_size_per_file = params->src_buffer_min_size_per_file;
    }

  if (! (state->policy & XP_IsReverse))
    {
      state->src_buffer_max_files         = 0;
      state->src_buffer_max_size          = 0;
      state->src_buffer_max_size_per_file = 0;
      state->src_buffer_min_size_per_file = 0;
    }

  return TRUE;
}

gboolean
xdfs_location_fields (XdfsLocation* loc)
{
  gboolean md5 = loc->state->flags & XF_MD5Equality;
  gboolean sha = loc->state->flags & XF_SHAEquality;

  if (md5 && sha)
    {
      /* @@@ */
      abort ();
    }

  if (md5)
    loc->md = edsio_message_digest_md5 ();
  else if (sha)
    loc->md = edsio_message_digest_sha ();

  if (loc->state->src_buffer_max_files > 0)
    loc->has_src_buffer = TRUE;

  return TRUE;
}

gboolean
xdfs_location_create (RepoTxn    *txn,
		      Path       *xdfs,
		      XdfsParams *params)
{
  XdfsLocation loc;
  XdfsState    state;

  memset (& loc, 0, sizeof (loc));

  /* Root
   */
  if (! dbfs_inode_find_root (txn, xdfs, DbfsFollowLinks, FT_NotPresent, & loc.loc_ino))
    return FALSE;

  /* State
   */
  if (! xdfs_state_init (& loc, & state, params))
    return FALSE;

  if (! xdfs_state_write (txn, & loc))
    return FALSE;

  if (! xdfs_location_fields (& loc))
    return FALSE;

  /* Sequence
   */
  if (! dbfs_inode_new (txn, & loc.seq_ino))
    return FALSE;

  if (! dbfs_make_sequence (txn, & loc.seq_ino))
    return FALSE;

  if (! dbfs_make_indirect_link_named (txn, & loc.loc_ino, path_basename (xseq_path), & loc.seq_ino))
    return FALSE;

  /* Index
   */
  if (loc.md)
    {
      if (! dbfs_inode_new (txn, & loc.idx_ino))
	return FALSE;

      if (! dbfs_make_index (txn, & loc.idx_ino, FALSE))
	return FALSE;

      if (! dbfs_make_indirect_link_named (txn, & loc.loc_ino, path_basename (xidx_path), & loc.idx_ino))
	return FALSE;
    }

  /* Src buffer
   */
  if (loc.has_src_buffer)
    {
      if (! dbfs_inode_new (txn, & loc.srcdir_ino))
	return FALSE;

      if (! dbfs_make_sequence (txn, & loc.srcdir_ino))
	return FALSE;

      if (! dbfs_make_indirect_link_named (txn, & loc.loc_ino, path_basename (xsrcdir_path), & loc.srcdir_ino))
	return FALSE;
    }

  return TRUE;
}

gboolean
xdfs_location_init (RepoTxn      *txn,
		    Path         *xdfs,
		    XdfsLocation *loc)
{
  memset (loc, 0, sizeof (* loc));

  if (! dbfs_inode_find_root (txn, xdfs, DbfsFollowLinks, FT_Segment, & loc->loc_ino))
    return FALSE;

  if (! xdfs_state_read (txn, loc))
    return FALSE;

  if (! xdfs_location_fields (loc))
    return FALSE;

  if (! dbfs_read_indirect_link_named (txn, & loc->loc_ino, path_basename (xseq_path), FT_Sequence, & loc->seq_ino))
    return FALSE;

  if (loc->md &&
      ! dbfs_read_indirect_link_named (txn, & loc->loc_ino, path_basename (xidx_path), FT_Index, & loc->idx_ino))
    return FALSE;

  if (loc->has_src_buffer &&
      ! dbfs_read_indirect_link_named (txn, & loc->loc_ino, path_basename (xsrcdir_path), FT_Sequence, & loc->srcdir_ino))
    return FALSE;

  return TRUE;
}

/**********************************************************************/
/*			    Insertion Code                            */
/**********************************************************************/

gboolean
xdfs_insert_version (RepoTxn *txn, Path *xdfs, FileHandle *in, Inode *inop_out)
{
  XdfsLocation  loc;
  gboolean      result;

  /* Initialize the XDFS directories
   */
  if (! xdfs_location_init (txn, xdfs, & loc))
    return FALSE;

  result = xdfs_insert_version_loc (txn, & loc, in, inop_out);

  g_free (loc.state);

  return result;
}

/* This function sets up the call to xdfs_insert_new_version, where
 * most of the real work gets done.  If there is an index, it computes
 * the digest of IN and tests for an existing version.
 */
gboolean
xdfs_insert_version_loc (RepoTxn *txn, XdfsLocation *loc, FileHandle *in, Inode *inop_out)
{
  /* @@@ This should use a nested transaction... eventually. */
  FileHandle   *copy = NULL;
  Inode         old_ino;
  Inode         copy_ino;
  gboolean      res = TRUE;
  gboolean      digest = (loc->md != NULL);
  Path         *digest_path = NULL;
  const guint8 *ins_digest = NULL;

  /* Create the new file entry by draining IN, meanwhile compute digest.
   */
  if (! dbfs_inode_new (txn, & copy_ino))
    goto exit;

  if (! (copy = dbfs_inode_open_replace (txn, & copy_ino)))
    goto exit;

  if (digest && ! handle_digest_compute (copy, loc->md))
    goto exit;

  if (! handle_drain (in, copy))
    goto exit;

  if (! handle_close (in))
    goto exit;

  if (! handle_close (copy))
    goto exit;

  old_ino.minor.ino_type = FT_NotFound;

  if (digest)
    {
      /* Look for the digest entry in the index, to see if it already exists.
       */
      if (! (ins_digest = handle_digest (copy, loc->md)))
	goto exit;

      digest_path = path_append_bytes (txn->alloc, path_current (txn->alloc), PP_Hex, ins_digest, loc->md->cksum_len);

      if (! dbfs_inode_find (txn, & loc->idx_ino, digest_path, DbfsNoFollowLinks, FT_IsAny, & old_ino))
	goto exit;
    }

  /* Even if the file already exists (by digest equality), we're adding a new
   * version (the unique_version_count will not be incremented, in that case).
   */
  loc->state->total_version_count += 1;

  switch (old_ino.minor.ino_type)
    {
    case FT_NotFound:

      /* Insert the link into the index, if there is one.
       */
      if (digest && ! dbfs_link_create (txn, & loc->idx_ino, & copy_ino, path_basename (digest_path), DbfsNoOverwrite))
	goto exit;

      /* Link new version into the sequence
       */
      if (! dbfs_link_create_next (txn, & loc->seq_ino, & copy_ino))
	goto exit;

      if (! xdfs_insert_new_version (txn, loc, & copy_ino))
	goto exit;

      if (inop_out)
	(* inop_out) = copy_ino;

      /* If StoreDigest is set, store the digest value in a minor inode.
       */
      if (loc->state->flags & XF_StoreDigest)
	{
	  FileHandle* dfh;
	  Inode       d_ino;
	  BaseName    bn;

	  bn.data = (char*) loc->md->name;
	  bn.len  = strlen (bn.data) + 1;

	  if (! (dfh = dbfs_inode_open_replace_named (txn, & copy_ino, & bn, & d_ino)))
	    goto exit;

	  if (! handle_write (dfh, ins_digest, loc->md->cksum_len))
	    goto exit;

	  if (! handle_close (dfh))
	    goto exit;

	  handle_free (dfh);
	}

      break;

    case FT_Segment:
    case FT_View:

#ifdef DEBUG_XDFS
      g_print ("Duplicate insertion skipped...\n");
#endif

      /* The file was found in the index, link the old version into a
       * new sequence entry
       */
      if (! dbfs_link_create_next (txn, & loc->seq_ino, & old_ino))
	goto exit;

      if (inop_out)
	(* inop_out) = old_ino;

      /* Copy_ino is not referenced, it will delete at commit.
       */
      break;

    default:
      /*dbfs_generate_inode_event (EC_XdfsUnexpectedFileType, & old_ino);*/
      /* @@@ */
      abort ();
    }

  if (0)
    {
    exit:
      res = FALSE;
    }

  if (res && ! xdfs_state_write (txn, loc))
    res = FALSE;

  if (copy)
    handle_free (copy);

  return res;
}

/* This is where the compression actually happens
 */
gboolean
xdfs_insert_new_version (RepoTxn      *txn,
			 XdfsLocation *loc,
			 Inode        *copy_ino)
{
  Inode            cur_ino;
  Inode            patch_ino;
  Inode            from_ino;
  Inode            to_ino;
  Inode            cont_ino;
  Inode           *patch_link_ino = NULL;
  XdeltaGenerator *gen;
  FileHandle      *delta_to;
  FileHandle      *patch_out;
  XdeltaControl   *control;
  gboolean         reject_delta = TRUE;
  XdfsPolicy       policy = loc->state->policy;
  int              i;

  /* Update counters.
   */
  loc->state->literals             += 1;
  loc->state->unique_version_count += 1;
  loc->state->unencoded_size       += copy_ino->minor.ino_segment_len;
  loc->state->literal_size         += copy_ino->minor.ino_segment_len;

  /* If no compression, that's all there was to do.  (There's no current
   * link for XP_NoCompress archives).
   */
  if (policy == XP_NoCompress)
    return TRUE;

  /* Lookup current version in xdfsdir.
   */
  if (! dbfs_read_indirect_link_named (txn, & loc->loc_ino, path_basename (xcur_path), FT_IsAny, & cur_ino))
    return FALSE;

  switch (cur_ino.minor.ino_type)
    {
    case FT_NotFound:
      /* If the current version link isn't there, then this is the first version.
       * Create the link and return.
       */
      if (! dbfs_make_indirect_link_named (txn, & loc->loc_ino, path_basename (xcur_path), copy_ino))
	return FALSE;

      loc->state->cluster_versions          = 1;
      loc->state->cluster_uncompressed_size = copy_ino->minor.ino_segment_len;
      loc->state->cluster_compressed_size   = copy_ino->minor.ino_segment_len;

      return TRUE;

    case FT_Segment:
      /* There is not the first version, continue.
       */
      break;

    default:
      dbfs_generate_int_event (EC_DbfsInvalidFileType, cur_ino.key.inum);
      return FALSE;
    }

  /* Prepare to compute the delta
   */
  gen = xdp_generator_new ();

  if (policy & XP_IsReverse)
    {
      /* Current is being replaced by reverse delta.
       */
      if (! xdfs_source_reverse_segments (txn, loc, gen))
	return FALSE;

      from_ino = *copy_ino;
      to_ino = cur_ino;
    }
  else
    {
      /* Current stays the same, compute a forward delta.
       */
      from_ino = cur_ino;
      to_ino = *copy_ino;
    }

  if (! xdfs_source_segment (txn, loc, gen, XS_Primary, & from_ino))
    return FALSE;

  if (! (delta_to = dbfs_inode_open_read (txn, & to_ino)))
    return FALSE;

  /* Now compute the delta.
   */
  if (! dbfs_inode_new (txn, & patch_ino))
    return FALSE;

  if (! (patch_out = dbfs_inode_open_replace (txn, & patch_ino)))
    return FALSE;

  if (! (control = xdp_generate_delta (gen, delta_to, NULL, patch_out)))
    return FALSE;

  if (! handle_close (delta_to))
    return FALSE;

  handle_free (delta_to);

  handle_free (patch_out);

  /* Write its control.  Writing it before deciding to reject, meaning
   * that if it gets rejected the pseudo-delta control will be
   * re-written.
   */
  if (! xdfs_control_write (txn, & to_ino, control, & cont_ino))
    return FALSE;

  /* Decide whether to reject this delta.
   */
  reject_delta = xdfs_reject_delta (txn, loc, copy_ino, & to_ino, & patch_ino, & cont_ino);

  if (reject_delta)
    {
      /* Patch_ino will remain unreferenced, it will delete at commit.
       * Must create a pseudo delta here.  This is like a delta
       * without a patch file, and it accompanies a literal file.  It
       * is used for delta extraction only.  If there is a patch file,
       * its copy instructions are replaced with copy instructions for
       * the literal file, which will remain.
       */
      if (xdp_has_data (gen))
	{
	  for (i = 0; i < control->inst_len; i += 1)
	    {
	      XdeltaInstruction *xi = control->inst + i;

	      if (xi->index == 0)
		xi->offset = xi->output_start;
	    }

	  /* The patch link now points back at TO.
	   */
	  patch_link_ino = & to_ino;
	}

      /* Re-write its control.  Note!!!  Could avoid this by estimating
       * the control length, and it can be avoided in any case for
       * computing Reverse deltas, since reject_delta ignores it.
       */
      if (! xdfs_control_write (txn, & to_ino, control, & cont_ino))
	return FALSE;

      /* We've started a new cluster.
       */
      loc->state->cluster_versions          = 1;
      loc->state->cluster_uncompressed_size = copy_ino->minor.ino_segment_len;
      loc->state->cluster_compressed_size   = copy_ino->minor.ino_segment_len;

      /* @@@ Problem: Cheating on the VIEW stack here, I want to
       * create a control and links without a view.  What to do?
       */
    }
  else
    {
      /* Update counters.
       */
      loc->state->literals                  -= 1;
      loc->state->literal_size              -= to_ino.minor.ino_segment_len;
      loc->state->patch_size                += patch_ino.minor.ino_segment_len;
      loc->state->cluster_versions          += 1;
      loc->state->cluster_uncompressed_size += copy_ino->minor.ino_segment_len;
      loc->state->cluster_compressed_size   += copy_ino->minor.ino_segment_len;
      loc->state->cluster_compressed_size   -= to_ino.minor.ino_segment_len;
      loc->state->cluster_compressed_size   += patch_ino.minor.ino_segment_len;
      loc->state->cluster_compressed_size   += cont_ino.minor.ino_segment_len;

      if (xdp_has_data (gen))
	{
	  loc->state->patches += 1;

	  patch_link_ino = & patch_ino;
	}

      /* Create view in to_ino, replacing old contents.
       */
      if (! dbfs_inode_delete (txn, & to_ino))
	return FALSE;

      if (! dbfs_make_view (txn, & to_ino, to_ino.minor.ino_segment_len, xdfs_stack_id, xdfs_reconstruct_view))
	return FALSE;
    }

  /* Update counters.
   */
  loc->state->control_size += cont_ino.minor.ino_segment_len;

  /* Create links to its sources (one may be the patch_link)
   */
  for (i = 0; i < control->src_count; i += 1)
    {
      Inode   *src_ino = xdp_source_data (gen, i);
      BaseName si;
      guint16  si_be = GUINT16_TO_BE (i);

      if (xdp_has_data (gen) && i == 0)
	{
	  /* In this case it is the delta's patch file, or
	   * the TO file if the delta is rejected.
	   */
	  g_assert (src_ino == NULL);
	  src_ino = patch_link_ino;
	}
      else
	{
	  g_assert (src_ino);

	  /* If its not FROM, then it must be a backward
	   * source buffer copy.  */
	  if (src_ino != & from_ino)
	    loc->state->src_buffer_copied += xdp_source_copies (gen, i);
	}

      si.data = (void*) & si_be;
      si.len  = sizeof (si_be);

      if (! dbfs_make_indirect_link_stacked_named (txn, & to_ino, xdfs_stack_id, & si, src_ino))
	return FALSE;
    }

  /* Decide whether to place patch in source set, what to remove,
   * etc.  This only happens if the delta generated a patch and if
   * the file is presumed to be evolving.
   */
  if (! reject_delta && xdp_has_data (gen) && (policy & XP_IsReverse))
    {
      if (! xdfs_source_evolve (txn, loc, & patch_ino))
	return FALSE;
    }

  xdp_generator_free (gen);

  xdp_control_free (control);

  if ((policy & XP_IsReverse) || ((policy & XP_IsEvolving) && reject_delta))
    {
      /* If Reverse is true or ForwardJump indicates JUMP, replace the
       * current version (CUR) with the new file (COPY).
       */
      if (! dbfs_inode_delete_named (txn, & loc->loc_ino, path_basename (xcur_path)))
	return FALSE;

      if (! dbfs_make_indirect_link_named (txn, & loc->loc_ino, path_basename (xcur_path), copy_ino))
	return FALSE;
    }

  return TRUE;
}

gboolean
xdfs_source_reverse_segments (RepoTxn         *txn,
			      XdfsLocation    *loc,
			      XdeltaGenerator *gen)
{
  /* Open the SRC sequence, where a history of recent patches is
   * stored and later used in the computation of reverse deltas.
   */

  Cursor *c = NULL;
  int x = 0;

  if (! loc->has_src_buffer)
    return TRUE;

  if (! (c = dbfs_cursor_open (txn, & loc->srcdir_ino, CT_Container)))
    goto exit;

  while (dbfs_cursor_next (c))
    {
      Inode *one_ino = alc_new (txn->alloc, Inode);

      if (! dbfs_cursor_inode (c, FT_Segment, one_ino))
	goto exit;

      if (! xdfs_source_segment (txn, loc, gen, XS_Secondary, one_ino))
	goto exit;

      x += 1;
    }

  if (! dbfs_cursor_close (c))
    goto exit;

  return TRUE;

 exit:

  if (c)
    dbfs_cursor_close (c);

  return FALSE;
}

gboolean
xdfs_source_segment (RepoTxn         *txn,
		     XdfsLocation    *loc,
		     XdeltaGenerator *gen,
		     XdeltaSourceType type,
		     Inode           *src)
{
  XdeltaSource *xs;
  FileHandle *fh;

  if (! (fh = dbfs_inode_open_read (txn, src)))
    goto exit;

  if (! (xs = xdp_source_new (fh, type, src)))
    goto exit;

  if (! xdp_source_add (gen, xs))
    goto exit;

  return TRUE;

 exit:

  if (fh)
    handle_close (fh);

  return FALSE;
}

gboolean
xdfs_source_evolve (RepoTxn         *txn,
		    XdfsLocation    *loc,
		    Inode           *patch_ino)
{
  guint32 this_len = patch_ino->minor.ino_segment_len;
  gboolean once = TRUE;
  Cursor *c = NULL;

  if (! loc->has_src_buffer)
    return TRUE;

  if (this_len > loc->state->src_buffer_max_size_per_file ||
      this_len < loc->state->src_buffer_min_size_per_file ||
      this_len > loc->state->src_buffer_max_size)
    return TRUE;

  loc->state->src_buffer_files += 1;
  loc->state->src_buffer_size += this_len;

  if (! dbfs_link_create_next (txn, & loc->srcdir_ino, patch_ino))
    goto exit;

  while (loc->state->src_buffer_files > loc->state->src_buffer_max_files ||
	 loc->state->src_buffer_size > loc->state->src_buffer_max_size)
    {
      Inode one_ino;

      if (once)
	{
	  once = FALSE;

	  if (! (c = dbfs_cursor_open (txn, & loc->srcdir_ino, CT_Container)))
	    goto exit;
	}

      if (! dbfs_cursor_next (c))
	goto exit;

      if (! dbfs_cursor_inode (c, FT_Segment, & one_ino))
	goto exit;

      loc->state->src_buffer_files -= 1;
      loc->state->src_buffer_size -= one_ino.minor.ino_segment_len;

      if (! dbfs_cursor_delete (c))
	goto exit;
    }

  if (c && ! dbfs_cursor_close (c))
    goto exit;

  return TRUE;

 exit:

  if (c)
    dbfs_cursor_close (c);

  return FALSE;
}

gboolean
xdfs_reject_delta (RepoTxn         *txn,
		   XdfsLocation    *loc,
		   Inode           *copy_ino,
		   Inode           *to_ino,
		   Inode           *patch_ino,
		   Inode           *cont_ino)
{
  /* Never reject a delta if it is not evolving.
   */
  if (! (loc->state->policy & XP_IsEvolving))
    return FALSE;

  if (loc->state->policy & XP_IsForward)
    {
      /* Reject if the compression ratio will decline (finding a local minima)
       */
      guint new_uncomp = loc->state->cluster_uncompressed_size + copy_ino->minor.ino_segment_len;
      guint new_comp   = loc->state->cluster_compressed_size;
      double current_ratio;
      double new_ratio;

      new_comp += copy_ino->minor.ino_segment_len;
      new_comp -= to_ino->minor.ino_segment_len;
      new_comp += patch_ino->minor.ino_segment_len;
      new_comp += cont_ino->minor.ino_segment_len;

      current_ratio = (double) loc->state->cluster_compressed_size / (double) loc->state->cluster_uncompressed_size;
      new_ratio     = (double) new_comp / (double) new_uncomp;

      if (new_ratio > current_ratio)
	{
#ifdef DEBUG_XDFS
	  g_print ("Delta rejected (minima): versions: %d\n", loc->state->cluster_versions);
#endif
	  return TRUE;
	}
    }
  else
    {
      /* Reject if the chain is too long.
       */
      if (loc->state->cluster_versions >= loc->state->cluster_max_versions)
	{
#ifdef DEBUG_XDFS
	  g_print ("Delta rejected (chain): versions: %d\n", loc->state->cluster_versions);
#endif
	  return TRUE;
	}
    }

  return FALSE;
}

gboolean
xdfs_control_write (RepoTxn         *txn,
		    Inode           *inode,
		    XdeltaControl   *control,
		    Inode           *cino_out)
{
  FileHandle *control_handle;

  if (! (control_handle = dbfs_inode_open_replace_stacked (txn, inode, xdfs_stack_id, cino_out)))
    return FALSE;

  if (! xdp_control_write (control, control_handle))
    return FALSE;

  if (! handle_close (control_handle))
    return FALSE;

  handle_free (control_handle);

  return TRUE;
}

gboolean
xdfs_control_read (RepoTxn         *txn,
		   Inode           *inode,
		   XdeltaControl  **control_out)
{
  FileHandle *control_handle;
  Inode out;

  if (! (control_handle = dbfs_inode_open_read_stacked (txn, inode, xdfs_stack_id, & out)))
    return FALSE;

  if (! ((* control_out) = xdp_control_read (control_handle)))
    return FALSE;

  if (! handle_close (control_handle))
    return FALSE;

  handle_free (control_handle);

  return TRUE;
}

/**********************************************************************/
/*			    Retrieval Code                            */
/**********************************************************************/

/* This function translates the Xdelta instruction TRANSLATE into an
 * XdfsInstruction coresponding to the current, local, literal file
 * set.  TRANSLATE offsets refer to the TRANSLATE_INDEX.  The output
 * is in APPEND_INST_ARRAY.
 */
void
xdfs_view_translate_copies (XdeltaInstruction *translate,
			    XdfsLocalIndex    *translate_index,
			    GArray            *append_inst_array)
{
  guint l = 0;
  guint u = translate_index->inst_len - 1;
  guint i;
  guint search_offset = translate->offset;
  guint output_start = translate->output_start;
  const XdfsInstruction *inst;

  g_assert (search_offset < translate_index->length);
  g_assert (translate->length > 0);

  /* Binary search */

 again:

  g_assert (u >= l);

  i = (u+l)/2;

  inst = & translate_index->inst[i];

  if (search_offset < inst->output_start)
    {
      u = i - 1;
      goto again;
    }
  else if (search_offset >= (inst->output_start + inst->length))
    {
      l = i + 1;
      goto again;
    }
  else
    {
      guint to_copy = translate->length;

      while (to_copy > 0)
	{
	  XdfsInstruction copy;
	  guint inst_offset;
	  guint this_copy;

	  inst_offset = search_offset - inst->output_start;
	  this_copy   = MIN (to_copy, inst->length - inst_offset);

	  copy.offset = inst->offset + inst_offset;
	  copy.length = this_copy;
	  copy.inop   = inst->inop;
	  copy.output_start = output_start;

	  g_array_append_val (append_inst_array, copy);

	  output_start += this_copy;
	  search_offset += this_copy;
	  to_copy -= this_copy;
	  inst += 1;
	}
    }
}

gboolean
xdfs_view_index_delta (Inode          *inode,
		       XdfsLocalIndex *lip,
		       XdfsLocalIndex *src_lis,
		       XdeltaControl  *control)
{
  guint i;
  GArray *inst_array = g_array_new (FALSE, FALSE, sizeof (XdfsInstruction));

  for (i = 0; i < control->inst_len; i += 1)
    {
      XdeltaInstruction *inst = & control->inst[i];
      XdfsLocalIndex *src_li = src_lis + inst->index;

      if (src_li->literal)
	{
	  XdfsInstruction xi;

	  xi.inop     = src_li->inop;
	  xi.offset   = inst->offset;
	  xi.length   = inst->length;
	  xi.output_start = inst->output_start;

	  g_array_append_val (inst_array, xi);
	}
      else
	{
	  xdfs_view_translate_copies (inst, src_li, inst_array);
	}
    }

  if (lip->inst_array)
    g_array_free (lip->inst_array, TRUE);

  lip->literal = FALSE;
  lip->inst = (XdfsInstruction*) inst_array->data;
  lip->inst_len = inst_array->len;
  lip->length = inode->minor.ino_segment_len;
  lip->inst_array = inst_array;

  return TRUE;
}

gboolean
xdfs_view_index_source (RepoTxn *txn, Inode* inop, XdfsLocalIndex* lip, XdfsLocalIndex* root)
{
  lip->inop = inop;

  switch (inop->minor.ino_type)
    {
    case FT_Segment:
      lip->if_seg.inop = inop;
      lip->if_seg.offset = 0;
      lip->if_seg.length = inop->minor.ino_segment_len;
      lip->if_seg.output_start = 0;

      lip->literal = TRUE;
      lip->length = inop->minor.ino_segment_len;
      lip->inst = & lip->if_seg;
      lip->inst_len = 1;
      break;

    case FT_View:
      {
	int i;
	XdfsLocalIndex *src_lis;
	XdeltaControl *control;

	g_assert (txn);	/* Just a test... the call from
			 * xdfs_extract_to_chain shouldn't reach this point. */

	if (! xdfs_control_read (txn, inop, & control))
	  return FALSE;

	src_lis = g_new0 (XdfsLocalIndex, control->src_count);

	for (i = 0; i < control->src_count; i += 1)
	  {
	    BaseName si;
	    guint16  si_be = GUINT16_TO_BE (i);
	    Inode *src_ino;

	    si.data = (void*) & si_be;
	    si.len  = sizeof (si_be);

	    src_ino = g_new (Inode, 1);

	    root->free_list = g_slist_prepend (root->free_list, src_ino);

	    if (! dbfs_read_indirect_link_stacked_named (txn, inop, xdfs_stack_id, & si, FT_IsAny, src_ino))
	      return FALSE;

	    switch (src_ino->minor.ino_type)
	      {
	      case FT_Segment:
	      case FT_View:
		if (! xdfs_view_index_source (txn, src_ino, src_lis + i, root))
		  goto exit;

		break;

	      default:
		abort ();
	      }
	  }

	if (! xdfs_view_index_delta (inop, lip, src_lis, control))
	  goto exit;

	for (i = 0; i < control->src_count; i += 1)
	  {
	    if (src_lis[i].inst_array)
	      g_array_free (src_lis[i].inst_array, TRUE);
	  }

	xdp_control_free (control);

	g_free (src_lis);
      }
      break;

    default:
      abort ();
      /* @@@ */
      return FALSE;
    }

  return TRUE;

 exit:

  /* @@@ cleanup */
  return FALSE;
}

gboolean
xdfs_view_begin (RepoTxn *txn,
		 Inode   *inop,
		 void    *private)
{
  XdfsLocalIndex *li = private;

  if (! xdfs_view_index_source (txn, inop, li, li))
    return FALSE;

  if (! (li->linger = dbfs_linger_open (txn)))
    return FALSE;

  return TRUE;
}

void
xdfs_local_index_free (XdfsLocalIndex *li)
{
  GSList *p;

  if (li->linger)
    dbfs_linger_close (li->linger);

  for (p = li->free_list; p; p = p->next)
    g_free (p->data);

  g_slist_free (li->free_list);

  if (li->inst_array)
    g_array_free (li->inst_array, TRUE);
}

gboolean
xdfs_view_end (RepoTxn *txn,
	       Inode   *inop,
	       void    *private)
{
  XdfsLocalIndex *li = private;

  xdfs_local_index_free (li);

  return TRUE;
}

gboolean
xdfs_view_page_in (RepoTxn *txn,
		   Inode   *inop,
		   void    *private,
		   guint8  *pgbuf,
		   gsize    offset,
		   gsize    len)
{
  XdfsLocalIndex *li = private;
  const XdfsInstruction *inst;
  guint u;
  guint l = 0;
  guint i;

  u = li->inst_len - 1;

  g_assert ((offset + len) <= li->length);

 again:
  /* Binary search */

  i = (u+l)/2;

  inst = & li->inst[i];

  if (offset < inst->output_start)
    {
      u = i - 1;
      goto again;
    }
  else if (offset >= (inst->output_start + inst->length))
    {
      l = i + 1;
      goto again;
    }
  else
    {
      gsize written = 0;

      while (len > 0)
	{
	  guint inst_offset;
	  guint this_copy;
	  FileHandle *fh;

	  inst_offset = offset - inst->output_start;
	  this_copy   = MIN (len, inst->length - inst_offset);

	  inst_offset += inst->offset;

	  if (! (fh = dbfs_inode_open_linger (li->linger, inst->inop)))
	    return FALSE;

	  if (handle_seek (fh, inst_offset, HANDLE_SEEK_SET) != inst_offset)
	    return FALSE;

	  if (handle_read (fh, pgbuf + written, this_copy) != this_copy)
	    return FALSE;

	  written += this_copy;
	  offset  += this_copy;
	  len     -= this_copy;
	  inst    += 1;
	}
    }

  return TRUE;
}

/**********************************************************************/
/*			Delta Extraction Code                         */
/**********************************************************************/

/* A function that returns:
 *   < 0 if a is less distant than b
 *   = 0 if b is equal to a
 *   > 0 if a is more distant than b
 * where distance is measured from the root, which is the most current
 * version.  It is a partial order based on insertion time (inode
 * number).
 */
int
xdfs_extract_furthest (XdfsExtraction *ex)
{
  if (ex->loc->state->policy & XP_IsForward)
    return ex->from.loc_inop->key.inum - ex->to.loc_inop->key.inum;
  else
    return ex->to.loc_inop->key.inum - ex->from.loc_inop->key.inum;
}

void
xdfs_extract_free_elt (XdfsExtrElt *elt)
{
  xdp_control_free (elt->control);

  g_free (elt->src_lis);
  g_free (elt->src_inos);
  g_free (elt);
}

void
xdfs_extract_free_elt_fe (gpointer	data,
			  gpointer	user_data)
{
  xdfs_extract_free_elt (data);
}

gboolean
xdfs_extract_advance (RepoTxn        *txn,
		      XdfsExtraction *ex,
		      XdfsExtrHalf   *half,
		      gboolean        is_to_chain,
		      gboolean       *broken)
{
  XdeltaControl *control;
  XdfsExtrElt   *elt;
  int            i;

  if (! xdfs_control_read (txn, half->loc_inop, & control))
    return FALSE;

  elt = g_new0 (XdfsExtrElt, 1);

  elt->this_inop = half->loc_inop;
  elt->control   = control;
  elt->src_lis   = g_new0 (XdfsLocalIndex, control->src_count);
  elt->src_inos  = g_new0 (Inode, control->src_count);

  for (i = 0; i < control->src_count; i += 1)
    {
      gboolean  is_primary = control->src_types[i] == XS_Primary;
      Inode    *src_inop   = & elt->src_inos[i];

      if (is_to_chain || is_primary)
	{
	  BaseName si;
	  guint16  si_be = GUINT16_TO_BE (i);

	  si.data = (void*) & si_be;
	  si.len  = sizeof (si_be);

	  if (! dbfs_read_indirect_link_stacked_named (txn, elt->this_inop, xdfs_stack_id, & si, FT_IsAny, src_inop))
	    return FALSE;

	  elt->src_lis[i].inop = src_inop;

	  switch (src_inop->minor.ino_type)
	    {
	    case FT_View:
	    case FT_Segment:
	      break;

	    default:
	      abort ();
	    }
	}

      if (is_primary)
	{
	  elt->primary_index = i;
	  elt->primary_inop  = src_inop;
	}
    }

  if (! elt->primary_inop)
    {
      /* If there is no primary, the chain is broken.
       */
      xdfs_extract_free_elt (elt);
      (* broken) = TRUE;
      return TRUE;
    }

  half->chain = g_slist_prepend (half->chain, elt);
  half->loc_inop = elt->primary_inop;
  half->chain_len += 1;

  (* broken) = FALSE;
  return TRUE;
}

void
xdfs_extract_from_chain (XdfsExtraction  *ex,
			 SkipList       **sl_out)
{
  GSList          *chain;
  SkipList        *sl = NULL;

  /* The chain is reversed from the traversal.
   */
  ex->from.chain = g_slist_reverse (ex->from.chain);

  for (chain = ex->from.chain; chain; chain = chain->next)
    {
      XdfsExtrElt *elt         = chain->data;
      guint64      this_len    = elt->this_inop->minor.ino_segment_len;
      guint64      primary_len = elt->primary_inop->minor.ino_segment_len;
      SkipList    *new_sl      = xdfs_skip_list_new (primary_len);
      SkipList    *tmp;

      const XdeltaInstruction *inst = elt->control->inst;
      int inst_len = elt->control->inst_len;
      int primary_index = elt->primary_index;
      int i;

      if (! sl)
	{
	  sl = xdfs_skip_list_new (this_len);

	  xdfs_skip_list_insert (sl, 0, this_len, 0);
	}

      for (i = 0; i < inst_len; i += 1)
	{
	  const XdeltaInstruction *xi = inst + i;
	  SkipListNode *sln;
	  int copy_length, copy_offset, copy_start, from_offset, from_length, from_start, this_length;

	  if (xi->index != primary_index)
	    continue;

	  copy_start  = xi->output_start;
	  copy_length = xi->length;
	  copy_offset = xi->offset;

	  while (copy_length > 0)
	    {
	      /* This is the translate_copies logic for skip lists.
	       */
	      sln = xdfs_skip_list_search_nearest (sl, copy_start);

	      from_start  = xdfs_skip_list_offset (sln);
	      from_length = xdfs_skip_list_length (sln);
	      from_offset = xdfs_skip_list_data (sln);

	      if (from_start > copy_start)
		{
		  int diff = from_start - copy_start;

		  copy_start  += diff;
		  copy_offset += diff;
		  copy_length -= diff;
		}
	      else if (from_start < copy_start)
		{
		  int diff = copy_start - from_start;

		  from_start  += diff;
		  from_offset += diff;
		  from_length -= diff;
		}

	      this_length = MIN (copy_length, from_length);

	      if (this_length <= 0)
		break;

	      xdfs_skip_list_insert (new_sl, copy_offset, this_length, from_offset);

	      copy_offset += this_length;
	      copy_start  += this_length;
	      copy_length -= this_length;
	    }
	}

      tmp = sl;
      sl = new_sl;

      xdfs_skip_list_free (tmp);
    }

  (* sl_out) = sl;
}

#ifdef DEBUG_XDFS
static void
xdfs_extract_assert_length (XdfsLocalIndex *li)
{
  int i;
  guint len = 0;

  for (i = 0; i < li->inst_len; i += 1)
    {
      len += li->inst[i].length;
    }

  g_assert (len == li->length);
}
#endif

gboolean
xdfs_extract_overlay (XdfsLocalIndex *li, SkipList *overlay, Inode *from_inop)
{
  GArray *inst_array = g_array_new (FALSE, FALSE, sizeof (XdfsInstruction));
  guint pos = 0;
  guint inst_i = 0;

  SkipListNode *sln = xdfs_skip_list_first (overlay);
  guint over_start  = li->length + 1;

#ifdef DEBUG_XDFS
  xdfs_extract_assert_length (li);
#endif

  if (sln)
    over_start = xdfs_skip_list_offset (sln);

  while (pos < li->length)
    {
      if (pos == over_start)
	{
	  XdfsInstruction over;

	  over.length = xdfs_skip_list_length (sln);
	  over.offset = xdfs_skip_list_data   (sln);
	  over.output_start = over_start;
	  over.inop = from_inop;

	  g_array_append_val (inst_array, over);

	  sln        = xdfs_skip_list_next   (sln);

	  if (sln)
	    over_start = xdfs_skip_list_offset (sln);
	  else
	    over_start = li->length + 1;

	  pos += over.length;
	}
      else
	{
	  /* Copy instructions, stopping at over_start.
	   */
	  int copy_length, copy_start, copy_offset;
	  const XdfsInstruction *orig;
	  XdfsInstruction xi;

	  while (li->inst[inst_i].output_start + li->inst[inst_i].length < pos)
	    inst_i += 1;

	  orig = li->inst + inst_i;

	  copy_start  = orig->output_start;
	  copy_offset = orig->offset;
	  copy_length = orig->length;

	  if (copy_start < pos)
	    {
	      int diff = pos - copy_start;

	      copy_start  += diff;
	      copy_offset += diff;
	      copy_length -= diff;
	    }

	  if (pos + copy_length > over_start)
	    {
	      copy_length = over_start - pos;
	    }
	  else
	    {
	      inst_i += 1;
	    }

	  xi.length = copy_length;
	  xi.inop   = orig->inop;
	  xi.offset = copy_offset;
	  xi.output_start = copy_start;

	  g_array_append_val (inst_array, xi);

	  pos += xi.length;
	}
    }

  if (li->inst_array)
    g_array_free (li->inst_array, TRUE);

  li->literal = FALSE;
  li->inst_array = inst_array;
  li->inst = (XdfsInstruction*) inst_array->data;
  li->inst_len = inst_array->len;

#ifdef DEBUG_XDFS
  xdfs_extract_assert_length (li);
#endif

  return TRUE;
}

gboolean
xdfs_extract_to_chain (XdfsExtraction *ex, XdfsLocalIndex *middle_li)
{
  GSList *chain = ex->to.chain;

  for (; chain; chain = chain->next)
    {
      XdfsExtrElt *elt = chain->data;
      int i;

      for (i = 0; i < elt->control->src_count; i += 1)
	{
	  if (i == elt->primary_index)
	    {
	      elt->src_lis[i] = * middle_li;
	    }
	  else
	    {
	      if (! xdfs_view_index_source (NULL, elt->src_lis[i].inop, & elt->src_lis[i], middle_li))
		return FALSE;
	    }
	}

      if (! xdfs_view_index_delta (elt->this_inop, middle_li, elt->src_lis, elt->control))
	return FALSE;

#ifdef DEBUG_XDFS
      xdfs_extract_assert_length (middle_li);
#endif
    }

  return TRUE;
}

gboolean
xdfs_extract_output_delta (RepoTxn        *txn,
			   XdfsLocalIndex *li,
			   Inode          *from_inop,
			   FileHandle     *delta_out)
{
  XdeltaControl      cont;
  GArray            *out_inst = g_array_new (FALSE, FALSE, sizeof (XdeltaInstruction));
  guint32            types[2];
  LingerHandle      *linger;
  int patch_index   = -1;
  int primary_index = -1;
  int i;
  guint patch_length = 0;
#ifdef DEBUG_XDFS
  guint copy_length = 0;
#endif

  cont.src_count = 0;
  cont.src_types = types;
  cont.length = li->length;

  if (! (linger = dbfs_linger_open (txn)))
    return FALSE;

  for (i = 0; i < li->inst_len; i += 1)
    {
      const XdfsInstruction *xi = li->inst + i;
      XdeltaInstruction one;

      if (xi->inop == from_inop)
	{
	  if (primary_index < 0)
	    {
	      primary_index = cont.src_count;
	      types[cont.src_count ++] = XS_Primary;
	    }

	  one.index = primary_index;
	  one.offset = xi->offset;

#ifdef DEBUG_XDFS
	  copy_length += xi->length;
#endif
	}
      else
	{
	  if (patch_index < 0)
	    {
	      patch_index = cont.src_count;
	      types[cont.src_count ++] = XS_Patch;
	    }

	  one.index = patch_index;
	  one.offset = patch_length;

	  patch_length += xi->length;
	}

      one.length = xi->length;

      g_array_append_val (out_inst, one);
    }

#ifdef DEBUG_XDFS
  printf ("copy: %d; patch: %d (%0.2f%%)\n", copy_length, patch_length, 100.0 * (double) patch_length / ((double) copy_length + patch_length));
#endif

  cont.inst_len = out_inst->len;
  cont.inst = (XdeltaInstruction*) out_inst->data;

  if (! xdp_control_write (& cont, delta_out))
    return FALSE;

  for (i = 0; i < li->inst_len; i += 1)
    {
      const XdfsInstruction *xi = li->inst + i;

      if (xi->inop != from_inop)
	{
	  FileHandle *fh;

	  if (! (fh = dbfs_inode_open_linger (linger, xi->inop)))
	    return FALSE;

	  if (! handle_copy (fh, delta_out, xi->offset, xi->length))
	    return FALSE;
	}
    }

  dbfs_linger_close (linger);
  g_array_free (out_inst, TRUE);

  return TRUE;
}

void
xdfs_extract_complete_overlay (XdfsLocalIndex *li, Inode *inop)
{
  li->inop = inop;
  li->if_seg.inop = inop;
  li->if_seg.offset = 0;
  li->if_seg.length = inop->minor.ino_segment_len;
  li->if_seg.output_start = 0;

  li->length = inop->minor.ino_segment_len;
  li->inst = & li->if_seg;
  li->inst_len = 1;
}

gboolean
xdfs_extract_delta (RepoTxn    *txn,
		    Path       *xdfs,
		    Inode      *from_inop,
		    Inode      *to_inop,
		    FileHandle *delta_out)
{
  XdfsExtraction  ex;
  XdfsLocation    loc;
  XdfsLocalIndex  middle_li;
  SkipList       *from_copies = NULL;
  gboolean        broken = FALSE;
  int f;

  memset (& ex, 0, sizeof (ex));
  memset (& middle_li, 0, sizeof (middle_li));

  if (! xdfs_location_init (txn, xdfs, & loc))
    return FALSE;

  g_assert (loc.state->policy != XP_NoCompress);

  ex.loc            = & loc;
  ex.from.loc_inop  = from_inop;
  ex.from.orig_inop = from_inop;
  ex.to.loc_inop    = to_inop;
  ex.to.orig_inop   = to_inop;

  /* Perform a DAG traversal here, where the edges are delta copies
   * and this loop advances along the oldest edge until the two points
   * meet.
   */
  while (! broken && (f = xdfs_extract_furthest (& ex)) != 0)
    {
      if (f > 0)
	{
	  if (! xdfs_extract_advance (txn, & ex, & ex.from, FALSE, & broken))
	    return FALSE;
	}
      else
	{
	  if (! xdfs_extract_advance (txn, & ex, & ex.to, TRUE, & broken))
	    return FALSE;
	}
    }

#ifdef DEBUG_XDFS
  g_print ("%p: Extract %qd->%qd: chain length: from: %d; to: %d\n", txn->dbfs,
	   from_inop->key.inum, to_inop->key.inum, ex.from.chain_len, ex.to.chain_len);
#endif

  /* @@@ Implement the special case here.
   */

  /* Construct a local index for the middle of the chain (end of the
   * TO chain).
   */
  if (broken || ex.from.chain)
    {
      /* If the chain NOT broken and there is no FROM chain, then
       * there will be a complete overlay.  That is MIDDLE == FROM.
       */
      if (! xdfs_view_index_source (txn, ex.to.loc_inop, & middle_li, & middle_li))
	return FALSE;
    }

  /* If broken is TRUE, then the FROM and TO chains are not joined.  This
   * code degenerates into a complete view extraction.  This only happens
   * when there are no primary copies.
   */
  if (! broken)
    {
      if (ex.from.chain)
	{
	  /* Compute FROM copies in MIDDLE_LI by inverting the FROM chain.
	   */
	  xdfs_extract_from_chain (& ex, & from_copies);

	  /* FROM_COPIES now represents an overlay for MIDDLE_LI, but it may be NULL
	   * when there is no FROM chain.
	   */
	  if (! xdfs_extract_overlay (& middle_li, from_copies, from_inop))
	    return FALSE;
	}
      else
	{
	  /* Complete overlay.
	   */
	  xdfs_extract_complete_overlay (& middle_li, from_inop);
	}
    }

  /* Finish index construction using the TO chain beginning with
   * MIDDLE_LI, which is destructively modified.  (After this call it
   * should be named TO_LI).
   */
  if (! xdfs_extract_to_chain (& ex, & middle_li))
    return FALSE;

  /* Output delta
   */
  if (! xdfs_extract_output_delta (txn, & middle_li, from_inop, delta_out))
    return FALSE;

  xdfs_local_index_free (& middle_li);

  if (from_copies)
    xdfs_skip_list_free (from_copies);

  g_slist_foreach (ex.from.chain, xdfs_extract_free_elt_fe, NULL);
  g_slist_foreach (ex.to.chain, xdfs_extract_free_elt_fe, NULL);
  g_slist_free (ex.from.chain);
  g_slist_free (ex.to.chain);

  g_free (loc.state);

  return TRUE;
}

gboolean
xdfs_apply_delta (FileHandle *from_in,
		  FileHandle *delta_in,
		  FileHandle *cons_out)
{
  XdeltaControl *control;
  const XdeltaInstruction *inst;
  int primary_index = -1;
  int i;

  if (! (control = xdp_control_read (delta_in)))
    return FALSE;

  inst = control->inst;

  for (i = 0; i < control->src_count; i += 1)
    {
      if (control->src_types[i] == XS_Primary)
	primary_index = i;
    }

  for (i = 0; i < control->inst_len; i += 1)
    {
      const XdeltaInstruction *xi = inst + i;

      if (xi->index == primary_index)
	{
	  if (! handle_copy (from_in, cons_out, xi->offset, xi->length))
	    return FALSE;
	}
      else
	{
	  if (! handle_copy_len (delta_in, cons_out, xi->length))
	    return FALSE;
	}
    }

  xdp_control_free (control);

  return TRUE;
}

#ifdef XDFS_INSERT_CURRENT
gboolean
xdfs_insert_current (RepoTxn *txn, XdfsLocation *loc, Inode *result_inop)
{
  FileHandle *rh;
  gboolean result;

  /* This could be optimized to avoid creating the initial inodes
   * (which always get deleted)... but see how it works first.
   * Instead of this copy operation, construct a LI and directly
   * construct the copy file.
   */
  if (! (rh = dbfs_inode_open_read (txn, result_inop)))
    return FALSE;

  result = xdfs_insert_version_loc (txn, loc, rh, result_inop);

  handle_free (rh);

  return result;
}
#endif

gboolean
xdfs_insert_delta (RepoTxn    *txn,
		   Path       *xdfs,
		   Inode      *against_ino,
		   FileHandle *delta_in,
		   Inode      *result_ino)
{
  XdfsLocation    loc;
  XdeltaControl  *control;
  Inode           patch_ino;
  Inode           control_ino;
  int             patch_index = -1;
  int             primary_index = -1;
  int             i;

  if (! xdfs_location_init (txn, xdfs, & loc))
    return FALSE;

  /* @@@ Haven't updated the counters properly when
   * XDFS_INSERT_CURRENT is NOT set.
   */

  if (! (control = xdp_control_read (delta_in)))
    return FALSE;

  for (i = 0; i < control->src_count; i += 1)
    {
      if (control->src_types[i] == XS_Patch)
	patch_index = i;
      else if (control->src_types[i] == XS_Primary)
	primary_index = i;
    }

  if (patch_index >= 0)
    {
      FileHandle *p_out;

      if (! dbfs_inode_new (txn, & patch_ino))
	return FALSE;

      if (! (p_out = dbfs_inode_open_replace (txn, & patch_ino)))
	return FALSE;

      if (! handle_drain (delta_in, p_out))
	return FALSE;

      if (! handle_close (p_out))
	return FALSE;

      handle_free (p_out);

#ifndef XDFS_INSERT_CURRENT
      if (! xdfs_source_evolve (txn, & loc, & patch_ino))
	return FALSE;
#endif
    }

  if (! dbfs_inode_new (txn, result_ino))
    return FALSE;

  if (! xdfs_control_write (txn, result_ino, control, & control_ino))
    return FALSE;

  if (! dbfs_make_view (txn, result_ino, control->length, xdfs_stack_id, xdfs_reconstruct_view))
    return FALSE;

  for (i = 0; i < control->src_count; i += 1)
    {
      Inode   *src_ino = NULL;
      BaseName si;
      guint16  si_be = GUINT16_TO_BE (i);

      if (i == patch_index)
	src_ino = & patch_ino;
      else if (i == primary_index)
	src_ino = against_ino;

      si.data = (void*) & si_be;
      si.len  = sizeof (si_be);

      if (! dbfs_make_indirect_link_stacked_named (txn, result_ino, xdfs_stack_id, & si, src_ino))
	return FALSE;
    }

  xdp_control_free (control);

#ifdef XDFS_INSERT_CURRENT
  if (! xdfs_insert_current (txn, & loc, result_ino))
    return FALSE;
#endif

  if (! dbfs_link_create_next (txn, & loc.seq_ino, result_ino))
    return FALSE;

  loc.state->total_version_count += 1;

  if (! xdfs_state_write (txn, & loc))
    return FALSE;

  g_free (loc.state);

  return TRUE;
}
