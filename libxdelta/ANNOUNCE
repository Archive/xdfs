 -*- Text -*-

	       XDelta -- A binary delta generator

Announcing version 1.0 of XDelta.  XDelta is a library interface and
application program designed to compute changes between files.  These
changes (deltas) are similar to the output of the "diff" program in
that they may be used to store and transmit only the changes between
files.  However, unlike diff, the output of XDelta is not expressed in
a human-readable format--XDelta can also also apply these deltas to a
copy of the original file(s).  XDelta uses a fast, linear algorithm
and performs well on both binary and text files.  XDelta typically
outperforms GNU diff in both time and generated-delta-size, even for
plain text files.  XDelta also includes a simple implementation of the
Rsync algorithm and several advanced features for implementing
RCS-like file-archival upon.

Since the 0.x series of releases which began almost a year ago, what
was originally an experiment has been greatly refined.  It is smaller,
optimized, and has a clean interface.  This version is a complete
rewrite, and uses less than 50% of the memory require by the 0.x
series, even greater reductions are seen in the application for
compressed files.  Files are now stream-processed when possible, and
paged otherwise; the application now features a --maxmem option to set
the size of the internal buffers.  No files are completely read into
memory.  In addition, many features in the 0.x series have been
removed, leaving only the delta and patch functions.  The GDBM-archive
support code has been reimplemented in the new version PRCS.

XDelta was designed and implemented by Josh MacDonald.  The delta
algorithm is based on the Rsync algorithm, though implementation and
interface considerations leave the two programs quite distinct.  The
Rsync algorithm is due to Andrew Tridgell and Paul Mackerras.

To compile and install XDelta, read the instructions in the INSTALL
file.  Once you have done this, you should at least read the first few
sections of the documentation.  It is available in info format.  All
documentation is located in the doc/ subdirectory.

This release, version 1.0.0, can be found at:

   ftp://ftp.XCF.Berkeley.EDU/pub/xdelta/xdelta-1.0.0.tar.gz

HTML documentation and recent developments are available online at

   http://www.XCF.Berkeley.EDU/~jmacd/xdelta.html.

XDelta is released under the GNU Library Public License (GPL), see the
file COPYING for details.

A mailing list has been formed for announcements and discussion:

   xdelta-list@XCF.Berkeley.EDU,

you can subscribe by mailing majordomo@XCF.Berkeley.EDU with the
single line

   subscribe xdelta-list

Comments about XDelta can be addressed to the following addresses:

   xdelta@XCF.Berkeley.EDU

Bug reports can be addressed to the following address:

   xdelta-bugs@XCF.Berkeley.EDU

Here are the news since the 0.x releases:

XDelta NEWS -- history of user-visible changes. -*- Text -*-
$Date: Mon, 07 Feb 2000 14:44:11 -0800 $
$ReleaseVersion: 2.0.0-pre7 $

Please send bug reports to xdelta-bugs@XCF.Berkeley.EDU.  See the file
`README' for a description of how to report bugs.


** Changes since version 0.24

* This version is a complete rewrite, and removes some of the features
  available in version 0.  Featuring:

  - A completely new and much improved library interface.  This code is
    a complete rewrite.
  - An optimized implementation of the XDelta algorithm.
  - A drastic memory reduction.
  - The library stream-processes files where possible and uses a page-
    based interface when seeking is required (allowing the application
    to run in bounded memory on large files).
  - There is now a little documentation.  A man page and the
    beginnings of texinfo documentation.
  - Includes several advanced features used by PRCS version 2, see the
    documentation for more information.
  - Includes a minimal implementation of the Rsync algorithm, also used
    by PRCS version 2.
  - Uses an abstract file handle interface, the library has very few
    system dependencies.  This allows the library client to compute
    MD5 checksums in a single-pass as XDelta reads files, use pre-
    computed checksums, or disable checksum verification entirely.
  - Compression and file-unarchival have been removed from the library,
    these features are orthogonal.

  At the application level:

  - GDBM archive experiment has been removed.  This functionality is
    implemented directly in PRCS version 2 using the present library
    interface.
  - Base 64 encoding support has been removed for technical reasons.
  - Can no longer accept patches from stdin, due to the same
    technical reasons (the patch instruction index is stored at the
    end of the file, requiring seeking).
  - Automatic detection and un-archival of RPM and dpkg file formats
    has been removed.  Automatic uncompression of gzipped files is
    still supported.
  - The GIMP plugin is no longer supported.
